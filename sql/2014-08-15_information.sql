CREATE  TABLE IF NOT EXISTS `naha_regi`.`information` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `subject` VARCHAR(255) NOT NULL COMMENT 'タイトル' ,
  `create_date` DATETIME NOT NULL COMMENT '作成日' ,
  `update_date` DATETIME NOT NULL COMMENT '更新日' ,
  `display_date` DATETIME NOT NULL COMMENT '表示する日付' ,
  `begin_date` DATETIME NULL DEFAULT NULL COMMENT '表示開始日' ,
  `end_date` DATETIME NULL DEFAULT NULL COMMENT '表示終了日' ,
  `open_status` TINYINT(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '公開ステータス\n-1:ゴミ箱\n0:非公開\n1:公開' ,
  `description` VARCHAR(255) NULL DEFAULT NULL COMMENT '概要' ,
  `contents` LONGTEXT NULL DEFAULT NULL COMMENT '本文' ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
