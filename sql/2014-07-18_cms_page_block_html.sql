CREATE  TABLE IF NOT EXISTS `naha_regi`.`cms_page_block_html` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `page_id` INT(10) UNSIGNED NOT NULL COMMENT 'ページID' ,
  `version_id` INT(10) UNSIGNED NOT NULL COMMENT 'バージョンID' ,
  `content` LONGTEXT NULL DEFAULT NULL COMMENT 'コンテンツ' ,
  `create_date` DATETIME NOT NULL COMMENT '作成日' ,
  `update_date` DATETIME NOT NULL COMMENT '変更日' ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
