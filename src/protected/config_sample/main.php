<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'language'=>'ja',
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'商品管理システム',
	
	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.extensions.*',
		'application.vendor.*',
		'application.modules.user.models.*',
		'application.modules.user.components.*',
	),

	'modules'=>array(
		
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'password',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		
		//yii-userモジュール設定
		'user'=>array(
			//encrypting method (php hash function)
			'hash' => 'md5',
			//send activation email
			'sendActivationMail' => false,
			//allow access for non-activated users
			'loginNotActiv' => true,
			//activate user on registration (only sendActivationMail = false)
			'activeAfterRegister' => false,
			//automatically login from registration
			'autoLogin' => false,
			//registration path
			'registrationUrl' => array('/user/registration'),
			//recovery password path
			'recoveryUrl' => array('/user/recovery'),
			//login form path
			'loginUrl' => array('/user/login'),
			//page after login
			'returnUrl' => array('/user/profile'),
			//page after logout
			'returnLogoutUrl' => array('/user/login'),
		),
		
		
	),
	
	// application components
	'components'=>array(
		
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		
		//メッセージ変更
		'coreMessages' => array(
			'basePath' => 'protected/messages',
		),
		
		//DBを外に
		'db'=> require dirname(__FILE__) . '/database.php',
		
		
		//セッションを追加
		'session'=>array(//追加
				'class'=>'CDbHttpSession',//追加
				'sessionTableName'=>'sessions',//追加
				'connectionID'=>'db',//追加
		),
		
		
		
		
		
		//yii-user
		'user'=>array(
			// enable cookie-based authentication
			'class' => 'WebUser',
			'allowAutoLogin'=>true,
			'loginUrl' => array('/user/login'),
		),
		
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),
	
	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
		
		//カート設定
		'cart_config' => require dirname(__FILE__) . '/cart_config.php',
		
		//画像ファイル
		'imageSizeList' => require dirname(__FILE__) . '/image_size.php',
		
		//画像リサイズの閾値
		'resize_x' => 600,
		
		//バーコードのコード体系
		'barcode' => 'INSTORE',//JAN:JAN形式のインストアコード INSTORE:独自インストアコード
		
		//公開スイッチ
		'open' => TRUE,//
		
	),
);