<?php

/**
 * This is the model class for table "product_sku".
 *
 * The followings are the available columns in table 'product_sku':
 * @property string $id
 * @property string $product_id
 * @property string $brunch_item_id
 * @property string $brunch_item_name
 * @property string $price
 * @property string $sale_price
 * @property string $stock
 * @property int $rank
 * @property int $delete_flag
 */
class ProductSkuModel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'product_sku';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_id, brunch_item_id, brunch_item_name', 'required'),
			array('product_id, price, sale_price, stock, rank, delete_flag', 'length', 'max'=>10),
			array('code_str', 'length', 'max'=>16),
			array('brunch_item_id', 'length', 'max'=>125),
			array('brunch_item_name', 'length', 'max'=>225),
			
			array( 'content', 'safe' ),
			
			
			
			//
			array('id, product_id, brunch_item_id, brunch_item_name, price, sale_price, stock, code_str', 'safe', 'on'=>'search'),
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			
			//親アイテム
			'products' => array(
				self::BELONGS_TO,
				'ProductsModel',
				'product_id'
			),
			
			//画像リスト
			'product_sku_images' => array(
				self::HAS_MANY,
				'ProductSkuImagesModel',
				array( 'sku_id' => 'id' )
			),
			
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'product_id' => '商品テーブルのID',
			'brunch_item_id' => '枝番',
			'brunch_item_name' => '枝番品名',
			'price' => '定価',
			'sale_price' => '売価',
			'stock' => '在庫数',
			'rank' => '表示順',
			'delete_flag' => '削除フラグ',
			'content' => '説明',
			'code_str' => 'インショップコード'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('product_id',$this->product_id,true);
		$criteria->compare('brunch_item_id',$this->brunch_item_id,true);
		$criteria->compare('brunch_item_name',$this->brunch_item_name,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('sale_price',$this->sale_price,true);
		$criteria->compare('stock',$this->stock,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductSkuModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	// ----------------------------------------------------
	/**
	 * pidとsidからskuのレコード抽出、在庫のアップデート
	 * @param array $pid, $sid, $stock, $opt
	 * @return array
	 */

	public function stockMod($params)
	{
		// パラメータ展開
		$pid = $params['pid'];
		$sid = $params['sid'];
		$stock = $params['stock'];
		$opt = $params['opt'];
		
		// DB接続
		$connection = Yii::app()->db;
		
		// トランザクション開始
		$command = $connection->createCommand('BEGIN');
		$res = $command->execute();
		
		// 対象のskuのレコードを抽出
		$sql = 
			'SELECT sku.id AS sku_id, stock FROM products AS p INNER JOIN product_sku AS sku ON p.id = sku.product_id'.
			' WHERE p.item_id = :pid'.
			' AND sku.brunch_item_id = :sid'.
			' FOR UPDATE'
		;
		$command = $connection->createCommand($sql);
		$command->bindParam(":pid", $pid, PDO::PARAM_STR);
		$command->bindParam(":sid", $sid, PDO::PARAM_STR);
		$rows = $command->queryAll();

		// 該当ない場合はエラー
		if(empty($rows))
		{
			return array('result' => false, 'error' => 404);
			exit;
		}

		// アップデート用データ準備
		$id = $rows[0]['sku_id'];
		$dbStock = $rows[0]['stock'];

		// $optの値を判定して、在庫の加算・減算・置き換え
		if($opt !== null){
			switch($opt)
			{
				case true:
					$dbStock += $stock;
					break;
				case false:
					$dbStock -= $stock;
			}
		}else{
			$dbStock = $stock;
		}

		// UPDATE処理
		$sql = 
			'UPDATE product_sku SET '.
			'stock = :stock '.
			'WHERE id = :id';
		$command = $connection->createCommand($sql);
		$command->bindParam(":stock", $dbStock, PDO::PARAM_STR);
		$command->bindParam(":id", $id, PDO::PARAM_STR);
		$res = $command->execute();
		
		// トランザクションコミット
		$command = $connection->createCommand('COMMIT');
		$res = $command->execute();
		
		// 成功をリターン
		return array('result' => true, 'stock' => $dbStock);
	}
	
	// ----------------------------------------------------
	
	/**
	 * 在庫処理（レジ用
	 * @param array $datas
	 * @param boolean
	 */
	public function regi_zaiko( $datas )
	{
		
		$sql = 'SELECT * FROM product_sku WHERE id = :id FOR UPDATE';
		$sql2 = 'UPDATE product_sku SET stock = :stock  WHERE id = :id';
		
		$return = true;
		$connection = Yii::app()->db;
		
		foreach( $datas as $id => $v )
		{
			$v = (int)$v;
			$transaction = $connection->beginTransaction();
			
			try
			{
				
				//今のデータ取得
				$command = $connection->createCommand($sql);
				$command->bindParam( ':id', $id );
				$rows = $command->queryAll();
				
				
				
				//在庫設定があれば、在庫カウントダウン
				if( $rows[0]['stock'] != '' )
				{
					$set =  $rows[0]['stock'] - $v;
					$command = $connection->createCommand($sql2);
					$command->bindParam( ':stock', $set );
					$command->bindParam( ':id', $id );
					$command->execute();
					
					
					//出庫実績
					$this->setLeaving( $connection, $id, $v );
				}
				
				
				$transaction->commit();
			}
			catch(Exception $e) // クエリの実行に失敗した場合、例外が発生します
			{
				$transaction->rollback();
				$return = false;
			}
			
		}
		
		
		return $return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 在庫処理
	 * @param array $list skuid->qtyのリスト
	 * @return boolean
	 */
	public function ec_zaiko( $list )
	{
		$connection = Yii::app()->db;
		$return = true;
		
		$sql1 = 'SELECT * FROM product_sku WHERE id = :id FOR UPDATE';
		$sql2 = 'UPDATE product_sku SET stock = :stock WHERE id = :id';
		
		$transaction = $connection->beginTransaction();
		foreach( $list as $id => $qty )
		{
			try
			{
				
				//今のデータ取得
				$command = $connection->createCommand($sql1);
				$command->bindParam( ':id', $id );
				$rows = $command->queryAll();
				//在庫カウントダウン
				$set =  $rows[0]['stock'] - $qty;
				if( $set >= 0 )
				{
					$command = $connection->createCommand($sql2);
					$command->bindParam( ':stock', $set );
					$command->bindParam( ':id', $id );
					$command->execute();
				}
				else
				{
					$set =  $rows[0]['stock'];
					$command = $connection->createCommand($sql2);
					$command->bindParam( ':stock', $set );
					$command->bindParam( ':id', $id );
					$command->execute();
					$return = false;
				}
				
				
				////出庫に記録
				$this->setLeaving( $connection, $id, $qty );
				
			}
			catch(Exception $e) // クエリの実行に失敗した場合、例外が発生します
			{
				$transaction->rollback();
				return false;
			}
			
		}
		
		
		//全体をコミット
		if( $return === true )
		{
			$transaction->commit();
			return true;
		}
		else
		{
			$transaction->rollback();
			return false;
		}
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * 出庫データ記録
	 * @param object $connection
	 * @param int $id
	 * @param int $qty
	 */
	protected function setLeaving( $connection, $id, $qty )
	{
		//出庫用ターゲットを取得
		$sql = 'SELECT * FROM stocking WHERE ( sku_id = :sku_id ) AND ( purchase_qty > leaving_qty ) ORDER BY insert_date ASC LIMIT 0,1 FOR UPDATE';
		$command = $connection->createCommand($sql);
		$command->bindParam( ':sku_id', $id );
		$row = $command->queryRow();
		
		//レコードなしは無視
		if( $row === FALSE ){ return; }
		
		
		//数を調査
		$current = $row['purchase_qty'] - $row['leaving_qty'];
		$nokori = $current - $qty;
		if( $nokori < 0 )
		{
			$set = $current;
		}
		else{
			$set = $qty;
		}
		
		//出庫記録に記録
		$sql = 'INSERT INTO stocking_leave (
					stocking_id,
					leaving_date,
					leaving_count
				) VALUES (
					:stocking_id,
					:leaving_date,
					:leaving_count
				)';
		$command = $connection->createCommand($sql);
		$command->bindParam( ':stocking_id', $row['id'] );
		$command->bindParam( ':leaving_date', date('Y-m-d H:i:s') );
		$command->bindParam( ':leaving_count', $set );
		$command->execute();
		
		
		//入荷に出て行った数を記録
		$set_leave = $row['leaving_qty'] + $set;
		$sql = 'UPDATE stocking SET leaving_qty = :leaving_qty WHERE id = :id';
		$command = $connection->createCommand($sql);
		$command->bindParam( ':leaving_qty', $set_leave );
		$command->bindParam( ':id', $row['id'] );
		$command->execute();
		
		
		//出庫実績が複数必要の場合
		if( $nokori < 0 )
		{
			$this->setLeaving( $connection, $id, (-1 * $nokori ) );
		}
		
		return;
	}
	// ----------------------------------------------------
	
	/**
	 * レジ用在庫処理
	 * @param object $connection
	 * @param object $transaction
	 * @param array $list
	 * @return mix
	 */
	public function regi_zaiko2( $connection, $transaction, $list )
	{
		$sql1 = 'SELECT * FROM product_sku WHERE id = :id FOR UPDATE';
		$sql2 = 'UPDATE product_sku SET stock = :stock WHERE id = :id';
		
		$return = array(
			'result' => true,
			'truns_error' => false,
			'errors' => array(),//在庫エラーになったskuidと現在在庫を保存
		);
		
		
		try
		{
			foreach( $list as $id => $qty )
			{
				//今のデータ取得
				$command = $connection->createCommand($sql1);
				$command->bindParam( ':id', $id );
				$rows = $command->queryAll();
				$set =  $rows[0]['stock'] - $qty;
				
				//在庫エラー
				if( $set < 0 )
				{
					$return[ $id ] = $rows[0]['stock'];
					$return['result'] = FALSE;
					continue;
				}
				
				
				//在庫エラーがなければ続けて処理
				$command = $connection->createCommand($sql2);
				$command->bindParam( ':stock', $set );
				$command->bindParam( ':id', $id );
				$command->execute();
				
				
				//出庫処理
				$this->setLeaving( $connection, $id, $qty );
				
			}
		
		}
		catch(Exception $e) // クエリの実行に失敗した場合、例外が発生します
		{
			$return['truns_error'] = TRUE;
			return $return;
		}
		
		
		return $return;
	}
	
	// ----------------------------------------------------
}
