<?php
/* =============================================================================
 * 那覇レジモデル
 * ========================================================================== */
class NahaRegiMailModel extends CFormModel
{
	//自動返信メール送信先
	protected $autoReplyList = array();
	
	//ルール保存用
	protected $mailRules = array();
	
	//名前保存用
	protected $mailAttributeNames = array();
	
	//csrf設定
	protected $csrf = array();
	
	//テスト用
	public $test = 'テスト';
	
	//モード定義
	public $mode = '';
	
	//ActiveForm保存
	public $form;
	
	//セッターゲッター用
	public $_dynamic = array();
	
	//dirname
	public $dir = '';
	
	//CSRF対策用トークン
	protected $token = '';


	// ----------------------------------------------------
	/**
	 * 初期化
	 */
	public function init() {
		parent::init();
		$this->setMode();
		$this->csrf();
	}
	
	// ----------------------------------------------------
	
	/**
	 * csrf設定
	 */
	public function csrf()
	{
		if( $this->csrf['switch'] === FALSE )
		{
			return;
		}
		
		
		session_start();
		
		switch( $this->mode )
		{
			case 'confirm':
			case 'send':
			case 'reinput':
				
				$form = Input::GetPost( $this->csrf['session'] );
				$session = $_SESSION[$this->csrf['session']];
				
				if( $form != $session )
				{
					throw new CHttpException(400,'クッキーエラーがおきています。');
					exit();
				}
				
				
			default:
				$token = sha1( time() . '---' . rand( 0, 100000 ) );
				$_SESSION[$this->csrf['session']] = $token;
				$this->token = $token;
				break;
		}
	}
	
	// ----------------------------------------------------
	
	/**
	 * トークンのHTMLを出力
	 */
	public function getTokenTag()
	{
		if( $this->csrf['switch'] === FALSE )
		{
			return;
		}
		echo CHtml::hiddenField( $this->csrf['session'], $this->token );
	}
	
	// ----------------------------------------------------
	
	/**
	 * モード定義
	 */
	public function setMode()
	{
		$mailstep = Input::GetPost('mailstep');
		switch( $mailstep )
		{
			case 'confirm':
				$this->mode = 'confirm';
				break;
			case 'send':
				$this->mode = 'send';
				break;
			case 'thanks':
				$this->mode = 'thanks';
				break;
			case 'reinput':
				$this->mode = 'reinput';
				break;
			default:
				$this->mode = 'input';
		}
	}
	
	// ----------------------------------------------------
	
	/**
	 * ラベル
	 */
	public function attributeLabels()
	{
		/*
		return
			array(
				'name1' => 'お客様姓',
			);
		*/
		return $this->mailAttributeNames;
	}
	
	// ----------------------------------------------------
	
	/**
	 * ルール
	 **/
	
	public function rules()
	{
		/*
		$res = array(
			array( 'payment_method', 'required' ),
		);
		return array_merge($res,$add);
		*/
		
		return $this->mailRules;
	}
	
	// ----------------------------------------------------
	
	/**
	 * データを空にする
	 */
	public function setEmpty($attribute,$params)
	{
		$this->{$attribute} = '';
		return true;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 電話番号形式チェック
	 */
	public function tel( $attribute, $params )
	{
		$target = $this->{$attribute};
		
		if( $target == '' ){ return true; }
		
		if( preg_match('/\d{2,4}-\d{2,4}-\d{4}/', $target))
		{
			return true;
		}
		
		$this->addError( $attribute, $this->getAttributeLabel($attribute) . 'が電話番号形式ではありません');
		return false;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 郵便番号形式
	 */
	public function zip($attribute,$params)
	{
		$target = $this->{$attribute};
		
		if( $target == '' ){ return true; }
		
		if(preg_match('/\d{3}\-\d{4}/', $target ))
		{
			return true;
		}
		
		
		$this->addError( $attribute, $this->getAttributeLabel($attribute) . 'が郵便番号形式ではありません');
		return false;
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * 自動返信メールに追加
	 */
	public function autoreply($attribute,$params)
	{
		$target = $this->{$attribute};
		if( $target == '' ){ return true; }
		
		$this->autoReplyList[] = $target;
		return true;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 引き渡しリストを作成
	 */
	public function getHidden()
	{
		$res = $this->getAttributes();
		$return = '';
		foreach( $res['_dynamic'] as $key => $val )
		{
			if(is_array($val))
			{
				foreach( $val as $one )
				{
					$return .= CHtml::hiddenField( $key.'[]', $one ) . "\n";
				}
			}
			else
			{
				$return .= CHtml::hiddenField( $key, $val ) . "\n";
			}
		}
		return $return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * エラー表示
	 * @param string $name
	 * @param string $pre
	 * @param string $tail
	 * @return string
	 */
	public function callError( $name, $pre = '<div class="error">', $tail = '</div>' )
	{
		$error = $this->getError( $name );
		if(is_null($error))
		{
			return '';
		}
		
		$return = $pre . $error . $tail ;
		
		return $return;
	}
	
	// ----------------------------------------------------
	/**
	 * アトリビュートを設定
	 */
	public function setAttr()
	{
		$attr = array();
		foreach( $this->mailAttributeNames as $key => $name )
		{
			$this->{$key} = '';
			$attr[$key] = Input::Post($key);
		}
		$this->setAttributes($attr);
	}
	// ----------------------------------------------------
	
	/**
	 * バリデーションを実行
	 */
	public function execValidate()
	{
		$this->setAttr();
		$res = $this->validate();
		return $res;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 再入力時などデータ取得
	 * @param string $name
	 * @param boolean $safe
	 * @return string
	 */
	public function getValue( $name, $safe = true )
	{
		$res = $this->getAttributes();
		if(array_key_exists( $name, $res['_dynamic'] ))
		{
			$return = $res['_dynamic'][$name];
			
			//配列の場合
			if( ( is_array($return) === TRUE )&&( $safe === TRUE ) )
			{
				$return2 = array();
				foreach( $return as $one )
				{
					$return2[] = CHtml::encode($one);
				}
				return $return2;
			}
			
			//セーフなしの場合
			if( $safe === FALSE )
			{
				return $return;
			}
			
			//通常リターン
			return CHtml::encode($return);
		}
		return '';
	}
	
	// ----------------------------------------------------
	
	
	/**
	 * ラジオボタンのチェックを取得
	 * @param string $name
	 * @param string $value
	 * @param array $htmlOptions
	 * @return void
	 */
	public function getRaidio( $name, $value, $htmlOptions )
	{
		$htmlOptions['value'] = $value;
		echo CHtml::radioButton(
				$name,
				(($value == $this->getValue($name))?TRUE:FALSE),
				$htmlOptions
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * チェックボタンを取得
	 * @param string $name
	 * @param string $value
	 * @param array $htmlOptions
	 * @return void
	 */
	public function getCheck( $name, $value, $htmlOptions )
	{
		$htmlOptions['value'] = $value;
		echo CHtml::checkBox(
				$name.'[]',
				$this->isSendedArray( $name, $value ),
				$htmlOptions
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * 値の中にデータがあるか
	 * @param
	 */
	protected function isSendedArray( $name, $value )
	{
		$get = Input::GetPost($name);
		if(!is_array($get)){ $get = array(); }
		if(in_array( $value, $get ) )
		{
			return TRUE;
		}
		return FALSE;
	}
	
	// ----------------------------------------------------
	
	/**
	 * メール送信
	 */
	public function sendMail()
	{
		require_once 'qdmail.php';
		
		//データ作成
		$datas = array();
		foreach( $this->mailAttributeNames as $name => $v )
		{
			$datas[$name]= $this->getValue( $name, false );
		}
		$datas['template_mode'] = true;
		
		
		//運営者あて
		$body = Yii::app()->controller->renderFile(
				$this->dir . 'mail_template_site.php',
				$datas,
				true
			);
		if( count( $this->autoReplyList) > 0 )
		{
			$from = $this->autoReplyList[0];
		}
		else
		{
			$from = $this->config['sitemanager']['From'];
		}
		qd_send_mail(
				'text' ,
				array( $this->config['sitemanager']['To'], $this->config['sitemanager']['ToName'] ),
				$this->config['sitemanager']['Subject'],
				$body,
				$from
			);
		
		
		//オートリプライ
		foreach( $this->autoReplyList as $mail )
		{
			$body = Yii::app()->controller->renderFile(
					$this->dir . 'mail_template_site.php',
					$datas,
					true
				);
			qd_send_mail(
					'text' ,
					$mail,
					$this->config['reply']['Subject'],
					$body,
					array( $this->config['reply']['From'], $this->config['reply']['FromName'] )
				);
		}
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * setter
	 */
	public function __set($name, $value) {
		if(property_exists($this, $name ))
		{
			parent::__set($name, $value);
			return;
		}
		$this->_dynamic[$name] = $value;
	}
	
	// ----------------------------------------------------
	
	/**
	 * getter
	 */
	public function __get($name)
	{
		if(array_key_exists( $name, $this->_dynamic ))
		{
			return $this->_dynamic[$name];
		}
		parent::__get($name);
		return;
	}
	// ----------------------------------------------------
	
}
