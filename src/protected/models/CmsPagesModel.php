<?php

/**
 * This is the model class for table "cms_pages".
 *
 * The followings are the available columns in table 'cms_pages':
 * @property string $id
 * @property string $page_root
 * @property string $page_path
 * @property string $subject
 * @property string $create_date
 * @property string $update_date
 * @property string $display_date
 * @property string $open_date
 * @property string $close_date
 * @property integer $page_status
 */
class CmsPagesModel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cms_pages';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('page_root, page_path, create_date, update_date, display_date', 'required'),
			array('page_status', 'numerical', 'integerOnly'=>true),
			array('page_root, page_path', 'length', 'max'=>255),
			array('subject, open_date, close_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, page_root, page_path, subject, create_date, update_date, display_date, open_date, close_date, page_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'page_root' => 'ページルート（実質的カテゴリ）',
			'page_path' => 'ページパス',
			'subject' => 'タイトル',
			'create_date' => '作成日',
			'update_date' => 'Update Date',
			'display_date' => '表示作成日',
			'open_date' => '公開日',
			'close_date' => '公開終了日',
			'page_status' => '公開ステータス
-2:完全消去
-1:ごみばこ
0:下書き
1:公開',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('page_root',$this->page_root,true);
		$criteria->compare('page_path',$this->page_path,true);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('update_date',$this->update_date,true);
		$criteria->compare('display_date',$this->display_date,true);
		$criteria->compare('open_date',$this->open_date,true);
		$criteria->compare('close_date',$this->close_date,true);
		$criteria->compare('page_status',$this->page_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CmsPagesModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
