<?php
/* =============================================================================
 * フォーム送信チェッカーモデル
 * ========================================================================== */
class HaisaiModel extends CFormModel
{
	//項目を作っておく
	public $name1;//お客様姓
	public $name2;//お客様名
	public $kana1;//お客様かな姓
	public $kana2;//お客様かな名
	public $zip;//郵便番号
	public $tel;//電話番号
	public $mailaddress;//メアド
	public $mailaddress2;//メアド確認用
	public $pref;//都道府県
	public $address1;//住所1（市区町村）
	public $address2;//住所2（町域番地）
	public $address3;//住所3（建物など）
	
	public $yobo;//要望
	
	public $d_check;//配送先が違う場合1
	
	public $d_name1;//配送先姓
	public $d_name2;//配送先名
	public $d_zip;//配送先郵便番号
	public $d_pref;//配送先都道府県
	public $d_tel;//配送先電話番号
	public $d_address1;//配送先住所1（市区町村）
	public $d_address2;//配送先住所2（町域番地）
	public $d_address3;//配送先住所3（建物など）
	
	
	public $payment_method;//支払い方法
	public $delivery_method;//配送方法
	
	// ----------------------------------------------------
	/**
	 * 初期化
	 */
	public function init() {
		parent::init();
	}
	
	// ----------------------------------------------------
	
	/**
	 * ラベル
	 */
	public function attributeLabels()
	{
		return
			array(
				'name1' => 'お客様姓',
				'name2' => 'お客様名',
				'kana1' => 'お客様かな姓',
				'kana2' => 'お客様かな名',
				'zip' => '郵便番号',
				'tel' => '電話番号',
				'mailaddress' => 'メアド',
				'mailaddress2' => 'メアド確認用',
				'pref' => '都道府県',
				'address1' => '住所1（市区町村）',
				'address2' => '住所2（町域番地）',
				'address3' => '住所3（建物など）',
				'yobo' => 'ご要望',
				'd_check' => '配送先が違う場合はチェック',
				'd_name1' => '配送先姓',
				'd_name2' => '配送先名',
				'd_zip' => '配送先郵便番号',
				'd_pref' => '配送先都道府県',
				'd_tel' => '配送先電話番号',
				'd_address1' => '配送先住所1（市区町村）',
				'd_address2' => '配送先住所2（町域番地）',
				'd_address3' => '配送先住所3（建物など）',
				'payment_method' => 'お支払い方法',
				'delivery_method' => '配送方法',
			);
	}
	
	// ----------------------------------------------------
	
	/**
	 * ルール
	 **/
	
	public function rules()
	{
		$res = array(
			array( 'payment_method', 'required' ),
			array( 'name1', 'required' ),
			array( 'name2', 'required' ),
			array( 'kana1', 'required' ),
			array( 'kana2', 'required' ),
			array( 'tel', 'required' ),
			array( 'tel', 'tel' ),
			array( 'zip', 'required' ),
			array( 'zip', 'zip' ),
			array( 'pref', 'required' ),
			array( 'address1', 'required' ),
			array( 'address2', 'required' ),
			//array( 'address3', 'required' ),
			array( 'address3', 'safe' ),
			array( 'mailaddress', 'required' ),
			array( 'mailaddress', 'email' ),
			array( 'mailaddress2', 'required' ),
			array( 'mailaddress', 'compare', 'compareAttribute'=>'mailaddress2' ),
			array( 'd_check', 'safe' ),
			array( 'yobo', 'safe' ),
			array( 'delivery_method', 'required' ),
		);
		
		
		
		$post = Input::Post('HaisaiModel');
		
		
		//配送先変更
		if( (isset($post['d_check'])) && ( $post['d_check'] == 1 ) )
		{
			$add = array(
				array( 'd_name1', 'required' ),
				array( 'd_name2', 'required' ),
				array( 'd_tel', 'required' ),
				array( 'd_tel', 'tel' ),
				array( 'd_zip', 'required' ),
				array( 'd_zip', 'zip' ),
				array( 'd_pref', 'required' ),
				array( 'd_address1', 'required' ),
				array( 'd_address2', 'required' ),
				array( 'd_address3', 'required' ),
			);
		}
		//配送先変更なし
		else
		{
			$add = array(
				array( 'd_name1', 'setEmpty' ),
				array( 'd_name2', 'setEmpty' ),
				array( 'd_tel', 'setEmpty' ),
				array( 'd_zip', 'setEmpty' ),
				array( 'd_pref', 'setEmpty' ),
				array( 'd_address1', 'setEmpty' ),
				array( 'd_address2', 'setEmpty' ),
				array( 'd_address3', 'setEmpty' ),
			);
		}
		
		
		
		return array_merge($res,$add);
	}
	
	// ----------------------------------------------------
	
	/**
	 * データを空にする
	 */
	public function setEmpty($attribute,$params)
	{
		$this->{$attribute} = '';
		return true;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 電話番号形式チェック
	 */
	public function tel($attribute,$params)
	{
		$target = $this->{$attribute};
		
		if( $target == '' ){ return true; }
		
		if( preg_match('/\d{2,4}-\d{2,4}-\d{4}/', $target))
		{
			return true;
		}
		
		$this->addError( $attribute, $this->getAttributeLabel($attribute) . 'が電話番号形式ではありません');
		return false;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 郵便番号形式
	 */
	public function zip($attribute,$params)
	{
		$target = $this->{$attribute};
		
		if( $target == '' ){ return true; }
		
		if(preg_match('/\d{3}\-\d{4}/', $target ))
		{
			return true;
		}
		
		
		$this->addError( $attribute, $this->getAttributeLabel($attribute) . 'が郵便番号形式ではありません');
		return false;
		
	}
	
	
	// ----------------------------------------------------
}
