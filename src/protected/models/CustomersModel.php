<?php

/**
 * This is the model class for table "customers".
 *
 * The followings are the available columns in table 'customers':
 * @property string $id
 * @property string $name1
 * @property string $name2
 * @property string $kana1
 * @property string $kana2
 * @property string $mailaddress
 * @property integer $customer_status
 * @property string $create_date
 * @property string $update_date
 */
class CustomersModel extends CActiveRecord
{
	//プロフィールフィールドのモデル
	public $profileFieldModel;
	
	
	// ----------------------------------------------------
	/**
	 * コンストラクタ
	 */
	public function __construct($scenario = 'insert') {
		$this->profileFieldModel = new CustomerProfileFieldModel();
		parent::__construct($scenario);
	}
	
	// ----------------------------------------------------
	
	/**
	 * デフォルト値
	 */
	public function init()
	{
		if($this->scenario == 'insert')
		{
			foreach( $this->profileFieldModel->findAll() as $one )
			{
				if( $one->default_value != '' )
				{
					$this->{$one->field_name} = $one->default_value;
				}
			}
		}
	}
	
	// ----------------------------------------------------
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customers';
	}
	
	
	// ----------------------------------------------------
	
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		
		//基本データ分
		$return = array(
			array('name1, name2, kana1, kana2, mailaddress', 'required'),
			array('customer_status', 'numerical', 'integerOnly'=>true),
			array('name1, name2, kana1, kana2', 'length', 'max'=>125),
			array(' create_date, update_date', 'safe'),
			array('', 'telNumber'),
			array('', 'zip'),
			array('mailaddress', 'email'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name1, name2, kana1, kana2, mailaddress, customer_status, create_date', 'safe', 'on'=>'search'),
		);
		
		
		
		
		foreach( $this->profileFieldModel->findAll() as $one )
		{
			
			//バリデーションの有り無しチェック
			$check = 0;
			
			//必須
			if( $one->required == 1 )
			{
				$return[] = array( $one->field_name, 'required' );
				$check ++ ;
			}
			
			//最短
			if( (int)$one->min_length > 0 )
			{
				$return[] = array( $one->field_name, 'length', 'min' => (int)$one->min_length );
				$check ++ ;
			}
			
			
			//最長
			if( (int)$one->max_length > 0 )
			{
				$return[] = array( $one->field_name, 'length', 'max' => (int)$one->max_length );
				$check ++ ;
			}
			
			
			//Yiiのバリデーションを利用
			if( $one->other_validation != '' )
			{
				$one->other_validation = str_replace( array( "\r\n", "\r"), array("\n","\n"), $one->other_validation );
				$list = explode( "\n", $one->other_validation );
				foreach( $list as $one_valid )
				{
					if( $one_valid == '' ){ continue; }
					$arr = eval( 'return ' . $one_valid . ';' );
					array_unshift( $arr, $one->field_name );
					$return[] = $arr;
					$check ++ ;
				}
			}
			
			//正規表現
			if( $one->regrex != '' )
			{
				$one->regrex = str_replace( array( "\r\n", "\r"), array("\n","\n"), $one->regrex );
				$list = explode( "\n", $one->regrex );
				foreach( $list as $one_valid )
				{
					if( $one_valid == '' ){ continue; }
					$arr = eval( 'return ' . $one_valid . ';' );
					array_unshift( $arr, 'match' );
					array_unshift( $arr, $one->field_name );
					$return[] = $arr;
					$check ++ ;
				}
			}
			
			
			//バリデーションがなかった時
			if( $check == 0 )
			{
				$return[] = array( $one->field_name, 'safe' );
			}
			
			
			
			//サーチ用
			if( $one->search_flag == 1 )
			{
				$return[] = array( $one->field_name, 'safe', 'on'=>'search' );
			}
			
			
		}
		
		return $return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 電話形式
	 */
	public function telNumber($attribute,$params)
	{
		if( $this->$attribute == '' )
		{
			return;
		}
		if(!preg_match("/\A[0-9]{2,4}\-?[0-9]{2,4}\-?[0-9]{3,4}\Z/u", $this->$attribute))
		{
			$this->addError( $attribute,'電話番号形式ではありません。');
		}
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * 郵便番号形式
	 */
	public function zip($attribute,$params)
	{
		if( $this->$attribute == '' )
		{
			return;
		}
		
		if(!preg_match( '/[0-9]{3}\-[0-9]{4}/i', $this->$attribute))
		{
			$this->addError( $attribute,'郵便番号形式ではありません。');
		}
		
		
	}
	// ----------------------------------------------------
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		$return = array(
			'id' => 'ID',
			'name1' => '姓',
			'name2' => '名',
			'kana1' => '姓（かな）',
			'kana2' => '名（かな）',
			'mailaddress' => 'メールアドレス1',
			'customer_status' => 'お客様ステータス',
			'create_date' => '入会日',
			'update_date' => '変更日',
		);
		
		
		foreach( $this->profileFieldModel->findAll() as $one )
		{
			$return[$one->field_name] = $one->field_label;
		}
		
		return $return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
		
		$criteria->compare('id',$this->id,true);
		$criteria->compare('name1',$this->name1,true);
		$criteria->compare('name2',$this->name2,true);
		$criteria->compare('kana1',$this->kana1,true);
		$criteria->compare('kana2',$this->kana2,true);
		$criteria->compare('mailaddress',$this->mailaddress,true);
		$criteria->compare('customer_status',$this->customer_status);
		$criteria->compare('create_date',$this->create_date,true);
		
		//追加
		foreach( $this->profileFieldModel->findAll('search_flag=1') as $one )
		{
			$criteria->compare( $one->field_name ,$this->{$one->field_name}, true );
		}
		
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CustomersModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
