<?php

/**
 * This is the model class for table "rel_product_categories".
 *
 * The followings are the available columns in table 'rel_product_categories':
 * @property string $categories_id
 * @property string $product_id
 */
class RelProductCategoriesModel extends CActiveRecord
{
	
	// ----------------------------------------------------
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rel_product_categories';
	}
	
	// ----------------------------------------------------
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('categories_id, product_id', 'required'),
			array('categories_id, product_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('categories_id, product_id', 'safe', 'on'=>'search'),
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			
			//プロダクト
			'product_search' => array( self::HAS_MANY, 'ProductsModel', array( 'id' => 'product_id' ) ),
			
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'categories_id' => 'カテゴリID',
			'product_id' => 'Product',
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * プロダクトサーチ
	 * @param int $id ID
	 */
	public function searchProduct( $id )
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 'categories_id = :categories_id';
		$criteria->params = array( ':categories_id' => $id );
		
		return new CActiveDataProvider(
			$this,
			array(
				'criteria'=>$criteria,
				'pagination' => array(
					'pageSize' => 1,
				),
			)
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('categories_id',$this->categories_id,true);
		$criteria->compare('product_id',$this->product_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RelProductCategoriesModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	// ----------------------------------------------------
}
