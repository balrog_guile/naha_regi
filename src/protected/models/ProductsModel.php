<?php

/**
 * This is the model class for table "products".
 *
 * The followings are the available columns in table 'products':
 * @property string $id
 * @property string $item_id
 * @property string $item_name
 * @property string $price
 * @property string $sale_price
 * @property integer $open_status
 * @property string $item_status
 * @property string $postage
 * @property string $point_rate
 * @property string $shipment
 * @property string $qty_limit
 * @property string $serch_word
 * @property string $remark
 * @property string $main_view_comment
 * @property string $main_detail_comment
 * @property string $thumbnail
 * @property string $main_image
 * @property string $main_big_image
 * @property string $sub1_title
 * @property string $sub1_text
 * @property string $sub1_img
 * @property string $sub2_title
 * @property string $sub2_text
 * @property string $sub2_img
 * @property string $sub3_title
 * @property string $sub3_img
 * @property string $sub3_text
 * @property string $sub4_title
 * @property string $sub4_text
 * @property string $sub4_img
 * @property string $sub5_title
 * @property string $sub5_text
 * @property string $sub5_img
 * @property string $sub6_title
 * @property string $sub6_text
 * @property string $sub6_img
 * @property integer $delete_flag
 * @property string $create_date
 * @property string $update_date
 * @property array $categories
 */
class ProductsModel extends CActiveRecord
{
	
	public $categories;
	public $reinpit_optionsku = null;
	
	// ----------------------------------------------------
	
	/**
	 * 初期化
	 */
	public function init() {
		parent::init();
		
		if(
			( Input::Get('ProductsModel_page') == '' )&&
			( (int)Input::Get('page') > 0 )
		)
		{
			$_GET['ProductsModel_page'] = (int)Input::Get('page');
		}
		unset( $_GET['page'] );
	}
	// ----------------------------------------------------
	
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products';
	}
	
	// ----------------------------------------------------
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		$return =  array(
			array('item_id, item_name, main_detail_comment', 'required'),
			array('open_status, delete_flag', 'numerical', 'integerOnly'=>true),
			array('item_id, shipment', 'length', 'max'=>125),
			array('item_name', 'length', 'max'=>225),
			array('price, sale_price, postage, point_rate, qty_limit', 'length', 'max'=>10),
			array('item_status, serch_word, remark, main_view_comment, thumbnail, main_image, main_big_image, sub1_title, sub1_text, sub1_img, sub2_title, sub2_text, sub2_img, sub3_title, sub3_img, sub3_text, sub4_title, sub4_text, sub4_img, sub5_title, sub5_text, sub5_img, sub6_title, sub6_text, sub6_img, create_date, update_date, categories_many', 'safe'),
			
			array('link_url', 'checkLinkFormat'),
			array('link_url', 'setOnEmpty'),
			
			array( 'page_key', 'safe' ),
			array( 'page_desc', 'safe' ),
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, item_id, item_name, price, sale_price, open_status, item_status, postage, point_rate, shipment, categories_many, qty_limit, serch_word, remark, main_view_comment, main_detail_comment, thumbnail, main_image, main_big_image, sub1_title, sub1_text, sub1_img, sub2_title, sub2_text, sub2_img, sub3_title, sub3_img, sub3_text, sub4_title, sub4_text, sub4_img, sub5_title, sub5_text, sub5_img, sub6_title, sub6_text, sub6_img, delete_flag, create_date, update_date', 'safe', 'on'=>'search'),
		);
		
		$return[] = array('link_url', 'checkRulExists', 'on'=>'insert');
		$return[] = array('link_url', 'checkRulExists', 'on'=>'update');
		
		
		$return[] = array('item_id', 'checkItemidExists', 'on'=>'insert');
		$return[] = array('item_id', 'checkItemidExists', 'on'=>'update');
		
		
		return $return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 品番重複チェック
	 */
	public function checkItemidExists($attribute,$params)
	{
		if( $this->scenario == 'insert' )
		{
			$item_id = $this->{$attribute};
			$find = $this->find('item_id = :item_id', array( ':item_id' => $item_id ));
			if(is_null($find))
			{
				return;
			}
			$this->addError( $attribute,'指定した商品番号はすでに存在しています');
			return;
		}
		else if( $this->scenario == 'update')
		{
			$item_id = $this->{$attribute};
			$find = $this->find('( item_id = :item_id )AND( id <> :id )', array( ':item_id' => $item_id, ':id' => $this->id ));
			if(is_null($find))
			{
				return;
			}
			$this->addError( $attribute,'指定した商品番号はすでに存在しています');
			return;
		}
		return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * URL重複チェック
	 */
	public function checkRulExists($attribute,$params)
	{
		if( $this->scenario == 'insert' )
		{
			$url = $this->{$attribute};
			$find = $this->find('link_url = :link_url', array( ':link_url' => $url ));
			if(is_null($find))
			{
				return;
			}
			$this->addError( $attribute,'指定したURLはすでに存在しています');
			return;
		}
		else if( $this->scenario == 'update')
		{
			$url = $this->{$attribute};
			$find = $this->find('( link_url = :link_url )AND( id <> :id )', array( ':link_url' => $url, ':id' => $this->id ));
			if(is_null($find))
			{
				return;
			}
			$this->addError( $attribute,'指定したURLはすでに存在しています');
			return;
		}
		return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * URLが空のときはitemidを
	 */
	public function setOnEmpty($attribute,$params)
	{
		//空でないときはスルー
		if( $this->$attribute != '' )
		{
			return;
		}
		
		$itemid = $this->item_id;
		if($this->$attribute == '')
		{
			$this->$attribute = $itemid;
		}
		
		
		if($this->$attribute == '')
		{
			$this->addError( $attribute,'URLか商品番号は必須です。');
			return;
		}
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * リンクURL用チェック
	 * @param
	 */
	public function checkLinkFormat($attribute,$params)
	{
		if( $this->$attribute == '' )
		{
			return;
		}
		
		$val = $this->$attribute;
		
		if(preg_match( '/[^0-9a-zA-Z\-\_]/', $val ))
		{
			$this->addError( $attribute,'URLは半角英数「-」「_」で入力してください。');
			return;
		}
		
		return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			
			//所属カテゴリ
			'categories_many' => array(
				self::MANY_MANY,
				'CategoryModel',
				'rel_product_categories(product_id,categories_id)'
			),
			
			
			//SKU
			'sku' => array(
				self::HAS_MANY,
				'ProductSkuModel',
				'product_id',
				'order' => 'rank'
			),
			
			
			//おすすめ関連
			'rel_product_to_product' => array(
				self::MANY_MANY,
				'ProductsModel',
				'rel_product_to_product(products_id,rel_product_id)'
			),
			
			
			//追加画像
			'product_images' => array(
				self::HAS_MANY,
				'ProductImagesModel',
				array( 'product_id' => 'id'),
				'order' => 'rank'
			),
			
			//FAQ
			'faq' => array(
				self::HAS_MANY,
				'FaqModel',
				array( 'product_id' => 'id'),
				'order' => 'rank'
			),
			
			//タグ
			'tags' => array(
				self::HAS_MANY,
				'RelProductTagsModel',
				array( 'product_id' => 'id' )
			),
			
			//オプションSKU
			'optionsku' => array(
				self::HAS_MANY,
				'RelOptionSkuModel',
				array( 'product_id' => 'id' )
			),
			
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'item_id' => '品番',
			'item_name' => '品名',
			'price' => '定価',
			'sale_price' => '売価',
			'open_status' => '公開ステータス',
			'item_status' => '商品ステータスフラグ（新商品など）',
			'postage' => '商品別送料',
			'point_rate' => 'ポイント付与率',
			'shipment' => '発送日目安',
			'qty_limit' => '販売数制限',
			'serch_word' => '検索キーワード（EC-CUBE)用',
			'remark' => '備考',
			'main_view_comment' => '一覧で表示される時の一言コメント',
			'main_detail_comment' => '商品詳細ページで表示される詳細な説明',
			'thumbnail' => '小さい画像',
			'main_image' => 'メインの売りたい画像',
			'main_big_image' => 'メインの拡大画像',
			'sub1_title' => '拡張エリア1 タイトル',
			'sub1_text' => '拡張エリア1 コメント',
			'sub1_img' => '拡張エリア1 画像',
			'sub2_title' => '拡張エリア2 タイトル',
			'sub2_text' => '拡張エリア2 コメント',
			'sub2_img' => '拡張エリア2 画像',
			'sub3_title' => '拡張エリア3 タイトル',
			'sub3_img' => '拡張エリア3 画像',
			'sub3_text' => '拡張エリア3 コメント',
			'sub4_title' => '拡張エリア4 タイトル',
			'sub4_text' => '拡張エリア4 コメント',
			'sub4_img' => '拡張エリア4 画像',
			'sub5_title' => '拡張エリア5 タイトル',
			'sub5_text' => '拡張エリア5 コメント',
			'sub5_img' => '拡張エリア5 画像',
			'sub6_title' => '拡張エリア6タイトル',
			'sub6_text' => '拡張エリア6 コメント',
			'sub6_img' => '拡張エリア6 画像',
			'delete_flag' => '削除フラグ',
			'create_date' => '作成日',
			'update_date' => '更新日',
			'categories' => 'カテゴリ',
			'link_url' => '商品ページURL',
			'page_key' => 'メタキーワード',
			'page_desc' => 'メタ詳細',
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		
		$criteria=new CDbCriteria;
		
		$criteria->compare('id',$this->id,true);
		$criteria->compare('item_id',$this->item_id,true);
		$criteria->compare('item_name',$this->item_name,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('sale_price',$this->sale_price,true);
		$criteria->compare('open_status',$this->open_status);
		$criteria->compare('item_status',$this->item_status,true);
		$criteria->compare('postage',$this->postage,true);
		$criteria->compare('point_rate',$this->point_rate,true);
		$criteria->compare('shipment',$this->shipment,true);
		$criteria->compare('qty_limit',$this->qty_limit,true);
		$criteria->compare('serch_word',$this->serch_word,true);
		$criteria->compare('remark',$this->remark,true);
		$criteria->compare('main_view_comment',$this->main_view_comment,true);
		$criteria->compare('main_detail_comment',$this->main_detail_comment,true);
		$criteria->compare('thumbnail',$this->thumbnail,true);
		$criteria->compare('main_image',$this->main_image,true);
		$criteria->compare('main_big_image',$this->main_big_image,true);
		$criteria->compare('sub1_title',$this->sub1_title,true);
		$criteria->compare('sub1_text',$this->sub1_text,true);
		$criteria->compare('sub1_img',$this->sub1_img,true);
		$criteria->compare('sub2_title',$this->sub2_title,true);
		$criteria->compare('sub2_text',$this->sub2_text,true);
		$criteria->compare('sub2_img',$this->sub2_img,true);
		$criteria->compare('sub3_title',$this->sub3_title,true);
		$criteria->compare('sub3_img',$this->sub3_img,true);
		$criteria->compare('sub3_text',$this->sub3_text,true);
		$criteria->compare('sub4_title',$this->sub4_title,true);
		$criteria->compare('sub4_text',$this->sub4_text,true);
		$criteria->compare('sub4_img',$this->sub4_img,true);
		$criteria->compare('sub5_title',$this->sub5_title,true);
		$criteria->compare('sub5_text',$this->sub5_text,true);
		$criteria->compare('sub5_img',$this->sub5_img,true);
		$criteria->compare('sub6_title',$this->sub6_title,true);
		$criteria->compare('sub6_text',$this->sub6_text,true);
		$criteria->compare('sub6_img',$this->sub6_img,true);
		$criteria->compare('delete_flag',$this->delete_flag);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('update_date',$this->update_date,true);
		
		if(
			(is_array($this->categories_many))&&
			(count($this->categories_many) > 0 )
		)
		{
			$list = array();
			$RelProductCategoriesModel = new RelProductCategoriesModel();
			foreach( $this->categories_many as $one )
			{
				foreach(
					$RelProductCategoriesModel->findAll('categories_id = :categories_id',array(':categories_id' => $one ))
					as
						$row
				)
				{
					$list[] = $row->product_id;
				}
			}
			$list = array_unique($list);
			$where = array();
			$i = 0;
			foreach( $list as $one )
			{
				$criteria->compare( 'id', $one, FALSE, (($i==0)?'AND':'OR'));
				$i ++ ;
			}
			
			
		}
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			)
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * レジビュー用
	 * @return CActiveDataProvider 
	 */
	
	public function regiSearch()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		
		$criteria=new CDbCriteria;
		
		
		
		$criteria->compare('id',$this->id,true);
		$criteria->compare('item_id',$this->item_id,true);
		$criteria->compare('item_name',$this->item_name,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('sale_price',$this->sale_price,true);
		$criteria->compare('open_status',$this->open_status);
		$criteria->compare('item_status',$this->item_status,true);
		$criteria->compare('postage',$this->postage,true);
		$criteria->compare('point_rate',$this->point_rate,true);
		$criteria->compare('shipment',$this->shipment,true);
		$criteria->compare('qty_limit',$this->qty_limit,true);
		$criteria->compare('serch_word',$this->serch_word,true);
		$criteria->compare('remark',$this->remark,true);
		$criteria->compare('main_view_comment',$this->main_view_comment,true);
		$criteria->compare('main_detail_comment',$this->main_detail_comment,true);
		$criteria->compare('thumbnail',$this->thumbnail,true);
		$criteria->compare('main_image',$this->main_image,true);
		$criteria->compare('main_big_image',$this->main_big_image,true);
		$criteria->compare('sub1_title',$this->sub1_title,true);
		$criteria->compare('sub1_text',$this->sub1_text,true);
		$criteria->compare('sub1_img',$this->sub1_img,true);
		$criteria->compare('sub2_title',$this->sub2_title,true);
		$criteria->compare('sub2_text',$this->sub2_text,true);
		$criteria->compare('sub2_img',$this->sub2_img,true);
		$criteria->compare('sub3_title',$this->sub3_title,true);
		$criteria->compare('sub3_img',$this->sub3_img,true);
		$criteria->compare('sub3_text',$this->sub3_text,true);
		$criteria->compare('sub4_title',$this->sub4_title,true);
		$criteria->compare('sub4_text',$this->sub4_text,true);
		$criteria->compare('sub4_img',$this->sub4_img,true);
		$criteria->compare('sub5_title',$this->sub5_title,true);
		$criteria->compare('sub5_text',$this->sub5_text,true);
		$criteria->compare('sub5_img',$this->sub5_img,true);
		$criteria->compare('sub6_title',$this->sub6_title,true);
		$criteria->compare('sub6_text',$this->sub6_text,true);
		$criteria->compare('sub6_img',$this->sub6_img,true);
		$criteria->compare('delete_flag',$this->delete_flag);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('update_date',$this->update_date,true);
		
		if(
			(is_array($this->categories_many))&&
			(count($this->categories_many) > 0 )
		)
		{
			$list = array();
			$RelProductCategoriesModel = new RelProductCategoriesModel();
			foreach( $this->categories_many as $one )
			{
				foreach(
					$RelProductCategoriesModel->findAll('categories_id = :categories_id',array(':categories_id' => $one ))
					as
						$row
				)
				{
					$list[] = $row->product_id;
				}
			}
			$list = array_unique($list);
			$where = array();
			$i = 0;
			foreach( $list as $one )
			{
				$criteria->compare( 'id', $one, FALSE, (($i==0)?'AND':'OR'));
				$i ++ ;
			}
			
			
		}
		
		
		return new CActiveDataProvider(
			$this,
			array(
				'criteria'=>$criteria,
			)
		);
		//ProductsModel_pageでページサイズ指定
	}
	
	
	// ----------------------------------------------------
	
	/**
	 * 小商品
	 */
	public function get_sku()
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 'product_id=:product_id';
		$criteria->params = array( 'product_id' => $this->id );
		
		return new CActiveDataProvider(
			'ProductSkuModel',
			array(
				'criteria'=>$criteria,
			)
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductsModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	// ----------------------------------------------------
	
	/**
	 * ひさてるさんのMANYサポートを利用
	 */
	public function behaviors()
	{
		return array(
			'manyManySupport'=>array(
				'class' => 'ManyManySupport',
			),
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * 処理後
	 */
	public function afterSave()
	{
		//カテゴリリスト登録

		$ProductsModel = input::Post('ProductsModel');
		if(isset($ProductsModel['categories_many'])){
			$lists = $_POST['ProductsModel']['categories_many'];
		}
		else
		{
			$lists = array();
		}
		
		$relProductCategoriesModel = new RelProductCategoriesModel();
		$relProductCategoriesModel->deleteAll(
					'product_id = :product_id',
					array(
						'product_id' => $this->id
					)
				);
		$categoryModel = new CategoryModel();
		foreach( $lists as $one )
		{
			$relProductCategoriesModel2 = new RelProductCategoriesModel();
			$relProductCategoriesModel2->categories_id = $one;
			$relProductCategoriesModel2->product_id = $this->id;
			$relProductCategoriesModel2->save();
		}
		
		
	}
	
	// ----------------------------------------------------
}
