<?php

/**
 * This is the model class for table "order_history".
 *
 * The followings are the available columns in table 'order_history':
 * @property string $id
 * @property string $customer_id
 * @property string $order_date
 * @property string $update_date
 * @property integer $order_status
 * @property integer $payment_status
 * @property integer $item_total
 * @property string $payment_method
 * @property integer $payment_fee
 * @property integer $grand_total
 * @property string $payment_date
 * @property string $canceld_date
 * @property string $name1
 * @property string $name2
 * @property string $kana1
 * @property string $kana2
 * @property string $zip
 * @property string $pref
 * @property string $address1
 * @property string $address2
 * @property string $address3
 * @property string $tel
 * @property string $fax
 * @property string $d_name1
 * @property string $d_name2
 * @property string $d_kana1
 * @property string $d_kana2
 * @property string $d_zip
 * @property string $d_pref
 * @property string $d_address1
 * @property string $d_address2
 * @property string $d_address3
 * @property string $d_tel
 */
class OrderHistoryModel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('order_date, update_date, order_status, payment_status', 'required'),
			array('order_status, payment_status, item_total, payment_fee, grand_total', 'numerical', 'integerOnly'=>true),
			array('customer_id, payment_method', 'length', 'max'=>10),
			array('name1, name2, kana1, kana2, pref, address1, address2, address3, d_name1, d_name2, d_kana1, d_kana2, d_pref, d_address1, d_address2, d_address3', 'length', 'max'=>125),
			array('zip, tel, fax, d_zip, d_tel', 'length', 'max'=>45),
			array('payment_date, canceld_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, customer_id, order_date, update_date, order_status, payment_status, item_total, payment_method, payment_fee, grand_total, payment_date, canceld_date, name1, name2, kana1, kana2, zip, pref, address1, address2, address3, tel, fax, d_name1, d_name2, d_kana1, d_kana2, d_zip, d_pref, d_address1, d_address2, d_address3, d_tel', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			
			//明細項目
			'detail' => array(
				self::HAS_MANY,
				'OrderDetailModel',
				'order_id'
			),
			
			//顧客
			'customer' => array(
				self::BELONGS_TO,
				'CustomersModel',
				'customer_id',
			),
			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'customer_id' => '顧客ID',
			'order_date' => 'オーダー日',
			'update_date' => 'アップデート日',
			'order_status' => 'オーダステータス -2:問題あり顧客 -1:キャンセル 1:仮受注 2:受注 3:確認済み 4:在庫割り当て 5:発送完了',
			'payment_status' => '支払ステータス 0:未支払い 1:支払い',
			'item_total' => '商品代金合計',
			'payment_method' => '支払方法コード（システムに従う）',
			'payment_fee' => '支払い手数料',
			'grand_total' => '支払い総合計',
			'payment_date' => '支払い実行日',
			'canceld_date' => 'キャンセル実行日',
			'name1' => '姓',
			'name2' => '名',
			'kana1' => 'かな（姓）',
			'kana2' => 'かな（名）',
			'zip' => '郵便番号',
			'pref' => '都道府県',
			'address1' => '住所1',
			'address2' => '住所2',
			'address3' => '住所3',
			'tel' => '電話',
			'fax' => 'fax',
			'd_name1' => '姓',
			'd_name2' => '名',
			'd_kana1' => 'かな（姓）',
			'd_kana2' => 'かな（名）',
			'd_zip' => '郵便番号',
			'd_pref' => '都道府県',
			'd_address1' => '住所1',
			'd_address2' => '住所2',
			'd_address3' => '住所3',
			'd_tel' => '電話',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('customer_id',$this->customer_id,true);
		$criteria->compare('order_date',$this->order_date,true);
		$criteria->compare('update_date',$this->update_date,true);
		$criteria->compare('order_status',$this->order_status);
		$criteria->compare('payment_status',$this->payment_status);
		$criteria->compare('item_total',$this->item_total);
		$criteria->compare('payment_method',$this->payment_method,true);
		$criteria->compare('payment_fee',$this->payment_fee);
		$criteria->compare('grand_total',$this->grand_total);
		$criteria->compare('payment_date',$this->payment_date,true);
		$criteria->compare('canceld_date',$this->canceld_date,true);
		$criteria->compare('name1',$this->name1,true);
		$criteria->compare('name2',$this->name2,true);
		$criteria->compare('kana1',$this->kana1,true);
		$criteria->compare('kana2',$this->kana2,true);
		$criteria->compare('zip',$this->zip,true);
		$criteria->compare('pref',$this->pref,true);
		$criteria->compare('address1',$this->address1,true);
		$criteria->compare('address2',$this->address2,true);
		$criteria->compare('address3',$this->address3,true);
		$criteria->compare('tel',$this->tel,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('d_name1',$this->d_name1,true);
		$criteria->compare('d_name2',$this->d_name2,true);
		$criteria->compare('d_kana1',$this->d_kana1,true);
		$criteria->compare('d_kana2',$this->d_kana2,true);
		$criteria->compare('d_zip',$this->d_zip,true);
		$criteria->compare('d_pref',$this->d_pref,true);
		$criteria->compare('d_address1',$this->d_address1,true);
		$criteria->compare('d_address2',$this->d_address2,true);
		$criteria->compare('d_address3',$this->d_address3,true);
		$criteria->compare('d_tel',$this->d_tel,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderHistoryModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
