<?php
/* =============================================================================
 * ファイルアップロード用モデル
 * ========================================================================== */
class FileUploadModel extends CFormModel
{
	public $image;
	
	// ----------------------------------------------------
	
	/**
	 * 
	 * @return typeルール
	 */
	
	public function rules()
	{
		$imageRule = array(
			'image',
			'file',
			'safe' => false,
			'types' => 'avi, mov, mp4, jpeg, jpg, png, gif',
			//'mimeTypes' => 'image/gif, image/jpeg, image/png, image/x-png',
			'message' => '画像が選択されていません。',
		);
		return array(
			$imageRule,
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * 画像リサイズ
	 * @param type $file
	 * @return boolean
	 */
	
	public function imageResize($file)
	{
		$resize_x = Yii::app()->params['resize_x'];
		$img_org = getimagesize($file);
		
		$exif = exif_read_data($file);
		
		// サイズ決定
		if($img_org[0] > $resize_x)
		{
			$img_re_x = $resize_x;
			$img_re_y = ceil($img_org[1] * ($resize_x / $img_org[0]));
		}
		else
		{
			$img_re_x = $img_org[0];
			$img_re_y = $img_org[1];
		}
		
		// 処理方法選定
		$matches = array();
		preg_match('/(jpeg|gif|png)$/', $img_org['mime'], $matches);
		
		// mimeタイプ別に処理
		$res = array();
		switch($matches[0])
		{
			case 'jpeg':
				$org = imagecreatefromjpeg($file);
				
				$resize = imagecreatetruecolor($img_re_x, $img_re_y);
				imagecopyresampled($resize, $org, 0, 0, 0, 0, $img_re_x, $img_re_y, $img_org[0], $img_org[1]);
				
				
				//回転
				if( ($exif !== FALSE )&&(isset($exif['Orientation']))&&( $exif['Orientation'] != 1 ))
				{
					if( $exif['Orientation'] == 3 )
					{
						$resize = imagerotate($resize, 180, 0 );
					}
					if( $exif['Orientation'] == 6 )
					{
						$resize = imagerotate($resize, 270, 0 );
					}
					if( $exif['Orientation'] == 8 )
					{
						$resize = imagerotate($resize, 90, 0 );
					}
				}
				
				
				$res = array(
					'type' => 'jpg',
					'image' => $resize
				);
			break;
			case 'gif':
				$org = imagecreatefromgif($file);
				
				$resize = imagecreatetruecolor($img_re_x, $img_re_y);
				$alpha = imagecolortransparent($org);
				imagefill($resize, 0, 0, $alpha);
				imagecolortransparent($resize, $alpha);
				
				imagecopyresampled($resize, $org, 0, 0, 0, 0, $img_re_x, $img_re_y, $img_org[0], $img_org[1]);
				
				$res = array(
					'type' => 'gif',
					'image' => $resize
				);
			break;
			case 'png':
				$org = imagecreatefrompng($file);
				
				$resize = imagecreatetruecolor($img_re_x, $img_re_y);
				imagealphablending($resize, false);
				imagesavealpha($resize, true);
				imagecopyresampled($resize, $org, 0, 0, 0, 0, $img_re_x, $img_re_y, $img_org[0], $img_org[1]);
				
				$res = array(
					'type' => 'png',
					'image' => $resize
				);
			break;
			default: 
				$res = false;
			break;
		}
		imagedestroy($org);
		return $res;
	}
}