<?php

/**
 * This is the model class for table "categories".
 *
 * The followings are the available columns in table 'categories':
 * @property string $id
 * @property string $parent_id
 * @property string $categori_name
 * @property string $create_date
 * @property string $update_date
 * @property string $rank
 * @property string $sort_key
 * @property string $main_image
 * @property string $thumb_image
 * @property string $description
 */
class CategoryModel extends CActiveRecord
{
	
	public $allrank;
	public $maximam;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'categories';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			
			array('parent_id, categori_name, link_url', 'required'),
			array('parent_id', 'length', 'max'=>10),
			array('link_url', 'length', 'max'=>30),
			array('link_url', 'checkLinkFormat'),
			
			array( 'page_key', 'safe' ),
			array( 'page_desc', 'safe' ),
			
			array('categori_name', 'length', 'max'=>255),
			array('create_date, update_date, description, main_image, thumb_image', 'safe'),
			
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, parent_id, categori_name, create_date, update_date', 'safe', 'on'=>'search'),
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * リンクURL用チェック
	 * @param
	 */
	public function checkLinkFormat($attribute,$params)
	{
		if( $this->$attribute == '' )
		{
			return;
		}
		
		$val = $this->$attribute;
		
		if(preg_match( '/[^0-9a-zA-Z\-\_]/', $val ))
		{
			$this->addError( $attribute,'URLは半角英数「-」「_」で入力してください。');
			return;
		}
		
		return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			
			//親子関係
			'children' => array(
				self::HAS_MANY,
				'CategoryModel',
				'parent_id'
			),
			
			//親
			'parent' => array(
				self::BELONGS_TO,
				'CategoryModel',
				'parent_id'
			),
			
			//商品リレーション
			'rel' => array(
				self::HAS_MANY,
				'RelProductCategoriesModel',
				'categories_id'
			),
			
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parent_id' => '親カテゴリID',
			'categori_name' => 'カテゴリ名',
			'create_date' => '作成日',
			'update_date' => '更新日',
			'rank' => '表示順',
			'sort_key' => '表示順（ソート用）',
			'main_image' => 'メインイメージ',
			'thumb_image' => 'サムネイル',
			'description' => '説明',
			'link_url' => 'カテゴリURL',
			'page_key' => 'メタキーワード',
			'page_desc' => 'メタ詳細',
			
		);
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		
		$criteria = new CDbCriteria;
		$criteria->select = array(
				'*', 
				//new CDbExpression("IF( parent.rank IS NOT NULL, CONCAT( parent.rank, '-', parent.id , '-', t.rank, '-', t.id ), CONCAT( t.rank , '-', t.id )  ) AS allrank"),
			);
		$criteria->with = array( 'parent' );
		$criteria->compare('id',$this->id,true);
		$criteria->compare('parent_id',$this->parent_id,true);
		$criteria->compare('categori_name',$this->categori_name,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('update_date',$this->update_date,true);
		$criteria->order = 't.sort_key';
		//$criteria->condition = 't.parent_id=0';
		$criteria->together=true;
		
		return new CActiveDataProvider(
				$this,
				array(
					'criteria'=>$criteria,
				)
			);
	}
	
	// ----------------------------------------------------
	
	/**
	 * 親子データを取得
	 */
	public function getAllSearch()
	{
		$criteria = new CDbCriteria;
		$criteria->select = array(
				'*', 
				new CDbExpression("IF( parent.rank IS NOT NULL, CONCAT( parent.rank, '-', parent.id , '-', t.rank, '-', t.id ), CONCAT( t.rank , '-', t.id )  ) AS allrank"),
			);
		$criteria->with = array( 'parent' );
		$criteria->compare('id',$this->id,true);
		$criteria->order = 'allrank';
		$criteria->together=true;
		
		return $this->findAll( $criteria );
	}
	
	
	// ----------------------------------------------------
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CategoryModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	// ----------------------------------------------------
	
	/**
	 * コンディションリスト
	 */
	public function scopes()
	{
		return array(
			/*
			'published'=>array(
				'condition'=>'status=1',
			),
			'recently'=>array(
				'order'=>'create_time DESC',
				'limit'=>5,
			),
			*/
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * 現在のデータの深さ指定
	 * @param boolean $update アップデートモードか？
	 */
	public function set_depth( $update = FALSE )
	{
		//ランク設定
		if( $update === FALSE )
		{
			$criteria=new CDbCriteria;
			$criteria->select = 'MAX(t.rank) AS maximam';
			$criteria->condition = 'parent_id=:parent_id';
			$criteria->params=array(':parent_id'=>$this->parent_id);
			$one = $this->find($criteria);
			//var_dump($one->maximam);
			//exit();
			if( $one->maximam == null )
			{
				$this->rank = 0;
			}
			else
			{
				$this->rank = $one->maximam + 1;
			}
			
			
		}
		if( $this->parent_id == 0 )
		{
			$this->sort_key = sprintf('%05d', $this->rank );
			return;
		}
		
		
		//親IDのデータを取得
		$row = $this->find(
					'id = :id',
					array(
						'id' => $this->parent_id
					)
				);
		
		$this->depth = $row->depth + 1;
		$this->sort_key = $row->sort_key . '-' . sprintf('%05d', $this->rank );
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * カテゴリリスト取得
	 * @param int $parent_id 親ID
	 * @return array
	 */
	public function getCategoryList()
	{
		//所属データを抜き出し
		$criteria=new CDbCriteria;
		$criteria->select = '*';
		$criteria->order = 't.sort_key';
		$list = $this->findAll( $criteria );
		return $list;
	}
	
	// ----------------------------------------------------
	
	/**
	 * セーブ後自動実装
	 */
	public function afterSave()
	{
		
		//データの保存
		if( $this->isNewRecord )
		{
			//$ar = CategoryModel::model()->findByPk($this->id);
			//$ar->sort_key = $sort;
			//$ar->save();
		}
		else
		{
			//saveAttributesはafterSaveが起きない
			//$this->saveAttributes( array( 'sort_key' => $sort ) );
		}
		
		
		parent::afterSave();
	}
	
	// ----------------------------------------------------
	
	/**
	 * ランク変動
	 * @param int $id
	 * @param string $mode
	 * @return void
	 */
	public function move_rank( $id, $mode )
	{
		//ランク変動の実装
		$row = $this->find( 'id=:id', array('id'=>$id) );
		
		
		//セットするランクの取得/例外処理/上下のレコード
		if( $mode == 'up')
		{
			$set_rank = $row->rank - 1;
			
			//現在ランクが一番上なら処理不要
			if( $set_rank < 0 )
			{
				return;
			}
			
		}
		else
		{
			$set_rank = $row->rank + 1;
			
			//現在ランクが一番下なら処理不要
			$criteria=new CDbCriteria;
			$criteria->select = 'MAX(t.rank) AS maximam';
			$criteria->condition = 'parent_id=:parent_id';
			$criteria->params=array(':parent_id' => $row->parent_id);
			$one = $this->find($criteria);
			if( $one->maximam == $this->rank )
			{
				return;
			}
		}
		
		//上下のレコードを取得
		$move_record2 = $this->find( ' (rank=:rank) AND ( parent_id=:parent_id)', array( 'rank' => $set_rank, 'parent_id' => $row->parent_id ) );
		$move_record2->rank = $row->rank;
		
		
		
		//親IDのデータを取得
		$parent = $this->find(
					'id = :id',
					array(
						'id' => $row->parent_id
					)
				);
		
		
		//ソートキーの作成
		$row->rank = $set_rank;
		if( $parent != null )
		{
			$row->sort_key = $parent->sort_key . '-' . sprintf('%05d', $row->rank );
		}
		else
		{
			$row->sort_key = sprintf('%05d', $row->rank );
		}
		$row->save();
		
		
		//子カテゴリの書き換え
		$this->rewriteChildrenSortkey( $row->id, $row->sort_key );
		
		
		
		
		////上下レコードの書き換え
		
		//上下レコードの親を取得
		$parent = $this->find(
					'id = :id',
					array(
						'id' => $move_record2->parent_id
					)
				);
		if( $parent != null )
		{
			$move_record2->sort_key = $parent->sort_key . '-' . sprintf('%05d', $move_record2->rank );
		}
		else
		{
			$move_record2->sort_key = sprintf('%05d', $move_record2->rank );
		}
		$move_record2->save();
		
		
		//上下レコードの小カテゴリの書き換え
		$this->rewriteChildrenSortkey( $move_record2->id, $move_record2->sort_key );
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * 小カテゴリのソートキーを書き換え
	 * @param int $id 所属キー
	 * @param string $sort_key 書き換え用ソートキー
	 * @return void
	 */
	public function rewriteChildrenSortkey( $id, $sort_key )
	{
		
		//所属データを抜き出し
		$criteria=new CDbCriteria;
		$criteria->select = '*';
		$criteria->condition = 'parent_id=:parent_id';
		$criteria->params=array(':parent_id' => $id );
		$list = $this->findAll( $criteria );
		
		
		foreach( $list as $row )
		{
			$row->sort_key = $sort_key . '-' . sprintf('%05d', $row->rank );
			$row->save();
			$this->rewriteChildrenSortkey( $row->id, $row->sort_key );
		}
		
		return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * セーブ前親カテゴリを取得
	 * @param int $id
	 * @return int
	 */
	public function getMotoParent($id)
	{
		$row = $this->find( 'id=:id', array( 'id' => $id ) );
		return $row->parent_id;
	}
	// ----------------------------------------------------
	
	/**
	 * 親カテゴリを変更したとき用のならべかえ設定
	 * @param boolean $change 親カテゴリ変更があったか？
	 */
	public function setInSaveSort( $change )
	{
		$criteria=new CDbCriteria;
		$criteria->select = '*';
		
		if($change === true )
		{
			$criteria->condition = ' ( id <> :id ) AND ( parent_id = :parent_id )';
			$criteria->params = array( 'id'=>$this->id, 'parent_id'=>$this->parent_id );
		}
		else
		{
			$criteria->condition = 'parent_id = :parent_id';
			$criteria->params = array( 'parent_id'=>$this->parent_id );
		}
		
		
		$criteria->order = 't.rank';
		$list = $this->findAll( $criteria );
		$i = 0;
		
		
		
		foreach( $list as $row )
		{
			//echo $row->id . '<br />';
			//echo $row->rank . '<br />';
			
			//ランクが違えば書き換え
			if( $i != $row->rank )
			{
				$row->rank = $i;
			}
			$parent = $this->find(
				'id = :id',
				array(
					'id' => $row->parent_id
				)
			);
			if(!is_null($parent))
			{
				$row->sort_key = $parent->sort_key . '-' . sprintf('%05d', $row->rank );
			}
			else
			{
				$row->sort_key = sprintf('%05d', $row->rank );
			}
			//echo $row->sort_key . '<br /><br />';
			$row->save();
			
			
			$this->rewriteChildrenSortkey( $row->id, $row->sort_key);
			
			$i ++ ;
		}
		
		
		//カテゴリ変更があった場合は自分を書き換え
		if( $change === true )
		{
			$this->rank = $i;
			$parent = $this->find(
				'id = :id',
				array(
					'id' => $this->parent_id
				)
			);
			
			if(!is_null($parent))
			{
				$this->sort_key = $parent->sort_key . '-' . sprintf('%05d', $this->rank );
			}
			else
			{
				$row->sort_key = sprintf('%05d', $this->rank );
			}
			$this->save();
			
		}
		
		
		//exit();
	}
	
	// ----------------------------------------------------
	
	/**
	 * カテゴリリスト
	 * @param $start_id 起点
	 */
	public function getCategoryListAll( $start_id = 0 )
	{
		return $this->getCategoryListLoop( $start_id );
	}
	
	protected function getCategoryListLoop( $parent_id )
	{
		$return = $this->findAll( 'parent_id = :parent_id', array( ':parent_id' => $parent_id ) );
		$i = 0;
		foreach( $return as $row )
		{
			$return[$i]->children = $this->getCategoryListLoop( $row->id );
			$i ++ ;
		}
		return $return;
	}
	
	
	// ----------------------------------------------------
	
	/**
	 * 指定カテゴリ以下属している商品を取得
	 * @param string $name
	 * @param array(ref) $list
	 * @param int $limit
	 * @return array(ref) $list
	 */
	public function getBelongtoList( $name, &$list, $limit = 5 )
	{
		//親子関係をすべて取得
		$target_list = array();
		$row = $this->find( 'link_url = :link_url', array(':link_url' => $name ) );
		$target_list[] = $row->id;
		$target_list = array_merge( $target_list, $this->getBelongTarget( $row->id ) );
		if( count($target_list) == 0 )
		{
			return array();
		}
		
		
		//ターゲットになるものを取得
		$RelProductCategoriesModel = new RelProductCategoriesModel();
		$criteria = new CDbCriteria();
		$criteria->limit = $limit;
		$criteria->join = 'RIGHT JOIN products P ON P.id = product_id';
		$criteria->order = 'P.create_date DESC';
		$criteria->select = 'product_id' ;
		$criteria->distinct = true;
		$in = array();
		$params = array();
		foreach( $target_list as $one )
		{
			$in[] = ' ? ';
			$params[] = $one;
		}
		$criteria->condition = 'categories_id IN( ' . implode( ',', $params ) . ' ) ';
		$criteria->params = $params;
		$rows = $RelProductCategoriesModel->findAll( $criteria );
		
		
		//商品情報
		$list = array();
		foreach( $rows as $row )
		{
			$ProductsModel = new ProductsModel();
			$pro = $ProductsModel->find( 'id =:id ', array( ':id' => $row->product_id ) );
			if(!is_null($pro))
			{
				$list[] = $pro;
			}
		}
		
		
		return $list;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 取得元ターゲットの子リスト
	 * @param string $id
	 * @return array
	 */
	protected function getBelongTarget( $id )
	{
		$return = array();
		$rows = $this->findAll( 'parent_id = :id', array( ':id' => $id ) );
		foreach( $rows as $row )
		{
			$return[] = $row->id;
			$children = $this->getBelongTarget( $row->id );
			$return = array_merge( $return, $children );
		}
		return $return;
	}
	
	// ----------------------------------------------------
}
