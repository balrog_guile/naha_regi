<?php

/**
 * This is the model class for table "comming_assets".
 *
 * The followings are the available columns in table 'comming_assets':
 * @property string $id
 * @property string $comming_id
 * @property integer $data_type
 * @property string $path
 * @property string $memo1
 * @property string $create_date
 * @property string $update_date
 */
class CommingAssetsModel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'comming_assets';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('comming_id, data_type, path', 'required'),
			array('data_type', 'numerical', 'integerOnly'=>true),
			array('comming_id', 'length', 'max'=>10),
			array('path', 'length', 'max'=>255),
			array('memo1, create_date, update_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, comming_id, data_type, path, memo1, create_date, update_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'comming_id' => '来店ID',
			'data_type' => 'データのタイプ
0:画像
1:動画
2:その他',
			'path' => '保存パス',
			'memo1' => 'メモ',
			'create_date' => '作成日',
			'update_date' => '更新日',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('comming_id',$this->comming_id,true);
		$criteria->compare('data_type',$this->data_type);
		$criteria->compare('path',$this->path,true);
		$criteria->compare('memo1',$this->memo1,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('update_date',$this->update_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CommingAssetsModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
