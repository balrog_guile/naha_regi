<?php

/**
 * This is the model class for table "shop".
 *
 * The followings are the available columns in table 'shop':
 * @property string $id
 * @property string $name
 * @property string $address1
 * @property string $address2
 * @property string $address3
 * @property string $tel
 * @property string $fax
 * @property string $mailaddress
 * @property integer $rank
 * @property string $create_date
 */
class ShopModel extends CActiveRecord
{
	
	// ----------------------------------------------------
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'shop';
	}
	
	// ----------------------------------------------------
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('rank', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>125),
			array('address1, address2, mailaddress', 'length', 'max'=>255),
			array('address3, tel, fax', 'length', 'max'=>45),
			array('create_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, address1, address2, address3, tel, fax, mailaddress, rank, create_date', 'safe', 'on'=>'search'),
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'ショップ名',
			'address1' => '住所1',
			'address2' => '住所2',
			'address3' => '住所3',
			'tel' => '電話',
			'fax' => 'fax',
			'mailaddress' => '代表メールアドレス',
			'rank' => '表示順',
			'create_date' => '作成日',
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('address1',$this->address1,true);
		$criteria->compare('address2',$this->address2,true);
		$criteria->compare('address3',$this->address3,true);
		$criteria->compare('tel',$this->tel,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('mailaddress',$this->mailaddress,true);
		$criteria->compare('rank',$this->rank);
		$criteria->compare('create_date',$this->create_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ShopModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	// ----------------------------------------------------
	
	/**
	 * ドロップダウン用リストを取得
	 * @return array
	 */
	public static function getShopDropDownList()
	{
		$return = array();
		foreach(
			self::model()->findAll( array( 'order' => 'rank' ) )
			as
			$one
		)
		{
			$return[$one->id] = $one->name;
		}
		
		return $return;
	}
	
	// ----------------------------------------------------
}
