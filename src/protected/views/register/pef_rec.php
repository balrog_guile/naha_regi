<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>無題ドキュメント</title>
<style>
body {
	font-size: 14px;
}
.title {
	font-size: 2em;
}
.em80{
	font-size: 0.8em;
}
.em85{
	font-size: 0.85em;
}
.em90{
	font-size: 0.9em;
}
.em120{
	font-size: 1.20em;
}
.em150{
	font-size: 1.50em;
}
.underline{
	text-decoration: underline;
}
</style>
</head>

<!-- <body onload="window.print();"> -->
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="3">
	<tr>
		<td align="right" scope="row" class="em80">請求書番号:<?php echo $receipt->id; ?></td>
	</tr>
	<tr>
		<th bgcolor="#EFEFEF" scope="row" class="title">領収書（兼納品書）</th>
	</tr>
	<tr>
		<td align="right" scope="row" class="em80">
			<?php echo date( 'Y年m月d日', strtotime($receipt->payment_date) ); ?>
		</td>
	</tr>
</table>
<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="left" valign="top" scope="row">
			<span class="em150 underline">　　　　　　　　　　様</span><br />
			件名：<?php echo $receipt->receipt_name; ?><br />
			について、確かに領収いたしました
			
			
		</td>
		<td width="3%">&nbsp;</td>
		<td width="32%" align="left" valign="top" class="em80">LaDea<br />
			名古屋市西区あっちそっち1-0-0<br />
			◯◯◯マンション2F<br />			
			担当：◯◯<br /></td>
	</tr>
</table>
<br />
<table width="100%" border="0" cellpadding="3" cellspacing="1" style="background-color:#666;">
	<tr>
		<th width="20%" align="center" valign="middle" bgcolor="#EFEFEF" scope="row">商品代金合計</th>
		<td align="center" bgcolor="#FFFFFF">
			<?php echo number_format($receipt->item_total); ?>円
		</td>
	</tr>
	<tr>
		<th align="center" valign="middle" bgcolor="#EFEFEF" scope="row">消費税</th>
		<td align="center" bgcolor="#FFFFFF">
			<?php echo number_format($receipt->tax); ?>円
		</td>
	</tr>
	<!--
	<tr>
		<th align="center" valign="middle" bgcolor="#EFEFEF" scope="row">手数料など</th>
		<td align="center" bgcolor="#FFFFFF">
			
		</td>
	</tr>
	-->
	<tr>
		<th align="center" valign="middle" bgcolor="#EFEFEF" scope="row">領収合計額</th>
		<td align="center" bgcolor="#FFFFFF">
			<?php echo number_format($receipt->grand_total); ?>円
		</td>
	</tr>
</table>
<br />
<table width="100%" border="0" cellspacing="1" cellpadding="3" style="background-color:#666;">
	<tr>
		<th width="60%" bgcolor="#EFEFEF" scope="row">品名</th>
		<th width="15%" bgcolor="#EFEFEF">単価</th>
		<th width="10%" bgcolor="#EFEFEF">数量</th>
		<th width="15%" bgcolor="#EFEFEF">小計</th>
	</tr>
	<?php $cnt = count($receipt->detail); for( $i = 0; $i < 15; $i ++ ): ?>
		
		<?php if( $cnt > $i ): ?>
			<tr>
				<td align="left" valign="top" bgcolor="#FFFFFF" scope="row">
					<?php echo $receipt->detail[$i]->item_id; ?> 
					<?php echo $receipt->detail[$i]->item_name; ?>
				</td>
				<td align="center" valign="middle" bgcolor="#FFFFFF">
					<?php echo number_format($receipt->detail[$i]->price); ?>円
				</td>
				<td align="center" valign="middle" bgcolor="#FFFFFF">
					<?php echo number_format($receipt->detail[$i]->qty); ?>個
				</td>
				<td align="center" valign="middle" bgcolor="#FFFFFF">
					<?php echo number_format($receipt->detail[$i]->sub_total); ?>円
				</td>
			</tr>
		<?php else: ?>
			<tr>
				<td align="left" valign="top" bgcolor="#FFFFFF" scope="row">
					　
				</td>
				<td align="center" valign="middle" bgcolor="#FFFFFF">　</td>
				<td align="center" valign="middle" bgcolor="#FFFFFF">　</td>
				<td align="center" valign="middle" bgcolor="#FFFFFF">　</td>
			</tr>
		<?php endif; ?>
	<?php endfor; ?>
</table>
</body>
</html>
