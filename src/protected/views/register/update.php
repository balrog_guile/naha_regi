<?php
/* @var $this RegisterController */
/* @var $model ReceiptModel */

$this->breadcrumbs=array(
	'Receipt Models'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ReceiptModel', 'url'=>array('index')),
	array('label'=>'Create ReceiptModel', 'url'=>array('create')),
	array('label'=>'View ReceiptModel', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ReceiptModel', 'url'=>array('admin')),
);
?>

<h1>Update ReceiptModel <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>