<?php
/* =============================================================================
 * 顧客フィールド管理
 * ========================================================================== */
$this->breadcrumbs=array();

$this->menu=array();
?>
<h1>顧客フィールド管理</h1>


<div>
	<?php echo CHtml::button( '現在状態を反映', array('class' => 'btn btn-large btn-info reflection') ); ?>
</div>


<div class="row-fluid sortable ui-sortable">
	<div class="box span12">
		<div class="box-header well" data-original-title="">
			<h2>
				<i class="icon-user"></i>
				Members
			</h2>
			<div class="box-icon">
				<a class="btn btn-setting btn-round" href="#">
					<i class="icon-cog"></i>
				</a>
				<a class="btn btn-minimize btn-round" href="#">
					<i class="icon-chevron-up"></i>
				</a>
				<a class="btn btn-close btn-round" href="#">
					<i class="icon-remove"></i>
				</a>
			</div>
		</div>
		<div class="box-content">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>
							フィールド名<br />
							フィールドラベル<br />
							フィールドタイプ<br />
							入力欄タイプ
							<hr />
							初期値<br />
							選択肢
						</th>
						<th>
							表示制御<hr />HTML制御(上級者向け）
						</th>
						<th>
							入力制御
						</th>
						<th>
							操作
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<?php echo CHtml::textField( 'field_name[]', '', array( 'class' => 'IM[customer_profile_field@field_name]' ) ); ?><br />
							<?php echo CHtml::textField( 'field_label[]', '', array( 'class' => 'IM[customer_profile_field@field_label]' ) ); ?><br />
							<?php echo CHtml::dropDownList( 'field_type[]', '', VarHelper::getCustomerFieldTypes(), array( 'class' => 'IM[customer_profile_field@field_type]') ); ?><br />
							<?php echo CHtml::dropDownList( 'input_type[]', '', VarHelper::getCustomerFieldInputType(), array( 'class' => 'IM[customer_profile_field@input_type]') ); ?>
							<hr />
							<label>初期値</label>
							<?php echo CHtml::textField( 'default_value[]', '', array( 'class' => 'IM[customer_profile_field@default_value]' ) ); ?>
							
							<label>選択肢 改行区切りで入力</label>
							<?php echo CHtml::textArea( 'selecte_column[]', '', array( 'class' => 'IM[customer_profile_field@selecte_column] selecte_column' ) ); ?>
							
						</td>
						<td>
							<label>
								<?php echo CHtml::checkBox( 'search_flag[]', FALSE, array( 'data-no-uniform' => 'true', 'value' => '1', 'class' => 'IM[customer_profile_field@search_flag]') ); ?>
								検索対象
							</label>
							<label>
								<?php echo CHtml::checkBox( 'view_display[]', FALSE, array( 'data-no-uniform' => 'true', 'value' => '1', 'class' => 'IM[customer_profile_field@view_display]') ); ?>
								顧客管理画面一覧に表示
							</label>
							<label>
								<?php echo CHtml::checkBox( 'summary_display[]', FALSE, array( 'data-no-uniform' => 'true', 'value' => '1', 'class' => 'IM[customer_profile_field@summary_display]') ); ?>
								顧客概要に表示
							</label>
							<hr />
							<label>HTML制御(上級者向け)</label>
							<label>セレクタ - class</label>
							<?php echo CHtml::textField( 'html_class[]', '', array( 'class' => 'IM[customer_profile_field@html_class]' ) ); ?>
							
							<label>セレクタ - ID</label>
							<?php echo CHtml::textField( 'html_id[]', '', array( 'class' => 'IM[customer_profile_field@html_id]' ) ); ?>
							
						</td>
						<td>
							<label>
								<?php echo CHtml::checkBox( 'required[]', FALSE, array( 'data-no-uniform' => 'true', 'value' => '1', 'class' => 'IM[customer_profile_field@required]') ); ?>
								必須にする
							</label>
							<label>
								最小長
								<?php echo CHtml::textField( 'min_length[]', '' , array( 'class' => 'IM[customer_profile_field@min_length] input-small' ) ); ?>
								文字
							</label>
							<label>
								最大長
								<?php echo CHtml::textField( 'max_length[]', '' , array( 'class' => 'IM[customer_profile_field@max_length] input-small' ) ); ?>
								文字
							</label>
							<hr />
							<label>以下上級者向け</label>
							
							<label>
								正規表現で判定(preg エラー判定ではなく真判定を入力 )
							</label>
							<?php echo Chtml::textArea( 'regrex[]', '', array( 'class' => 'IM[customer_profile_field@regrex]' ) ); ?>
							
							<label>
								その他 yiiのその他のバリデーションを実行
							</label>
							<?php echo Chtml::textArea( 'other_validation[]', '', array( 'class' => 'IM[customer_profile_field@other_validation]' ) ); ?>
							
						</td>
						<td>
							</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>