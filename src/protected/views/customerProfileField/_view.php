<?php
/* @var $this CustomerProfileFieldController */
/* @var $data CustomerProfileFieldModel */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('field_name')); ?>:</b>
	<?php echo CHtml::encode($data->field_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('field_type')); ?>:</b>
	<?php echo CHtml::encode($data->field_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('input_type')); ?>:</b>
	<?php echo CHtml::encode($data->input_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('selecte_column')); ?>:</b>
	<?php echo CHtml::encode($data->selecte_column); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('html_class')); ?>:</b>
	<?php echo CHtml::encode($data->html_class); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('html_id')); ?>:</b>
	<?php echo CHtml::encode($data->html_id); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('search_flag')); ?>:</b>
	<?php echo CHtml::encode($data->search_flag); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('view_display')); ?>:</b>
	<?php echo CHtml::encode($data->view_display); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_profile_fieldcol')); ?>:</b>
	<?php echo CHtml::encode($data->customer_profile_fieldcol); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('required')); ?>:</b>
	<?php echo CHtml::encode($data->required); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('min_length')); ?>:</b>
	<?php echo CHtml::encode($data->min_length); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('max_length')); ?>:</b>
	<?php echo CHtml::encode($data->max_length); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('regrex')); ?>:</b>
	<?php echo CHtml::encode($data->regrex); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('default_value')); ?>:</b>
	<?php echo CHtml::encode($data->default_value); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('other_validation')); ?>:</b>
	<?php echo CHtml::encode($data->other_validation); ?>
	<br />

	*/ ?>

</div>