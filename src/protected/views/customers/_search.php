<?php
/* @var $this CustomersController */
/* @var $model CustomersModel */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<hr />
	<div class="row-fluid">
	
		<div class="span4">
			<?php echo $form->label($model,'name1'); ?>
			<?php echo $form->textField($model,'name1',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->label($model,'name2'); ?>
			<?php echo $form->textField($model,'name2',array('size'=>60,'maxlength'=>125)); ?>
		</div>
		<div class="span4">
			<?php echo $form->label($model,'kana1'); ?>
			<?php echo $form->textField($model,'kana1',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->label($model,'kana2'); ?>
			<?php echo $form->textField($model,'kana2',array('size'=>60,'maxlength'=>125)); ?>
		</div>

		<div class="span4">
			<?php echo $form->label($model,'mailaddress'); ?>
			<?php echo $form->textField($model,'mailaddress',array('size'=>60,'maxlength'=>255)); ?>
		</div>
	
	</div>
	
	<hr />
	
	<div class="row-fluid">
		<?php foreach( $model->profileFieldModel->findAll('search_flag=1') as $one ): ?>
			<div class="span4">
				<?php echo $form->label($model,$one->field_name); ?>
				<?php echo $form->textField($model,$one->field_name); ?>
			</div>
		<?php endforeach; ?>
	</div>
	
	<div class="form-actions buttons">
		<?php echo CHtml::submitButton('Search', array( 'class' => 'btn btn-primary' ) ); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->