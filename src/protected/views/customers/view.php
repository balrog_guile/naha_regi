<?php
/* @var $this CustomersController */
/* @var $model CustomersModel */

$this->breadcrumbs=array(
	'Customers Models'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List CustomersModel', 'url'=>array('index')),
	array('label'=>'Create CustomersModel', 'url'=>array('create')),
	array('label'=>'Update CustomersModel', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete CustomersModel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CustomersModel', 'url'=>array('admin')),
);
?>

<h1>View CustomersModel #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name1',
		'name2',
		'kana1',
		'kana2',
		'mailaddress',
		'mailaddress2',
		'tel',
		'celphone',
		'fax',
		'birthday',
		'gender',
		'zip',
		'pref',
		'address1',
		'address2',
		'address3',
		'customer_status',
		'create_date',
		'update_date',
	),
)); ?>
