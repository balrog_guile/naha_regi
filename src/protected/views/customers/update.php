<?php
/* @var $this CustomersController */
/* @var $model CustomersModel */

$this->breadcrumbs=array(
	'Customers Models'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
);
?>

<h1>顧客編集</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>