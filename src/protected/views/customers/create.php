<?php
/* @var $this CustomersController */
/* @var $model CustomersModel */

$this->breadcrumbs=array(
	'Customers Models'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CustomersModel', 'url'=>array('index')),
	array('label'=>'Manage CustomersModel', 'url'=>array('admin')),
);
?>

<h1>顧客登録</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>