<?php
/* @var $this CustomersController */
/* @var $model CustomersModel */

$this->breadcrumbs=array(
);

$this->menu=array(
);

Yii::app()->clientScript->registerScript('search', "

$('.search-form form').submit(function(){
	$('#customers-model-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");


////追加項目を表示するか
$display_add = array();
foreach( $model->profileFieldModel->findAll('view_display = 1') as $one )
{
	$display_add[] = $one->field_name;
}


?>

<h1>顧客管理</h1>

<div>
	<?php echo CHtml::button( '新規顧客追加', array('class' => 'btn btn-large btn-info set_new') ); ?>
</div>


<!-- 検索 -->
<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div>
<!-- /検索 -->



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customers-model-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>  array_merge(
		array(
			'name1',
			'name2',
			'kana1',
			'kana2',
			'mailaddress'
		),
		$display_add,
		array(
			array(
				'class'=>'CButtonColumn',
				'template'=> '{update}<br />{comming}',
				'updateButtonImageUrl' => '',
				'buttons'=>array(
					'update' => array(
						'label'=>'顧客情報編集',
						'options' => array('class' => 'btn btn-success', 'style' => 'width: 150px; margin-bottom: 0.5em;')
					),
					'comming' => array(
						'label'=>'来店履歴',
						'icon'=>'tasks',
						'url'=>'Yii::app()->createUrl("comming/history", array("id"=>$data->id))',
						'options' => array('class' => 'btn btn-success', 'style' => 'width: 150px;')
					),
				)
			)
		)
	),
)); ?>
