<?php
/* @var $this CustomersController */
/* @var $model CustomersModel */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customers-model-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><span class="required">*</span>の項目は必須です</p>

	<?php echo $form->errorSummary($model); ?>
	
	<div class="row-fluid">
		
		<div class="span">
			<?php echo $form->labelEx($model,'name1'); ?>
			<?php echo $form->textField($model,'name1',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'name1'); ?>
		</div>

		<div class="">
			<?php echo $form->labelEx($model,'name2'); ?>
			<?php echo $form->textField($model,'name2',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'name2'); ?>
		</div>

		<div class="">
			<?php echo $form->labelEx($model,'kana1'); ?>
			<?php echo $form->textField($model,'kana1',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'kana1'); ?>
		</div>

		<div class="">
			<?php echo $form->labelEx($model,'kana2'); ?>
			<?php echo $form->textField($model,'kana2',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'kana2'); ?>
		</div>

		<div class="">
			<?php echo $form->labelEx($model,'mailaddress'); ?>
			<?php echo $form->textField($model,'mailaddress',array('size'=>60,'maxlength'=>255)); ?>
			<?php echo $form->error($model,'mailaddress'); ?>
		</div>

		<div class="">
			<?php echo $form->labelEx($model,'customer_status'); ?>
			<?php echo $form->dropDownList( $model,'customer_status', VarHelper::getCustomerStatus() ); ?>
			<?php echo $form->error($model,'customer_status'); ?>
		</div>
		
	</div>
	
	
	
	<?php foreach( $model->profileFieldModel->findAll() as $one ): ?>
		
		
		<?php /* テキストフィールド */ if( $one->input_type == 0 ): ?>
			<div class="<?php echo $one->html_class; ?>" id="<?php echo $one->html_id; ?>">
				<div class="">
					<?php echo $form->labelEx($model,$one->field_name); ?>
					<?php echo $form->textField( $model, $one->field_name, array() ); ?>
					<?php echo $form->error($model,$one->field_name); ?>
				</div>
			</div>
		<?php endif; ?>
		
		
		
		
		
		
		
		
		
		
		
		
	<?php endforeach; ?>
	
	
	
	
	
	<div class="buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? '登録' : '編集'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->