<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
	<!-- The styles -->
	<link id="bs-css" href="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/css/bootstrap-spacelab.css" rel="stylesheet">
	<style type="text/css">
	  body {
		padding-bottom: 40px;
	  }
	  .sidebar-nav {
		padding: 9px 0;
	  }
	</style>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/css/bootstrap-responsive.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/css/charisma-app.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
	<link href='<?php echo Yii::app()->request->baseUrl; ?>/regi_files/css/fullcalendar.css' rel='stylesheet'>
	<link href='<?php echo Yii::app()->request->baseUrl; ?>/regi_files/css/fullcalendar.print.css' rel='stylesheet'  media='print'>
	<link href='<?php echo Yii::app()->request->baseUrl; ?>/regi_files/css/chosen.css' rel='stylesheet'>
	<link href='<?php echo Yii::app()->request->baseUrl; ?>/regi_files/css/uniform.default.css' rel='stylesheet'>
	<link href='<?php echo Yii::app()->request->baseUrl; ?>/regi_files/css/colorbox.css' rel='stylesheet'>
	<link href='<?php echo Yii::app()->request->baseUrl; ?>/regi_files/css/jquery.cleditor.css' rel='stylesheet'>
	<link href='<?php echo Yii::app()->request->baseUrl; ?>/regi_files/css/jquery.noty.css' rel='stylesheet'>
	<link href='<?php echo Yii::app()->request->baseUrl; ?>/regi_files/css/noty_theme_default.css' rel='stylesheet'>
	<link href='<?php echo Yii::app()->request->baseUrl; ?>/regi_files/css/elfinder.min.css' rel='stylesheet'>
	<link href='<?php echo Yii::app()->request->baseUrl; ?>/regi_files/css/elfinder.theme.css' rel='stylesheet'>
	<link href='<?php echo Yii::app()->request->baseUrl; ?>/regi_files/css/jquery.iphone.toggle.css' rel='stylesheet'>
	<link href='<?php echo Yii::app()->request->baseUrl; ?>/regi_files/css/opa-icons.css' rel='stylesheet'>
	<link href='<?php echo Yii::app()->request->baseUrl; ?>/regi_files/css/uploadify.css' rel='stylesheet'>
	<link href='<?php echo Yii::app()->request->baseUrl; ?>/regi_files/css/append_style.css' rel='stylesheet'>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/css/append_style_print.css" rel="stylesheet" media="print">
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/jqueryui/themes/base/jquery.ui.all.css" rel="stylesheet">
	
	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!-- The fav icon -->
	<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/img/favicon.ico">
		
</head>

<body>
	
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<!-- <a class="brand" href="index.html"> -->
				<!-- </a> -->
				
				<!-- user dropdown starts -->
				<?php if( Yii::app()->user->getId() > 0 ): ?>
					<div class="btn-group pull-right" >
						<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
							<i class="icon-user"></i><span class="hidden-phone"> <?php echo Yii::app()->user->first_name; ?></span>
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<?php echo CHtml::link( 'プロフィール制御', Yii::app()->createUrl('user/profile') ); ?>
							<li class="divider"></li>

							<li>
								<?php echo CHtml::link( 'ログアウト', Yii::app()->createUrl('user/logout') ); ?>
							</li>
							<li class="divider"></li>

						</ul>
					</div>
				<?php endif; ?>
				<!-- user dropdown ends -->
				
				<!--
				<div class="top-nav nav-collapse">
					<ul class="nav">
						<li><a href="#">Visit Site</a></li>
						<li>
							<form class="navbar-search pull-left">
								<input placeholder="Search" class="search-query span2" name="query" type="text">
							</form>
						</li>
					</ul>
				</div>
				-->
				<!--/.nav-collapse -->
			</div>
		</div>
	</div>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
			
			
			<!-- left menu starts -->
			<div class="span2 main-menu-span">
				<div class="well nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
						
						<li class="nav-header hidden-tablet">商品管理</li>
						<li>
							<a class="ajax-link" href="<?php echo Yii::app()->createUrl('product/categories'); ?>">
								<i class="icon-th-list"></i>
								<span class="hidden-tablet"> カテゴリ管理</span>
							</a>
						</li>
						<li>
							<a class="ajax-link" href="<?php echo Yii::app()->createUrl('product/product'); ?>">
								<i class="icon-glass"></i>
								<span class="hidden-tablet"> 商品管理</span>
							</a>
						</li>
						<li>
							<a class="ajax-link" href="<?php echo Yii::app()->createUrl('partner/admin'); ?>">
								<i class="icon-glass"></i>
								<span class="hidden-tablet"> 仕入先リスト</span>
							</a>
						</li>
						
						
						<li class="nav-header hidden-tablet"">お知らせ管理</li>
						<li>
							<a class="ajax-link" href="<?php echo Yii::app()->createUrl('information'); ?>">
								<i class="icon-bell"></i>
								<span class="hidden-tablet">お知らせ管理</span>
							</a>
						</li>
						<li>
							<a class="ajax-link" href="<?php echo Yii::app()->createUrl('information/create'); ?>">
								<i class="icon-bell"></i>
								<span class="hidden-tablet">お知らせ投稿</span>
							</a>
						</li>
						
						<li class="nav-header hidden-tablet">レジスター</li>
						<li>
							<a class="ajax-link" href="<?php echo Yii::app()->createUrl('register'); ?>">
								<i class="icon-shopping-cart icon-barcode"></i>
								<span class="hidden-tablet"> レジスターモード</span>
							</a>
						</li>
						<li>
							<a class="ajax-link" href="<?php echo Yii::app()->createUrl('register/admin'); ?>">
								<i class="icon-book"></i>
								<span class="hidden-tablet"> 売上管理</span>
							</a>
						</li>
						
						<!--
						<li class="nav-header hidden-tablet">来店管理</li>
						<li>
							<a class="ajax-link" href="<?php echo Yii::app()->createUrl('comming/admin'); ?>">
								<i class="icon-list-alt"></i>
								<span class="hidden-tablet"> 来店管理</span>
							</a>
						</li>
						-->
						
						<li class="nav-header hidden-tablet">顧客管理</li>
						<li>
							<a class="ajax-link" href="<?php echo Yii::app()->createUrl('customers/admin'); ?>">
								<i class="icon-heart"></i>
								<span class="hidden-tablet"> 顧客リスト</span>
							</a>
						</li>
						<?php if(Yii::app()->user->isAdmin() === TRUE ): ?>
							<li>
								<a class="ajax-link" href="<?php echo Yii::app()->createUrl('customerProfileField'); ?>">
									<i class="icon-list"></i>
									<span class="hidden-tablet"> 顧客項目管理</span>
								</a>
							</li>
						<?php endif; ?>
						
						
						<?php if(Yii::app()->user->isAdmin() === TRUE ): ?>
							<li class="nav-header hidden-tablet">ユーザー管理</li>
							<li>
								<a class="ajax-link" href="<?php echo Yii::app()->createUrl('user/admin'); ?>">
									<i class="icon-user"></i>
									<span class="hidden-tablet"> ユーザー管理モード</span>
								</a>
							</li>
							<li>
								<a class="ajax-link" href="<?php echo Yii::app()->createUrl('user/admin/create'); ?>">
									<i class="icon-user icon-plus"></i>
									<span class="hidden-tablet"> ユーザー追加モード</span>
								</a>
							</li>
							
							<li class="nav-header hidden-tablet">ショップ管理</li>
							<li>
								<a class="ajax-link" href="<?php echo Yii::app()->createUrl('shop'); ?>">
									<i class="icon-gift"></i>
									<span class="hidden-tablet"> ショップ管理</span>
								</a>
							</li>
							
						<?php endif; ?>
						
						
						<?php
						/*
						<li class="nav-header hidden-tablet">Main</li>
						<li><a class="ajax-link" href="index.html"><i class="icon-home"></i><span class="hidden-tablet"> Dashboard</span></a></li>
						<li><a class="ajax-link" href="ui.html"><i class="icon-eye-open"></i><span class="hidden-tablet"> UI Features</span></a></li>
						<li><a class="ajax-link" href="form.html"><i class="icon-edit"></i><span class="hidden-tablet"> Forms</span></a></li>
						<li><a class="ajax-link" href="chart.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> Charts</span></a></li>
						<li><a class="ajax-link" href="typography.html"><i class="icon-font"></i><span class="hidden-tablet"> Typography</span></a></li>
						<li><a class="ajax-link" href="gallery.html"><i class="icon-picture"></i><span class="hidden-tablet"> Gallery</span></a></li>
						<li class="nav-header hidden-tablet">Sample Section</li>
						<li><a class="ajax-link" href="table.html"><i class="icon-align-justify"></i><span class="hidden-tablet"> Tables</span></a></li>
						<li><a class="ajax-link" href="calendar.html"><i class="icon-calendar"></i><span class="hidden-tablet"> Calendar</span></a></li>
						<li><a class="ajax-link" href="grid.html"><i class="icon-th"></i><span class="hidden-tablet"> Grid</span></a></li>
						<li><a class="ajax-link" href="file-manager.html"><i class="icon-folder-open"></i><span class="hidden-tablet"> File Manager</span></a></li>
						<li><a href="tour.html"><i class="icon-globe"></i><span class="hidden-tablet"> Tour</span></a></li>
						<li><a class="ajax-link" href="icon.html"><i class="icon-star"></i><span class="hidden-tablet"> Icons</span></a></li>
						<li><a href="error.html"><i class="icon-ban-circle"></i><span class="hidden-tablet"> Error Page</span></a></li>
						<li><a href="login.html"><i class="icon-lock"></i><span class="hidden-tablet"> Login Page</span></a></li>
						*/
						?>
					</ul>
					<!--
					<label id="for-is-ajax" class="hidden-tablet" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>
					-->
				</div><!--/.well -->
			</div><!--/span-->
			<!-- left menu ends -->
			
			
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			
			
			
			<!-- メインコンテンツ -->
			<div id="content" class="span10">
			
				<!-- パンくず -->
				<!--
				<div>
					<ul class="breadcrumb">
						<li>
							<a href="#">Home</a> <span class="divider">/</span>
						</li>
						<li>
							<a href="#">Dashboard</a>
						</li>
					</ul>
				</div>
				-->
				<!-- /パンくず -->
				
				<?php echo $content; ?>
				
			</div>
			<!-- /メインコンテンツ -->
			
			
		</div><!--/fluid-row-->
				
		
		<hr>
		
		
		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>
		
		
		
		<!-- jQueryUIモーダル用 -->
		<div id="jquimodal">
			<div id="jquimodal_content">
			</div>
			<div>
				<?php echo CHtml::button( 'OK', array( 'id' => 'jquimodal_ok') ); ?>
			</div>
		</div>
		
		
		<!--
		<footer>
			<p class="pull-left">Seiko</p>
			<p class="pull-right">Powered by: <a href="http://usman.it/free-responsive-admin-template">Charisma</a></p>
		</footer>
		-->
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<!-- jQuery -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/jquery-1.7.2.min.js"></script>
	<!-- jQuery UI -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/jquery-ui-1.8.21.custom.min.js"></script>
	<!-- transition / effect library -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/bootstrap-transition.js"></script>
	<!-- alert enhancer library -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/bootstrap-alert.js"></script>
	<!-- modal / dialog library -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/bootstrap-modal.js"></script>
	<!-- custom dropdown library -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/bootstrap-dropdown.js"></script>
	<!-- scrolspy library -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/bootstrap-scrollspy.js"></script>
	<!-- library for creating tabs -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/bootstrap-tab.js"></script>
	<!-- library for advanced tooltip -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/bootstrap-tooltip.js"></script>
	<!-- popover effect library -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/bootstrap-popover.js"></script>
	<!-- button enhancer library -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/bootstrap-button.js"></script>
	<!-- accordion library (optional, not used in demo) -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/bootstrap-collapse.js"></script>
	<!-- carousel slideshow library (optional, not used in demo) -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/bootstrap-carousel.js"></script>
	<!-- autocomplete library -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/bootstrap-typeahead.js"></script>
	<!-- tour library -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/bootstrap-tour.js"></script>
	<!-- library for cookie management -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/jquery.cookie.js"></script>
	<!-- calander plugin -->
	<script src='<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/fullcalendar.min.js'></script>
	<!-- data table plugin -->
	<script src='<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/jquery.dataTables.min.js'></script>

	<!-- chart libraries start -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/excanvas.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/jquery.flot.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/jquery.flot.pie.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/jquery.flot.stack.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/jquery.flot.resize.min.js"></script>
	<!-- chart libraries end -->

	<!-- select or dropdown enhancer -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/jquery.chosen.min.js"></script>
	<!-- checkbox, radio, and file input styler -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/jquery.uniform.min.js"></script>
	<!-- plugin for gallery image view -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/jquery.colorbox.min.js"></script>
	<!-- rich text editor library -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/jquery.cleditor.min.js"></script>
	<!-- notification plugin -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/jquery.noty.js"></script>
	<!-- file manager library -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/jquery.elfinder.min.js"></script>
	<!-- star rating plugin -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/jquery.raty.min.js"></script>
	<!-- for iOS style toggle switch -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/jquery.iphone.toggle.js"></script>
	<!-- autogrowing textarea plugin -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/jquery.autogrow-textarea.js"></script>
	<!-- multiple file upload plugin -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/jquery.uploadify-3.1.min.js"></script>
	<!-- history.js for cross-browser state change on ajax -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/js/charisma.js"></script>
	
	
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/jqueryui/ui/jquery.ui.core.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/jqueryui/ui/jquery.ui.widget.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/jqueryui/ui/jquery.ui.mouse.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/jqueryui/ui/jquery.ui.draggable.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/jqueryui/ui/jquery.ui.position.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/jqueryui/ui/jquery.ui.resizable.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/jqueryui/ui/jquery.ui.button.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/jqueryui/ui/jquery.ui.dialog.js"></script>
	
	
	<script>
		var regifiles_path = '<?php echo Yii::app()->request->baseUrl; ?>/regi_files/';
		var upload_helper_url = '<?php echo Yii::app()->createUrl('fileupload'); ?>';
	</script>
	
</body>
</html>
