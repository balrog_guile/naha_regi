<?php
/* @var $this CategoriesController */
/* @var $model CategoryModel */
/* @var $form CActiveForm */
?>

<div class="form">

<br />
<br />

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'category-model-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class' => 'form-horizontal',
	),
)); ?>

	
	
	<div class="row-fluid">
		<div class="span12">
			<?php echo $form->labelEx($model,'categori_name'); ?>
			<?php echo $form->textField($model,'categori_name',array('size'=>60,'maxlength'=>255)); ?>
			<?php echo $form->error($model,'categori_name'); ?>
		</div>
	</div>
	
	<hr />
	
	<div class="row-fluid">
		<div class="span12">
			<?php echo $form->labelEx($model,'link_url'); ?>
			<p>カテゴリURLはページ表示時のカテゴリURLとなります。半角英数「-」「_」で入力してください<br />
			空のまま送信すると、URLは商品番号と同じになります。</p>
			<?php echo $form->textField($model,'link_url'); ?>
			<?php echo $form->error($model,'link_url'); ?>
		</div>
	</div>
	
	<hr />
	
	<div class="row-fluid">
		<div class="span12">
			<?php echo $form->labelEx($model,'parent_id'); ?>
			<?php echo $form->textField($model,'parent_id',array('size'=>10,'maxlength'=>10, 'readonly'=>'readonly' )); ?>
				<?php echo CHtml::button( '選択', array( 'class' => 'btn btn-large btn-success parent_btn') ); ?>
			<?php echo $form->error($model,'parent_id'); ?>
		</div>
	</div>
	
	<hr />
	
	<div class="row-fluid">
		<div class="span12">
			<?php if( $model->main_image != '' ): ?>
			<div>
				<img src="<?php echo $model->main_image; ?>" id="category_main_space" class="upload_space" />
			</div>
			<?php endif; ?>
			<?php echo $form->labelEx($model,'main_image'); ?>
			<?php echo $form->hiddenField($model,'main_image',array( 'id' => 'input_main_image','size'=>60,'maxlength'=>255, 'readonly' => 'readonly')); ?>
			<?php echo CHtml::fileField( 'file', '', array(
					'class' => 'input-file uniform_on file_upload_on',//file_upload_onでajaxアップロード
					'id' => 'main_image',//IDは合ってもなくてもOK
					'data-uploadmode' => 'category_main',//コントローラーで定義されたアップロードモード
					'data-uplodedfilespace' => 'category_main_space',//アップロード後表示される場所
					'data-uplodedfileinput' => 'input_main_image',//パスを保存する場所
				)); ?>
			<?php echo $form->error($model,'main_image'); ?>
		</div>
	</div>
	
	<hr />
	
	<div class="row-fluid">
		<div class="span12">
			<?php if( $model->thumb_image != '' ): ?>
			<div>
				<img src="<?php echo $model->thumb_image; ?>" id="category_thumb_space" class="upload_space" />
			</div>
			<?php endif; ?>
			<?php echo $form->labelEx($model,'thumb_image'); ?>
			<?php echo $form->hiddenField($model,'thumb_image',array( 'id' => 'input_thumb_image', 'size'=>60,'maxlength'=>255, 'readonly' => 'readonly')); ?>
			<?php echo CHtml::fileField( 'file', '', array(
					'class' => 'input-file uniform_on file_upload_on',//file_upload_onでajaxアップロード
					'id' => 'thumb_image',//IDは合ってもなくてもOK
					'data-uploadmode' => 'category_thumb',//コントローラーで定義されたアップロードモード
					'data-uplodedfilespace' => 'category_thumb_space',//アップロード後表示される場所
					'data-uplodedfileinput' => 'input_thumb_image',//パスを保存する場所
				)); ?>
			<?php echo $form->error($model,'thumb_image'); ?>
		</div>
	</div>
	
	<hr />
	
	<div class="row-fluid">
		<div class="span12">
			<?php echo $form->labelEx($model,'page_key'); ?>
			<?php echo $form->textField($model,'page_key',array()); ?>
			<?php echo $form->error($model,'page_key'); ?>
		</div>
	</div>
	
	<hr />
	
	<div class="row-fluid">
		<div class="span12">
			<?php echo $form->labelEx($model,'page_desc'); ?>
			<?php echo $form->textField($model,'page_desc',array()); ?>
			<?php echo $form->error($model,'page_desc'); ?>
		</div>
	</div>
	
	
	<hr />
	
	<div class="row-fluid">
		<div class="span12">
			<?php echo $form->labelEx($model,'description'); ?>
			<?php //echo $form->textArea($model,'description',array( 'class' => 'cleditor' )); ?>
			<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50, 'class' => 'cleditor' )); ?>
			<?php echo $form->error($model,'description'); ?>
		</div>
	</div>
	
	
	<div class="form-actions">
		<?php echo CHtml::submitButton($model->isNewRecord ? '作成' : '編集', array( 'class' => 'btn btn-large btn-success') ); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<!--
<?php echo YiiBase::getPathOfAlias('webroot'); ?>
<br />
<?php echo Yii::app()->getBaseUrl(true) . '/files/'; ?>
-->