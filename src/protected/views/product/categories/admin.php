<?php
/* @var $this CategoriesController */
/* @var $model CategoryModel */

$this->breadcrumbs=array(
	'Category Models'=>array('index'),
	'Manage',
);

$this->menu=array(
	//array('label'=>'List CategoryModel', 'url'=>array('index')),
	//array('label'=>'Create CategoryModel', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#category-model-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>カテゴリ管理</h1>

<hr />
<div>
	<?php echo CHtml::link( '新規作成', Yii::app()->createUrl( 'product/categories/create'), array('class' => 'btn btn-large btn-info') ); ?>
</div>
<hr />
	
<!--
<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>
-->

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<!-- <div class="search-form" style="display:none"> -->
<?php //$this->renderPartial('_search',array( 'model'=>$model, )); ?>
<!-- </div> -->



<?php $this->widget(
	'zii.widgets.grid.CGridView',
	array(
		'id'=>'category-model-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			array(
				'name' => 'categori_name',
				'value' => 'str_repeat("　─", $data->depth) . $data->categori_name',
			),
			'link_url',
			/*
			array(
					'name'=>'rank',
					'type'=>'raw',
					'htmlOptions'=>array('style'=>'text-align: center'),
					'value'=>
						'CHtml::link("▲", array( "product/categories/rank", "id" => $data->id, "mode" => "up" )) . " " . '.
						'CHtml::link("▼", array( "product/categories/rank", "id" => $data->id, "mode" => "down" ))'
			),
			*/
			array(
				'class'=>'CButtonColumn',
				'template'=>
					implode(
						' ',
						array(
							'{update}',
							'{delete}',
							'<br />'
						)
					)
			),
		),
	)
	);
?>