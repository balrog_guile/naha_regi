<?php
/* @var $this StockingController */
/* @var $model StockingModel */
/* @var $form CActiveForm */
?>

<div class="">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'stocking-model-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
	<?php echo $form->errorSummary($model); ?>
	
	<hr />
	
	<div class="row-fluid">
		<div class="span3">
			<?php echo $form->labelEx($model,'insert_date'); ?>
			<?php echo $form->textField( $model, 'insert_date', array('class' => 'input-medium') ); ?>
			<?php echo $form->error($model,'insert_date'); ?>
		</div>
		<div class="span3">
			<?php echo $form->labelEx($model,'partner_id'); ?>
			<?php echo $form->dropdownlist( $model, 'partner_id', PartnerModel::getAllListDropDown(), array('class' => 'input-medium')); ?>
			<?php echo $form->error($model,'partner_id'); ?>
		</div>
		<div class="span3">
			<?php echo $form->labelEx($model,'purchase_price'); ?>
			<?php echo $form->textField($model,'purchase_price',array('size'=>10,'maxlength'=>10, 'class' => 'input-medium')); ?>
			<?php echo $form->error($model,'purchase_price'); ?>
		</div>
		<div class="span3">
			<?php echo $form->labelEx($model,'purchase_qty'); ?>
			<?php echo $form->textField($model,'purchase_qty', array('class' => 'input-medium')); ?>
			<?php echo $form->error($model,'purchase_qty'); ?>
		</div>
	</div>
	<p class="alert alert-input">
		編集しても出庫記録には影響を与えません。出庫実績のある入庫記録を編集する場合はお気をつけてください。
	</p>
	
	<div class="buttons">
		<?php echo $form->hiddenField($model,'sku_id'); ?>
		<?php echo CHtml::submitButton(
				($model->isNewRecord ? '作成' : '編集'),
				array(
					'class' => 'btn btn-large btn-info'
				)
			); ?>
	</div>
	
	
<?php $this->endWidget(); ?>

</div><!-- form -->