<?php
/* @var $this SkuController */
/* @var $model ProductSkuModel */

$this->breadcrumbs=array(
	'Product Sku Models'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProductSkuModel', 'url'=>array('index')),
	array('label'=>'Manage ProductSkuModel', 'url'=>array('admin')),
);
?>

<h1>Create ProductSkuModel</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>