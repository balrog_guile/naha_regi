<?php
/* @var $this StockingLeaveController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Stocking Leave Models',
);

$this->menu=array(
	array('label'=>'Create StockingLeaveModel', 'url'=>array('create')),
	array('label'=>'Manage StockingLeaveModel', 'url'=>array('admin')),
);
?>

<h1>Stocking Leave Models</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
