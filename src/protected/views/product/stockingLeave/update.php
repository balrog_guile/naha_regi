<?php
/* @var $this StockingLeaveController */
/* @var $model StockingLeaveModel */

$this->breadcrumbs=array(
	'Stocking Leave Models'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List StockingLeaveModel', 'url'=>array('index')),
	array('label'=>'Create StockingLeaveModel', 'url'=>array('create')),
	array('label'=>'View StockingLeaveModel', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage StockingLeaveModel', 'url'=>array('admin')),
);
?>

<h1>Update StockingLeaveModel <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>