<?php
/* @var $this ProductController */
/* @var $model ProductsModel */

$this->breadcrumbs=array(
	'Products Models'=>array('index'),
	'Manage',
);

$this->menu=array();

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#products-model-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<h1>バーコード管理</h1>



<!-- 検索フォーム -->
<div class="dontyouprint">
	<div class="search-form">
	<?php $this->renderPartial('_search',array(
		'model'=>$model,
		'categories' => $categories,
	)); ?>
	</div>
</div>
<!-- 検索フォーム -->


<!--バーコード表示-->
<div class="barcode_print">
	<?php $ca = $model->search();foreach( $ca->getData() as $row ): ?>
	<div class="barcode_print_one clearfix">
		<h2><?php echo $row->item_id; ?> <?php echo $row->item_id; ?></h2>

		<?php $i=0; foreach( $row->sku as $sku): ?>
			<div class="barcode_print_sku <?php if($i == 3):?> barcode_print_sku_clear<?php $i=0;endif; ?>">
				<b><?php echo $sku->brunch_item_name; ?> <?php echo $sku->brunch_item_id; ?></b>
				 <?php echo number_format( $sku->sale_price ); ?>円
				<div>
					<img src="<?php echo $this->createUrl( 'barcode2', array( 'setcode' => $sku->code_str ) ); ?>" />
				</div>
			</div>
		<?php $i ++; ?>
		<?php endforeach; ?>

	</div>
	<?php endforeach; ?>
</div>
<!--バーコード表示-->



<!-- ページネーション -->
<div class="dontyouprint">
<?php $this->widget('CLinkPager', array(
	'pages' => $ca->getPagination(),
)) ?>
</div>
<!-- /ページネーション -->