<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>無題ドキュメント</title>
</head>
<body style="font-size: 12px;">
<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#333333">
	<?php foreach( ProductsModel::model()->findAll() as $row ): ?>
		<tr>
			<td bgcolor="#FFFFFF" style="text-align: center;">
				<?php echo CHtml::image(Yii::app()->baseUrl . $row->main_image, '', array('width' => 100)); ?>
			</td>
			<td bgcolor="#FFFFFF">
				品番:<?php echo $row->item_id; ?><br />
				品名:<?php echo $row->item_name; ?><br />
			</td>
			<td bgcolor="#FFFFFF">
				<?php foreach(
						ProductSkuModel::model()->
							findAll(array(
								'condition' => 'product_id = :id',
								'params' => array( ':id' => $row->id ),
								'order' => 'rank'
							))
						as
						$sku
				): ?>
					<hr />
					枝番号:<?php echo $sku->brunch_item_id; ?>　枝名:<?php echo $sku->brunch_item_name; ?><br />
					定価:<?php echo number_format($sku->price); ?>円　
					売価:<?php echo number_format($sku->sale_price); ?>円<br />
					在庫:<?php echo number_format($sku->stock); ?>
				<?php endforeach; ?>
					<hr />
			</td>
		</tr>
	<?php endforeach; ?>
</table>
</body>
</html>
