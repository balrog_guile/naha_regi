<?php
/* @var $this ProductController */
/* @var $model ProductsModel */

$this->breadcrumbs=array(
	'Products Models'=>array('index'),
	'Manage',
);

$this->menu=array();

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#products-model-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<h1>商品管理</h1>


<!-- 検索フォーム -->
<div class="search-form">
<?php $this->renderPartial(
	'_search',
	array(
		'model'=>$model,
		'categories' => $categories_list,
	)
); ?>
</div>
<!-- 検索フォーム -->


<!-- 操作パネル -->
<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th"></i> 操作パネル</h2>
			<div class="box-icon">
			</div>
		</div>
		<div class="box-content">
			<div class="row-fluid">
				
				
				<?php echo CHtml::link(
						'新規商品追加',
						Yii::app()->createUrl('product/product/create'),
						array( 'class' => 'btn btn-large btn-primary')
					);
				?>
				
				
				<?php echo CHtml::link(
						'バーコード印刷',
						Yii::app()->createUrl('product/product/barcode'),
						array( 'class' => 'btn btn-large btn-primary')
					);
				?>
				
				
				<?php echo CHtml::link(
						'全一覧',
						Yii::app()->createUrl('product/product/allview'),
						array( 'class' => 'btn btn-large btn-primary')
					);
				?>
				
			</div>
		</div>
	</div>
</div>
<!-- /操作パネル -->



<!-- エラーメッセージ -->
<?php if( $product_error_message != '' ): ?>
<div class="alert alert-error">
	<?php echo $product_error_message; ?>
</div>
<?php endif; ?>
<!-- /エラーメッセージ -->



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'products-model-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'header' => '画像',
			'type' => 'raw',
			'value' => function($data){
				return CHtml::image(Yii::app()->baseUrl . $data->main_image, '', array('width' => 100));
			},
			'htmlOptions' => array(
				'width' => '10%'
			),
		),
		'item_id',
		'item_name',
		array(
			'name' => 'open_status',
			'value' => function($data){
				return (($data->open_status == 1) ? '公開' : '非公開');
			}
			,
			'htmlOptions' => array(
				'width' => '5%'
			),
		),
		array(
			'header' => '在庫単位管理',
			'type' => 'raw',
			'htmlOptions' => array(
				'width' => '40%'
			),
			'value' => function($data){
				$html = '';
				foreach($data->sku as $sku){
					$delete_flag = (($sku->delete_flag) ? '非公開' : '公開');
					$html .= <<<EOT
<table style="margin-bottom:5px; width: 100%;">
	<tr>
		<th width="20%">状態</th>
		<td>$delete_flag</td>
	</tr>
	<tr>
		<th>品番/品名</th>
		<td>$sku->brunch_item_id / $sku->brunch_item_name</td>
	</tr>
	<tr>
		<th>定価/売価</th>
		<td>$sku->price / $sku->sale_price</td>
	</tr>
	<tr>
		<th>在庫</th>
		<td>$sku->stock</td>
	</tr>
</table>
EOT;
				}
				return $html;
			}
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update} {delete}',
		),
	),
));
