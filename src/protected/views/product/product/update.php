<?php
/* @var $this ProductController */
/* @var $model ProductsModel */

$this->breadcrumbs=array(
	'Products Models'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
);
?>

<h1>商品編集</h1>

<?php $this->renderPartial(
		'_form',
		array(
			'model'=>$model,
			'categories'=>$categories,
			'skumodel' => $skumodel,
			'create_mode' => $create_mode,
			'skucheck' => $skucheck,
			'create_ok' => $create_ok,
			'rel_json' => $rel_json,
			'faq_list' => $faq_list,
			'tags' => $tags,
		)
	);
?>