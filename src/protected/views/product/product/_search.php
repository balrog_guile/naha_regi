<?php
/* @var $this ProductController */
/* @var $model ProductsModel */
/* @var $form CActiveForm */
?>

<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th"></i> 検索</h2>
			<div class="box-icon">
			</div>
		</div>
		<div class="box-content">
			<div class="row-fluid">
				<?php $form=$this->beginWidget(
					'CActiveForm', array(
					'action'=>Yii::app()->createUrl($this->route),
					'method'=>'get',
				)); ?>
					<div class="row-fluid">
						<div class="span3">
							<?php echo $form->label($model,'item_id'); ?>
							<?php echo $form->textField($model,'item_id',array('maxlength'=>125, 'class' => 'input-block-level')); ?>
						</div>
						<div class="span3">
							<?php echo $form->label($model,'item_name'); ?>
							<?php echo $form->textField($model,'item_name',array('maxlength'=>225, 'class' => 'input-block-level')); ?>
						</div>
						<div class="span3">
							<?php echo $form->label($model,'open_status'); ?>
							<?php echo $form->dropDownList($model,'open_status', VarHelper::getStatus(), array( 'class' => 'input-small') ); ?>
						</div>
					</div>
					<hr />
					<div class="row-fluid">
						<div class="span3">
							<?php echo $form->labelEx($model,'categories'); ?>
							<?php echo $form->listBox($model,'categories_many',$categories, array('multiple'=>'multiple') ); ?>
						</div>
					</div>
					<hr />
					<div class="row-fluid">
						<div class="span6">
							<?php echo CHtml::submitButton('　　　検索　　　', array( 'class' => 'btn btn-info') ); ?>
							<a href="<?php echo Yii::app()->createUrl('product/product/admin'); ?>" class="btn btn btn-danger">
								全商品表示
							</a>
						</div>
					</div>
				</div>
					
				<?php $this->endWidget(); ?>
				
			</div>
		</div>
	</div>
</div>
<!-- search-form -->