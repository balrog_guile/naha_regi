<?php
/* @var $this ProductController */
/* @var $model ProductsModel */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php
$form=$this->beginWidget(
	'CActiveForm',
	array(
		'action'=>Yii::app()->createUrl($this->route),
		'method'=>'get',
		'htmlOptions' => array(
			'class' => 'regi_search_form',
		),
	)
); ?>
	
	
	<div class="row-fluid">
		<div class="span3">
			<?php echo $form->label($model,'item_id'); ?>
			<?php echo $form->textField($model,'item_id',array('size'=>60,'maxlength'=>125)); ?>
		</div>
		
		<div class="span3">
			<?php echo $form->label($model,'item_name'); ?>
			<?php echo $form->textField($model,'item_name',array('size'=>60,'maxlength'=>225)); ?>
		</div>
		
		
		
	</div>
	
	<hr />
	
	<div class="row-fluid">
		<div class="span3">
			<?php echo $form->labelEx($model,'categories'); ?>
			<?php echo $form->listBox($model,'categories_many',$categories, array('multiple'=>'multiple') ); ?>
		</div>
		<div class="span3"><br />
			<?php echo CHtml::submitButton('　検索　', array( 'class' => 'btn btn-info') ); ?>
		</div>
	</div>
	

<?php $this->endWidget(); ?>

</div><!-- search-form -->
