<?php
/* @var $this ProductController */
/* @var $model ProductsModel */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'products-model-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
	<?php if( ( $model->hasErrors() === FALSE )&&( $skucheck === TRUE ) ): ?>
	<hr />
	<a href="<?php echo Yii::app()->createUrl('product/product/create'); ?>" class="btn btn-info btn-large">
			　　新規作成へ　　
		</a>
	<?php endif; ?>
	<hr />
	<p>
		*は必須項目です
	</p>
	<hr />
	
	<?php if( ( $model->hasErrors() )||( $skucheck === false ) ): ?>
	<div class="alert alert-error">
		エラーがあります。各項目を見なおしてください。
	</div>
	<?php endif; ?>
	
	
	<?php if( $create_ok === true ): ?>
	<div class="alert alert-success alert-input">
		<button type="button" class="close" data-dismiss="alert-input">&times;</button>
		<h4>商品登録／編集に成功しました。</h4>
		在庫登録が必要な場合はSKUから入荷管理をおこなってください。
	</div>
	<?php endif; ?>
	
	
	
	<div class="row-fluid">
		<div class="span6">
			<div class="">
				<?php echo $form->labelEx($model,'item_id'); ?>
				<?php echo $form->textField($model,'item_id',array('size'=>60,'maxlength'=>125)); ?>
				<?php echo $form->error($model,'item_id'); ?>
			</div>
			<div class="">
				<?php echo $form->labelEx($model,'item_name'); ?>
				<?php echo $form->textField($model,'item_name',array('size'=>60,'maxlength'=>225)); ?>
				<?php echo $form->error($model,'item_name'); ?>
			</div>
			
		</div>
		<div class="span6">
			<div class="">
				<?php echo $form->labelEx($model,'categories'); ?>
				<?php echo $form->listBox($model,'categories_many',$categories, array('multiple'=>'multiple') ); ?>
				<?php echo $form->error($model,'categories_many'); ?>
			</div>
		</div>
	</div>
	
	<hr />
	<div class="row-fluid">
		<div class="span12">
			<div class="">
				<?php echo $form->labelEx($model,'link_url'); ?>
				<p>カテゴリURLはページ表示時のカテゴリURLとなります。半角英数「-」「_」で入力してください</p>
				<?php echo $form->textField($model,'link_url',array('size'=>60,'maxlength'=>225)); ?>
				<?php echo $form->error($model,'link_url'); ?>
			</div>
		</div>
	</div>
	
	<hr />
	
	<div class="row-fluid">
		<div class="span6">
			<?php echo $form->labelEx($model,'page_key'); ?>
			<?php echo $form->textField($model,'page_key',array()); ?>
			<?php echo $form->error($model,'page_key'); ?>
		</div>
		<div class="span6">
			<?php echo $form->labelEx($model,'page_desc'); ?>
			<?php echo $form->textField($model,'page_desc',array()); ?>
			<?php echo $form->error($model,'page_desc'); ?>
		</div>
	</div>
	
	
	<hr />
	
	<!-- タグ機能 -->
	<label>タグ</label>
	<div class="tags_area" style="margin-bottom: 0.8em; width: 100%; word-break: break-all;">
		<span class="label tags_element"
			  id="tag_element_source"
			  style="margin-right: 0.3em; display:inline-block"
			  data-check-label=""
		>
			<span class="tags_element_label">Default</span>
			<a href="javascript:void(0);">[×]</a>
			<?php echo CHtml::hiddenField( 'tags[]', '', array( 'class' => 'tags_element_hidden' ,'id' => '') ); ?>
		</span>
		<?php foreach( $tags as $tag ): ?>
			<span class="label tags_element"
				  id="tag_element_source"
				  style="margin-right: 0.3em; display:inline-block"
				  data-check-label="<?php echo $tag; ?>"
			>
				<span class="tags_element_label"><?php echo $tag; ?></span>
				<a href="javascript:void(0);">[×]</a>
				<?php echo CHtml::hiddenField( 'tags[]', $tag, array( 'class' => 'tags_element_hidden' ,'id' => '') ); ?>
			</span>
		<?php endforeach; ?>
	</div>
	<?php echo CHtml::textField( 'addtags', '', array() ); ?>
	<?php echo CHtml::button( 'タグ追加', array( 'class' => 'btn btn-mini addtagsbtn') ); ?>
	<!-- /タグ機能 -->
	
	
	<hr />
	
	<div class="row-fluid sortable ui-sortable">
		<div class="box-header well" data-original-title="">
			<h2>SKU（小品番）</h2>
		</div>
		
		<?php if( $skucheck === false ): ?>
			<div class="alert alert-error">
				SKUがありません。
			</div>
		<?php endif; ?>
		
		<div class="box-content">
			<table id="children_data_table" class="table table-striped">
				<tbody>
					
					<?php $i = 0;foreach( $skumodel as $sku ): ?>
						<?php if( $i == 0 ){$x = 0; }else{ $x = $i-1; } ?>
						<tr class="children_data_tr" <?php if($i == 0): ?>id="children_data_tr"<?php endif; ?>>
							<!--
							<td class="sku_delete_flag_row">
								<?php if( $sku->delete_flag == 1 ): ?>
									販売停止
								<?php else: ?>
									販売表示
								<?php endif; ?>
							</td>
							-->
							<td>
								
								<?php echo $form->hiddenField($sku,'[' . $x. ']id', array() ); ?>
								<?php echo $form->hiddenField($sku,'[' . $x. ']product_id', array() ); ?>
								
								<div class="row-fluid">
									<div class="span4 sku_name_set_area">
										<table style="width: 100%;">
											<tr>
												<th>
													<?php echo $form->labelEx($sku,'[' . $x .']brunch_item_id'); ?>
												</th>
												<td>
													<?php echo $form->textField($sku,'[' . $x .']brunch_item_id',array('size'=>10,'maxlength'=>10)); ?>
													<?php echo $form->error($sku,'[' . $x .']brunch_item_id'); ?>
												</td>
											</tr>
											<tr>
												<th>
													<?php echo $form->labelEx($sku,'[' . $x .']brunch_item_name'); ?>
												</th>
												<td>
													<?php echo $form->textField($sku,'[' . $x .']brunch_item_name',array('size'=>10,'maxlength'=>10)); ?>
													<?php echo $form->error($sku,'[' . $x .']brunch_item_name'); ?>
												</td>
											</tr>
											<tr>
												<th>
													<?php echo $form->labelEx($sku,'[' . $x .']code_str'); ?>
												</th>
												<td>
													<?php echo $form->textField($sku,'[' . $x .']code_str',array('size'=>10, 'class' => 'input-small', 'readonly' => 'raedonly' )); ?>
													<?php echo Chtml::button( '読込', array( 'class' => 'btn btn-link sku_code_read', 'data-index' => $x ) ); ?>
												</td>
											</tr>
											<tr>
												<th>
													<?php echo $form->labelEx($sku,'[' . $x .']price'); ?>
												</th>
												<td>
													<?php echo $form->textField($sku,'[' . $x .']price',array('size'=>10,'maxlength'=>10, 'class' => 'input-small' )); ?>
													<?php echo $form->error($sku,'[' . $x .']price'); ?>
												</td>
											</tr>
											<tr>
												<th>
													<?php echo $form->labelEx($sku,'[' . $x .']sale_price'); ?>
												</th>
												<td>
													<?php echo $form->textField($sku,'[' . $x .']sale_price',array('size'=>10,'maxlength'=>10, 'class' => 'input-small' )); ?>
													<?php echo $form->error($sku,'[' . $x .']sale_price'); ?>
												</td>
											</tr>
											<tr>
												<th>
													<?php echo $form->labelEx($sku,'[' . $x .']stock'); ?>
												</th>
												<td>
													<?php echo $form->textField($sku,'[' . $x .']stock',array( 'readonly'=> 'readonly', 'size'=>10,'maxlength'=>10, 'class' => 'input-small' )); ?>
													<?php echo $form->error($sku,'[' . $x .']stock'); ?>
													<?php if( $create_mode===TRUE): ?>
														<br />在庫追加は商品登録後行うことができます。
													<?php endif; ?>
													<?php if( $create_mode===FALSE): ?>
														<?php echo Chtml::button( '入荷管理', array( 'class' => 'btn btn-link sku_insert_stocking', 'data-index' => $x, 'data-skuid' => $sku->id ) ); ?>
													<?php endif; ?>
												</td>
											</tr>
										</table>
									</div>
									<div class="span4">
										<table style="width: 100%;">
											<tr>
												<th>
													<?php echo $form->labelEx($sku,'[' . $x .']content'); ?>
												</th>
												<td>
													<?php echo $form->textArea($sku,'[' . $x .']content',array( 'style' => 'width: 100%', 'rows' => 5 )); ?>
													<?php echo $form->error($sku,'[' . $x .']content'); ?>
												</td>
											</tr>
										</table>
										<hr />
										<div class="row-fluid" style="">
											<div class="span6">
												<?php echo $form->labelEx($sku,'[' . $x .']delete_flag'); ?>
												<?php echo $form->checkBox($sku, '[' . $x .']delete_flag', array('value' => 1, 'data-no-uniform' => 'true', 'class' => 'sku_delete_flag')); ?>
												非表示にする
												<br />
												<?php echo Chtml::button( '完全削除', array( 'class' => 'btn btn-link sku_remove_all', 'data-target_id' => $sku->id) ); ?>
											</div>
											<div class="span6" style=" text-align: center;">
												<?php echo Chtml::button( '▲', array( 'class' => 'btn btn-link sku_rank_up' ) ); ?>
												<?php echo Chtml::button( '▼', array( 'class' => 'btn btn-link sku_rank_down' ) ); ?>
												<?php echo $form->hiddenField($sku,'[' . $x .']rank',array( 'class' => 'sku_rank' )); ?>
											</div>
										</div>
									</div>
									
									<!-- SKU画像 -->
									<div class="span4 sku_image_set_area" style="overflow: scroll;">
										<h4>画像</h4>
										<div class="sku_image_drop_area" style="margin: 0.8em 0px; border: 2px #FFA0A2 dashed; height: 40px; padding: 0.3em; text-align: center; background-color: #FFF;">
											画像をドラッグ＆ドロップ
										</div>
										<input type="file" class="sku_image_select_area" />
										<table style="width: 100%;" class="sku_image_add_area">
											<thead>
												<tr>
													<th>画像</th>
													<th>操作</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>
														<input type="hidden" name="sku_image_path[0][]" value="" />
														<img src="" width="120" />
													</td>
													<td style="text-align: center;">
														<?php echo Chtml::button( '▲', array( 'class' => 'btn btn-link sku_image_rank_change' , 'data-sku-image-rank-type' =>'up', 'data-skuimage-index' => $x ) ); ?>
														<?php echo Chtml::button( '▼', array( 'class' => 'btn btn-link sku_image_rank_change' , 'data-sku-image-rank-type' =>'down', 'data-skuimage-index' => $x ) ); ?>
														<?php echo Chtml::button( '削除', array( 'class' => 'btn btn-danger sku_image_rank_delete' , 'data-skuimage-index' => $x ) ); ?>
													</td>
												</tr>
												<?php foreach( $sku->product_sku_images as $sku_image ): ?>
													<tr>
														<td>
															<input type="hidden" name="sku_image_path[<?php echo $x; ?>][]" value="<?php echo $sku_image->image; ?>" />
															<img src="<?php echo $sku_image->image; ?>" width="120" />
														</td>
														<td style="text-align: center;">
															<?php echo Chtml::button( '▲', array( 'class' => 'btn btn-link sku_image_rank_change', 'data-sku-image-rank-type' =>'up', 'data-skuimage-index' => $x ) ); ?>
															<?php echo Chtml::button( '▼', array( 'class' => 'btn btn-link sku_image_rank_change', 'data-sku-image-rank-type' =>'down', 'data-skuimage-index' => $x ) ); ?>
															<?php echo Chtml::button( '削除', array( 'class' => 'btn btn-danger sku_image_rank_delete' , 'data-skuimage-index' => $x ) ); ?>
														</td>
													</tr>
												<?php endforeach; ?>
											</tbody>
										</table>
									</div>
									<!-- /SKU画像 -->
									
								</div>
							</td>
						</tr>
					<?php $i ++ ;endforeach; ?>
				</tbody>
			</table>
		</div>
		
		<hr />
		
		<div class="row-fluid">
			<div class="span12 right">
				<?php echo Chtml::button( '追加', array( 'class' => 'btn btn-link sku_add' ) ); ?>
			</div>
		</div>

		<div id="modal_code_read" class="hide">
			<label id="modal_code_read_label">
				バーコードを読み込んでください
				<?php echo Chtml::textField('tmp_code', '', array( 'id' => 'modal_code_read_input' ) ); ?>
			</label>
		</div>
		
	</div>
	
	
	<hr />
	
	
	<!-- SKU表示用 -->
	<div id="children_data_area" class="">
	</div>
	<!-- /SKU表示用 -->
	
	
	<div class="row-fluid">
		<div class="span4">
			<div class="">
				<?php echo $form->labelEx($model,'open_status'); ?>
				<?php echo $form->dropDownList($model,'open_status',array( '1'=> '公開', '2' => '非公開' )); ?>
				<?php echo $form->error($model,'open_status'); ?>
			</div>
			
			<div class="">
				<?php echo $form->labelEx($model,'item_status'); ?>
				<?php echo $form->listBox($model,'item_status',VarHelper::getItemStatus(), array('multiple'=>'multiple', 'size' => '6') ); ?>
				<?php echo $form->error($model,'item_status'); ?>
			</div>
		</div>
		<div class="span4">
			<div class="">
				<?php echo $form->labelEx($model,'postage'); ?>
				<?php echo $form->textField($model,'postage',array('size'=>10,'maxlength'=>10)); ?>
				<?php echo $form->error($model,'postage'); ?>
			</div>
			
			<div class="">
				<?php echo $form->labelEx($model,'point_rate'); ?>
				<?php echo $form->textField($model,'point_rate',array('size'=>10,'maxlength'=>10)); ?>
				<?php echo $form->error($model,'point_rate'); ?>
			</div>
			
			<div class="">
				<?php echo $form->labelEx($model,'shipment'); ?>
				<?php echo $form->textField($model,'shipment',array('size'=>60,'maxlength'=>125)); ?>
				<?php echo $form->error($model,'shipment'); ?>
			</div>
			
			<div class="">
				<?php echo $form->labelEx($model,'qty_limit'); ?>
				<?php echo $form->textField($model,'qty_limit',array('size'=>10,'maxlength'=>10)); ?>
				<?php echo $form->error($model,'qty_limit'); ?>
			</div>
		</div>
		<div class="span4">
			<div class="">
				<?php echo $form->labelEx($model,'serch_word'); ?>
				<?php echo $form->textArea($model,'serch_word',array('rows'=>6, 'cols'=>50)); ?>
				<?php echo $form->error($model,'serch_word'); ?>
			</div>
			
			<div class="">
				<?php echo $form->labelEx($model,'remark'); ?>
				<?php echo $form->textArea($model,'remark',array('rows'=>6, 'cols'=>50)); ?>
				<?php echo $form->error($model,'remark'); ?>
			</div>
		</div>
	</div>
	
	
	<hr />
	
	<div class="row-fluid">
		<div class="span4">
			<div class="">
				<?php echo $form->labelEx($model,'thumbnail'); ?>
				<div class="upload_result_area" data-target="thumbnail">
					<?php if(!empty($model->thumbnail)): ?>
						<?php echo Chtml::image(Yii::app()->baseUrl . $model->thumbnail, null, array('width' => 100, 'id' => 'thumbnail_img')); ?>
					<?php endif; ?>
				</div>
				
				<div
					class="sku_product_drop_area"
					style="margin: 0.8em 0px; border: 2px #FFA0A2 dashed; height: 40px; padding: 0.3em; text-align: center; background-color: #FFF;"
					data-target="thumbnail"
				>
					画像をドラッグ＆ドロップ
				</div>
				
				<?php echo $form->hiddenField($model,'thumbnail', array('id' => 'thumbnail_hidden')); ?>
				<?php echo Chtml::fileField('FileUploadModel', null, array('class' => 'upload_image', 'data-target' => 'thumbnail', 'data-no-uniform' => 'true', 'data-mode' => 'product_images')); ?>
				<?php echo $form->error($model,'thumbnail'); ?>
			</div>
			<hr />
			<div class="">
				<?php echo $form->labelEx($model,'main_image'); ?>
				<div class="upload_result_area" data-target="main_image">
					<?php if(!empty($model->main_image)): ?>
						<?php echo Chtml::image(Yii::app()->baseUrl . $model->main_image, null, array('width' => 100, 'id' => 'main_image_img')); ?>
					<?php endif; ?>
				</div>
				<div
					class="sku_product_drop_area"
					style="margin: 0.8em 0px; border: 2px #FFA0A2 dashed; height: 40px; padding: 0.3em; text-align: center; background-color: #FFF;"
					data-target="main_image"
				>
					画像をドラッグ＆ドロップ
				</div>
				<?php echo $form->hiddenField($model,'main_image', array('id' => 'main_image_hidden')); ?>
				<?php echo Chtml::fileField('FileUploadModel', null, array('class' => 'upload_image', 'data-target' => 'main_image', 'data-no-uniform' => 'true', 'data-mode' => 'product_images')); ?>
				<?php echo $form->error($model,'main_image'); ?>
			</div>
			<hr />
			<div class="">
				<?php echo $form->labelEx($model,'main_big_image'); ?>
				<div class="upload_result_area" data-target="main_big_image">
					<?php if(!empty($model->main_big_image)): ?>
						<?php echo Chtml::image(Yii::app()->baseUrl . $model->main_big_image, null, array('width' => 100, 'id' => 'main_big_image_img')); ?>
					<?php endif; ?>
				</div>
				<div
					class="sku_product_drop_area"
					style="margin: 0.8em 0px; border: 2px #FFA0A2 dashed; height: 40px; padding: 0.3em; text-align: center; background-color: #FFF;"
					data-target="main_big_image"
				>
					画像をドラッグ＆ドロップ
				</div>
				<?php echo $form->hiddenField($model,'main_big_image', array('id' => 'main_big_image_hidden')); ?>
				<?php echo Chtml::fileField('FileUploadModel', null, array('class' => 'upload_image', 'data-target' => 'main_big_image', 'data-no-uniform' => 'true', 'data-mode' => 'product_images')); ?>
				<?php echo $form->error($model,'main_big_image'); ?>
			</div>
		</div>
		<div class="span8">
			<div class="">
				<?php echo $form->labelEx($model,'main_view_comment'); ?>
				<?php echo $form->textArea($model,'main_view_comment',array('rows'=>6, 'cols'=>50, 'class' => 'cleditor' )); ?>
				<?php echo $form->error($model,'main_view_comment'); ?>
			</div>
			<div class="">
				<?php echo $form->labelEx($model,'main_detail_comment'); ?>
				<?php echo $form->textArea($model,'main_detail_comment',array('rows'=>6, 'cols'=>50, 'class' => 'cleditor' )); ?>
				<?php echo $form->error($model,'main_detail_comment'); ?>
			</div>
		</div>
	</div>
	
	
	<hr />
	
	
	<div class="row-fluid">
		<div class="span4">
			<div class="">
				<?php echo $form->labelEx($model,'sub1_img'); ?>
				<div class="upload_result_area" data-target="sub1_img">
					<?php if(!empty($model->sub1_img)): ?>
						<?php echo Chtml::image(Yii::app()->baseUrl . $model->sub1_img, null, array('width' => 100, 'id' => 'sub1_img_img')); ?>
					<?php endif; ?>
				</div>
				<div
					class="sku_product_drop_area"
					style="margin: 0.8em 0px; border: 2px #FFA0A2 dashed; height: 40px; padding: 0.3em; text-align: center; background-color: #FFF;"
					data-target="sub1_img"
				>
					画像をドラッグ＆ドロップ
				</div>
				<?php echo $form->hiddenField($model,'sub1_img', array('id' => 'sub1_img_hidden')); ?>
				<?php echo Chtml::fileField('FileUploadModel', null, array('class' => 'upload_image', 'data-target' => 'sub1_img', 'data-no-uniform' => 'true', 'data-mode' => 'product_images')); ?>
				<?php echo $form->error($model,'sub1_img'); ?>
			</div>
		</div>
		<div class="span8">
			<div class="">
				<?php echo $form->labelEx($model,'sub1_title'); ?>
				<?php echo $form->textfield($model,'sub1_title',array('rows'=>6, 'cols'=>50)); ?>
				<?php echo $form->error($model,'sub1_title'); ?>
			</div>
			<div class="">
				<?php echo $form->labelEx($model,'sub1_text'); ?>
				<?php echo $form->textArea($model,'sub1_text',array('rows'=>6, 'cols'=>50, 'class' => 'cleditor' )); ?>
				<?php echo $form->error($model,'sub1_text'); ?>
			</div>
		</div>
	</div>
	
	<hr />
	
	
	<div class="row-fluid">
		<div class="span4">
			<div class="">
				<?php echo $form->labelEx($model,'sub2_img'); ?>
				<div class="upload_result_area" data-target="sub2_img">
					<?php if(!empty($model->sub2_img)): ?>
						<?php echo Chtml::image(Yii::app()->baseUrl . $model->sub2_img, null, array('width' => 100, 'id' => 'sub2_img_img')); ?>
					<?php endif; ?>
				</div>
				<div
					class="sku_product_drop_area"
					style="margin: 0.8em 0px; border: 2px #FFA0A2 dashed; height: 40px; padding: 0.3em; text-align: center; background-color: #FFF;"
					data-target="sub2_img"
				>
					画像をドラッグ＆ドロップ
				</div>
				<?php echo $form->hiddenField($model,'sub2_img', array('id' => 'sub2_img_hidden')); ?>
				<?php echo Chtml::fileField('FileUploadModel', null, array('class' => 'upload_image', 'data-target' => 'sub2_img', 'data-no-uniform' => 'true', 'data-mode' => 'product_images')); ?>
				<?php echo $form->error($model,'sub2_img'); ?>
			</div>
		</div>
		<div class="span8">
			<div class="">
				<?php echo $form->labelEx($model,'sub2_title'); ?>
				<?php echo $form->textfield($model,'sub2_title',array('rows'=>6, 'cols'=>50)); ?>
				<?php echo $form->error($model,'sub2_title'); ?>
			</div>
			<div class="">
				<?php echo $form->labelEx($model,'sub2_text'); ?>
				<?php echo $form->textArea($model,'sub2_text',array('rows'=>6, 'cols'=>50, 'class' => 'cleditor' )); ?>
				<?php echo $form->error($model,'sub2_text'); ?>
			</div>
		</div>
	</div>
	
	<hr />
	
	
	
	<div class="row-fluid">
		<div class="span4">
			<div class="">
				<?php echo $form->labelEx($model,'sub3_img'); ?>
				<div class="upload_result_area" data-target="sub3_img">
					<?php if(!empty($model->sub3_img)): ?>
						<?php echo Chtml::image(Yii::app()->baseUrl . $model->sub3_img, null, array('width' => 100, 'id' => 'sub3_img_img')); ?>
					<?php endif; ?>
				</div>
				<div
					class="sku_product_drop_area"
					style="margin: 0.8em 0px; border: 2px #FFA0A2 dashed; height: 40px; padding: 0.3em; text-align: center; background-color: #FFF;"
					data-target="sub3_img"
				>
					画像をドラッグ＆ドロップ
				</div>
				<?php echo $form->hiddenField($model,'sub3_img', array('id' => 'sub3_img_hidden')); ?>
				<?php echo Chtml::fileField('FileUploadModel', null, array('class' => 'upload_image', 'data-target' => 'sub3_img', 'data-no-uniform' => 'true', 'data-mode' => 'product_images')); ?>
				<?php echo $form->error($model,'sub3_img'); ?>
			</div>
		</div>
		<div class="span8">
			<div class="">
				<?php echo $form->labelEx($model,'sub3_title'); ?>
				<?php echo $form->textfield($model,'sub3_title',array('rows'=>6, 'cols'=>50)); ?>
				<?php echo $form->error($model,'sub3_title'); ?>
			</div>
			<div class="">
				<?php echo $form->labelEx($model,'sub3_text'); ?>
				<?php echo $form->textArea($model,'sub3_text',array('rows'=>6, 'cols'=>50, 'class' => 'cleditor' )); ?>
				<?php echo $form->error($model,'sub3_text'); ?>
			</div>
		</div>
	</div>
	
	<hr />
	
	
	
	<div class="row-fluid">
		<div class="span4">
			<div class="">
				<?php echo $form->labelEx($model,'sub4_img'); ?>
				<div class="upload_result_area" data-target="sub4_img">
					<?php if(!empty($model->sub4_img)): ?>
						<?php echo Chtml::image(Yii::app()->baseUrl . $model->sub4_img, null, array('width' => 100, 'id' => 'sub4_img_img')); ?>
					<?php endif; ?>
				</div>
				<div
					class="sku_product_drop_area"
					style="margin: 0.8em 0px; border: 2px #FFA0A2 dashed; height: 40px; padding: 0.3em; text-align: center; background-color: #FFF;"
					data-target="sub4_img"
				>
					画像をドラッグ＆ドロップ
				</div>
				<?php echo $form->hiddenField($model,'sub4_img', array('id' => 'sub4_img_hidden')); ?>
				<?php echo Chtml::fileField('FileUploadModel', null, array('class' => 'upload_image', 'data-target' => 'sub4_img', 'data-no-uniform' => 'true', 'data-mode' => 'product_images')); ?>
				<?php echo $form->error($model,'sub4_img'); ?>
			</div>
		</div>
		<div class="span8">
			<div class="">
				<?php echo $form->labelEx($model,'sub4_title'); ?>
				<?php echo $form->textfield($model,'sub4_title',array('rows'=>6, 'cols'=>50)); ?>
				<?php echo $form->error($model,'sub4_title'); ?>
			</div>
			<div class="">
				<?php echo $form->labelEx($model,'sub4_text'); ?>
				<?php echo $form->textArea($model,'sub4_text',array('rows'=>6, 'cols'=>50, 'class' => 'cleditor' )); ?>
				<?php echo $form->error($model,'sub4_text'); ?>
			</div>
		</div>
	</div>
	
	<hr />
	
	
	
	<div class="row-fluid">
		<div class="span4">
			<div class="">
				<?php echo $form->labelEx($model,'sub5_img'); ?>
				<div class="upload_result_area" data-target="sub5_img">
					<?php if(!empty($model->sub5_img)): ?>
						<?php echo Chtml::image(Yii::app()->baseUrl . $model->sub5_img, null, array('width' => 100, 'id' => 'sub5_img_img')); ?>
					<?php endif; ?>
				</div>
				<div
					class="sku_product_drop_area"
					style="margin: 0.8em 0px; border: 2px #FFA0A2 dashed; height: 40px; padding: 0.3em; text-align: center; background-color: #FFF;"
					data-target="sub5_img"
				>
					画像をドラッグ＆ドロップ
				</div>
				<?php echo $form->hiddenField($model,'sub5_img', array('id' => 'sub5_img_hidden')); ?>
				<?php echo Chtml::fileField('FileUploadModel', null, array('class' => 'upload_image', 'data-target' => 'sub5_img', 'data-no-uniform' => 'true', 'data-mode' => 'product_images')); ?>
				<?php echo $form->error($model,'sub5_img'); ?>
			</div>
		</div>
		<div class="span8">
			<div class="">
				<?php echo $form->labelEx($model,'sub5_title'); ?>
				<?php echo $form->textfield($model,'sub5_title',array('rows'=>6, 'cols'=>50)); ?>
				<?php echo $form->error($model,'sub5_title'); ?>
			</div>
			<div class="">
				<?php echo $form->labelEx($model,'sub5_text'); ?>
				<?php echo $form->textArea($model,'sub5_text',array('rows'=>6, 'cols'=>50, 'class' => 'cleditor' )); ?>
				<?php echo $form->error($model,'sub5_text'); ?>
			</div>
		</div>
	</div>
	
	<hr />
	
	
	<div class="row-fluid">
		<div class="span4">
			<div class="">
				<?php echo $form->labelEx($model,'sub6_img'); ?>
				<div class="upload_result_area" data-target="sub6_img">
					<?php if(!empty($model->sub6_img)): ?>
						<?php echo Chtml::image(Yii::app()->baseUrl . $model->sub6_img, null, array('width' => 100, 'id' => 'sub6_img_img')); ?>
					<?php endif; ?>
				</div>
				<div
					class="sku_product_drop_area"
					style="margin: 0.8em 0px; border: 2px #FFA0A2 dashed; height: 40px; padding: 0.3em; text-align: center; background-color: #FFF;"
					data-target="sub6_img"
				>
					画像をドラッグ＆ドロップ
				</div>
				<?php echo $form->hiddenField($model,'sub6_img', array('id' => 'sub6_img_hidden')); ?>
				<?php echo Chtml::fileField('FileUploadModel', null, array('class' => 'upload_image', 'data-target' => 'sub6_img', 'data-no-uniform' => 'true', 'data-mode' => 'product_images')); ?>
				<?php echo $form->error($model,'sub6_img'); ?>
			</div>
		</div>
		<div class="span8">
			<div class="">
				<?php echo $form->labelEx($model,'sub6_title'); ?>
				<?php echo $form->textfield($model,'sub6_title',array('rows'=>6, 'cols'=>50)); ?>
				<?php echo $form->error($model,'sub6_title'); ?>
			</div>
			<div class="">
				<?php echo $form->labelEx($model,'sub6_text'); ?>
				<?php echo $form->textArea($model,'sub6_text',array('rows'=>6, 'cols'=>50, 'class' => 'cleditor' )); ?>
				<?php echo $form->error($model,'sub6_text'); ?>
			</div>
		</div>
	</div>
	
	
	<hr />
	
	
	<!-- /追加画像 -->
	<div class="row-fluid sortable ui-sortable">
		<div class="box-header well" data-original-title="">
			<h2>追加画像</h2>
		</div>
		<div class="box-content">
			<table class="table table-striped" id="add_images">
				<thead>
					<tr>
						<th>画像</th>
						<th>コメント</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<img src="" width="150">
							<?php echo CHtml::hiddenField( 'add_images[]', '',array() ); ?>
						</td>
						<td>
							<textarea name="add_images_content[]" style="width: 200px"></textarea>
						</td>
						<td>
							<?php echo Chtml::button( '▲', array( 'class' => 'btn btn-link add_images_rank_change', 'data-rank-type' =>'up' ) ); ?>
							<?php echo Chtml::button( '▼', array( 'class' => 'btn btn-link add_images_rank_change', 'data-rank-type' =>'down' ) ); ?>
							<?php echo Chtml::button( '削除', array( 'class' => 'btn btn-danger add_images_delete') ); ?>
						</td>
					</tr>
					<?php foreach( $model->product_images as $add_image): ?>
						<tr>
							<td>
								<img src="<?php echo $add_image->image; ?>" width="150">
								<?php echo CHtml::hiddenField( 'add_images[]', $add_image->image ,array() ); ?>
							</td>
							<td>
								<textarea name="add_images_content[]" style="width: 200px"><?php echo $add_image->content; ?></textarea>
							</td>
							<td>
								<?php echo Chtml::button( '▲', array( 'class' => 'btn btn-link add_images_rank_change', 'data-rank-type' =>'up' ) ); ?>
								<?php echo Chtml::button( '▼', array( 'class' => 'btn btn-link add_images_rank_change', 'data-rank-type' =>'down' ) ); ?>
								<?php echo Chtml::button( '削除', array( 'class' => 'btn btn-danger add_images_delete') ); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="3">
							<div
								class="add_image_drop_area"
								style="margin: 0.8em 0px; border: 2px #FFA0A2 dashed; height: 40px; padding: 0.3em; text-align: center; background-color: #FFF;"
							>
								画像をドラッグ＆ドロップ
							</div>
							<?php echo Chtml::fileField( 'add_images', '', array() ); ?>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	<!-- /追加画像 -->
	
	
	
	<hr />
	
	
	<!-- FAQ -->
	<div class="row-fluid">
		<div class="box-header well" data-original-title="">
			<h2>FAQ</h2>
		</div>
		<div class="box-content">
			<table class="table table-striped" id="faq_table">
				<thead>
					<tr>
						<th>質問</th>
						<th width="15%">操作</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<?php echo $form->hiddenField($faq_list['set'], '[0000]id' ); ?>
							<?php echo $form->label( $faq_list['set'], '[0000]question' ); ?>
							<?php echo $form->textArea( $faq_list['set'], '[0000]question', array( 'style' => 'width: 100%; height: 50px;' ) ); ?>
							<hr>
							<?php echo $form->label( $faq_list['set'], '[0000]answer' ); ?>
							<?php echo $form->textArea( $faq_list['set'], '[0000]answer', array('style' => 'width: 100%; height: 50px;') ); ?>
						</td>
						<td>
							<?php echo Chtml::button( '▲', array( 'class' => 'btn btn-link faq_rank_change', 'data-rank-type' =>'up' ) ); ?>
							<?php echo Chtml::button( '▼', array( 'class' => 'btn btn-link faq_rank_change', 'data-rank-type' =>'down' ) ); ?>
							<?php echo Chtml::button( '削除', array( 'class' => 'btn btn-danger faq_delete') ); ?>
						</td>
					</tr>
					<?php $count = 0; foreach( $faq_list['models'] as $faq_model ): ?>
						<tr>
							<td>
								<?php echo $form->hiddenField($faq_model, '[' . $count .']id' ); ?>
								<?php echo $form->label( $faq_model, '[' . $count .']question' ); ?>
								<?php echo $form->textArea( $faq_model, '[' . $count .']question', array( 'style' => 'width: 100%' ) ); ?>
								<?php echo $form->error( $faq_model, '[' . $count .']question'); ?>
								<hr>
								<?php echo $form->label( $faq_model, '[' . $count .']answer' ); ?>
								<?php echo $form->textArea( $faq_model, '[' . $count .']answer', array('style' => 'width: 100%') ); ?>
								<?php echo $form->error( $faq_model, '[' . $count .']answer'); ?>
							</td>
							<td>
								<?php echo Chtml::button( '▲', array( 'class' => 'btn btn-link faq_rank_change', 'data-rank-type' =>'up' ) ); ?>
								<?php echo Chtml::button( '▼', array( 'class' => 'btn btn-link faq_rank_change', 'data-rank-type' =>'down' ) ); ?>
								<?php echo Chtml::button( '削除', array( 'class' => 'btn btn-danger faq_delete') ); ?>
							</td>
						</tr>
					<?php $count ++ ; endforeach; ?>
				</tbody>
				<tfoot>
					<tr>
						<th colspan="2">
							<?php echo CHtml::button( '追加', array( 'class' => 'btn faq_add') ); ?>
						</th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	<!-- /FAQ -->
	
	<hr />
	
	<!-- 関連商品 -->
	<div class="row-fluid sortable ui-sortable">
		<div class="box-header well" data-original-title="">
			<h2>関連商品</h2>
		</div>
		
		<div class="rel_products">
			
		</div>
		
		<hr />
		<?php echo CHtml::hiddenField( 'rel_json', rawurlencode($rel_json), array( 'class' => 'rel_json' ) ); ?>
		<input type="button" data-id="<?php echo $model->id; ?>" value="関連商品検索" class="get-rel btn" />
		
	</div>
	<!-- /関連商品 -->
	
	
	<hr />
	
	
	<!-- 他商品のSKUをオプションとして呼ぶ機能 -->
	<div class="row-fluid">
		<div class="box-header well" data-original-title="">
			<h2>他商品のSKUをオプション品として呼ぶ機能</h2>
		</div>
		<div class="box-content">
			<table class="table table-striped" id="sku_option_add_table">
				<thead>
					<tr>
						<th>画像</th>
						<th>商品名</th>
						<th width="15%">操作</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<img src="" class="skuimage" width="150">
						</td>
						<td>
							<?php echo CHtml::hiddenField('optionsku[]', '', array() ); ?>
							<span class="product"></span>
							<span class="sku"></span>
						</td>
						<td>
							<?php echo Chtml::button( '▲', array( 'class' => 'btn btn-link skuoptionrank', 'data-rank-type' =>'up' ) ); ?>
							<?php echo Chtml::button( '▼', array( 'class' => 'btn btn-link skuoptionrank', 'data-rank-type' =>'down' ) ); ?>
							<?php echo Chtml::button( '削除', array( 'class' => 'btn btn-danger skuoptiondelete') ); ?>
						</td>
					</tr>
					<?php $op = ((!is_null($model->reinpit_optionsku)?$model->reinpit_optionsku:$model->optionsku));foreach( $op as $optsku): ?>
						<tr>
							<td>
								<?php if( (isset($optsku->sku->product_sku_images[0]))&&($optsku->sku->product_sku_images[0]->image != '' )): ?>
									<img
										src="<?php echo $optsku->sku->product_sku_images[0]->image; ?>"
										class="skuimage"
										width="150"
									>
								<?php else: ?>
									画像なし
								<?php endif; ?>
							</td>
							<td>
								<?php echo CHtml::hiddenField('optionsku[]', $optsku->sku->id, array() ); ?>
								<span class="product">
									<?php echo $optsku->sku->products->item_id ; ?>
									 
									<?php echo $optsku->sku->products->item_name ; ?>
								</span>
								<span class="sku">
									<?php echo $optsku->sku->brunch_item_id ; ?>
									 
									<?php echo $optsku->sku->brunch_item_name ; ?>
								</span>
							</td>
							<td>
								<?php echo Chtml::button( '▲', array( 'class' => 'btn btn-link skuoptionrank', 'data-rank-type' =>'up' ) ); ?>
								<?php echo Chtml::button( '▼', array( 'class' => 'btn btn-link skuoptionrank', 'data-rank-type' =>'down' ) ); ?>
								<?php echo Chtml::button( '削除', array( 'class' => 'btn btn-danger skuoptiondelete') ); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<hr />
			<?php echo CHtml::button( 'オプション選択', array('class' => 'btn btn select_adds') ); ?>
		</div>
	</div>
	<!-- /他商品のSKUをオプションとして呼ぶ機能 -->
	
	
	
	<div class="form-actions">
		<?php echo CHtml::submitButton($model->isNewRecord ? '作成' : '編集', array( 'class' => 'btn btn-large btn-success') ); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->



<!-- 関連商品選択モーダル用 -->
<div id="relmodal" style="display: none;">
</div>
<!-- /関連商品選択モーダル用 -->



<!-- オプション品モーダル用 -->
<div id="option_adds_modal" style="display: none;">
</div>
<!-- /オプション品用 -->



<!-- ローディングモーダル用 -->
<div id="loading_modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<!--
			<div class="modal-header">
				<h4>アップロード中</h4>
			</div>
			-->
			<div class="modal-body" style="text-align: center;">
				<p><img src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/img/loading.gif" /></p>
			</div>
			<!--
			<div class="modal-footer">
			</div>
			-->
		</div>
	</div>
</div>
<!-- /ローディングモーダル用 -->