<?php
/* @var $this InformationController */
/* @var $model InformationModel */

$this->breadcrumbs=array(
	'Information Models'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List InformationModel', 'url'=>array('index')),
	array('label'=>'Create InformationModel', 'url'=>array('create')),
	array('label'=>'Update InformationModel', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete InformationModel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage InformationModel', 'url'=>array('admin')),
);
?>

<h1>View InformationModel #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'subject',
		'create_date',
		'update_date',
		'display_date',
		'begin_date',
		'end_date',
		'open_status',
		'description',
		'contents',
	),
)); ?>
