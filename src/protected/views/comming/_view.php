<?php
/* @var $this CommingController */
/* @var $data CommingModel */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comming_date')); ?>:</b>
	<?php echo CHtml::encode($data->comming_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('memo1')); ?>:</b>
	<?php echo CHtml::encode($data->memo1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('memo2')); ?>:</b>
	<?php echo CHtml::encode($data->memo2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('memo3')); ?>:</b>
	<?php echo CHtml::encode($data->memo3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_date')); ?>:</b>
	<?php echo CHtml::encode($data->update_date); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('staff_id')); ?>:</b>
	<?php echo CHtml::encode($data->staff_id); ?>
	<br />

	*/ ?>

</div>