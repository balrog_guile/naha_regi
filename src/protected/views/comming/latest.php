<?php
/* 顧客別最新来店表示 */
$this->breadcrumbs=array();
$this->menu=array();
?>

<h1>来店管理</h1>


<div>
	<?php echo CHtml::button( 'このお客様の来店履歴', array('class' => 'btn btn-large btn-info history') ); ?>
	<?php echo CHtml::button( 'お会計', array('class' => 'btn btn-large btn-info go2register') ); ?>
</div>



<div class="row-fluid">
	<div class="box span12">
		<div class="box-header well">
			<h2>
				<i class="icon-info-sign"></i>お客様
			</h2>
			<div class="box-icon">
				<a class="btn btn-minimize btn-round" href="#">
					<i class="icon-chevron-up"></i>
				</a>
				<a class="btn btn-close btn-round" href="#">
					<i class="icon-remove"></i>
				</a>
			</div>
		</div>
		<div class="box-content">
			<div class="row-fluid">
				<div class="span6">
					<div>
						<strong>お名前</strong>: <?php echo $customer->name1; ?> <?php echo $customer->name2; ?>様
					</div>
					<div>
						<strong>かな</strong>: <?php echo $customer->kana1; ?> <?php echo $customer->kana2; ?>様
					</div>
					<div>
						<strong>メールアドレス</strong>: <?php echo $customer->mailaddress; ?>
					</div>
				</div>
				<div class="span6">
					<?php foreach( CustomerProfileFieldModel::model()->findAll('summary_display=1') as $one ): ?>
						<div>
							<strong><?php echo $one->field_label; ?></strong>: <?php echo $customer->{$one->field_name}; ?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</div>



<div class="_im_enclosure"><div class="_im_repeater">
	
	
	<?php echo CHtml::hiddenField( 'main_id', '', array( 'class' => 'IM[comming@id]') ); ?>
	
	<!-- メインの記録 -->
	<div class="row-fluid">
		<div class="box span12">
			<div class="box-header well">
				<h2>
					<i class="icon-info-sign"></i>来訪記録
				</h2>
				<div class="box-icon">
					<a class="btn btn-minimize btn-round" href="#">
						<i class="icon-chevron-up"></i>
					</a>
					<a class="btn btn-close btn-round" href="#">
						<i class="icon-remove"></i>
					</a>
				</div>
			</div>
			<div class="box-content">
				
				<div class="row-fluid">
					<div class="span2">来訪日</div>
					<div class="span10 IM[comming@comming_date]"></div>
				</div>
				<div class="row-fluid">
					<div class="span6">
						<label>メモ1</label>
						<?php echo CHtml::textArea( 'memo1', '', array( 'class' => 'IM[comming@memo1] input-xlarge focused') ); ?>
					</div>
					<div class="span6">
						<label>メモ2</label>
						<?php echo CHtml::textArea( 'memo2', '', array( 'class' => 'IM[comming@memo2] input-xlarge focused') ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /メインの記録 -->
	
	
	
	
	
	
	
	<!-- アセット -->
	<div class="row-fluid">
		<div class="box span12">
			<div class="box-header well">
				<h2>
					<i class="icon-info-sign"></i>アセット（画像／動画）
				</h2>
				<div class="box-icon">
					<a class="btn btn-minimize btn-round" href="#">
						<i class="icon-chevron-up"></i>
					</a>
					<a class="btn btn-close btn-round" href="#">
						<i class="icon-remove"></i>
					</a>
				</div>
			</div>
			<div class="box-content">
				
				<div class="_im_enclosure"><div class="_im_repeater">
					<div class="row-fluid">
						<div class="span4">
							<!-- レコードID -->
							<?php echo CHtml::hiddenField( 'id', '', array( 'class' => 'IM[comming_assets@id] assets_id') ); ?>
							<!-- ハッシュ値 -->
							<?php echo CHtml::hiddenField( 'hashcode', '', array( 'class' => 'IM[comming_assets@hashcode] hashcode') ); ?>
							<!-- データタイプ -->
							<?php echo CHtml::hiddenField( 'data_type', '', array( 'class' => 'IM[comming_assets@data_type] data_type') ); ?>
							<!-- パス -->
							<?php echo CHtml::hiddenField( 'path', '', array( 'class' => 'IM[comming_assets@path] path') ); ?>
							
							
							
							<!-- イメージ表示領域 -->
							<div class="image_place_area">
								<img src="" class="image_place_holder" />
							</div>
							
							<!-- 動画表示領域 -->
							<div class="movie_place_area">
								<video class="movie_place_holder" controls preload="auto" />
							</div>
							<div class="movie_place_area_no_sp">
								<?php echo CHtml::link( '動画再生', '', array( 'class' => 'movie_href btn btn-large btn-info', 'target' => '_blank', 'style' => 'width: 95%; height: 3em;' ) ); ?>
							</div>
							
							
							
						</div>
						<div class="span8">
							<div class="file_select_area">
								<?php echo CHtml::fileField( 'file', '', array( 'class' => 'asset_select_image', 'accept' => 'image/*' ) ); ?>
								<br />
								<?php echo CHtml::fileField( 'file', '', array( 'class' => 'asset_select_movie', 'accept' => 'video/*' ) ); ?>
							</div>
							<?php echo CHtml::textArea( 'memo2', '', array( 'class' => 'IM[comming_assets@memo1] focused', 'style' => 'width: 90%;') ); ?>
						</div>
					</div>
					<hr />
				</div></div>
				
			</div>
		</div>
	</div>
	<!-- /アセット -->
	
	
	
	<!-- お会計記録 -->
	<div class="_im_enclosure"><div class="_im_repeater">
	<div class="row-fluid">
		<div class="box span12">
			<div class="box-header well">
				<h2>
					<i class="icon-info-sign"></i>お会計記録
				</h2>
				<div class="box-icon">
					<a class="btn btn-minimize btn-round" href="#">
						<i class="icon-chevron-up"></i>
					</a>
					<a class="btn btn-close btn-round" href="#">
						<i class="icon-remove"></i>
					</a>
				</div>
			</div>
			<div class="box-content">
				<div class="row-fluid">
					<div class="span2">商品代金計</div>
					<div class="span4">
						<span class="IM[receipt@item_total]"></span>円
					</div>
					<div class="span2">お支払い日</div>
					<div class="span4">
						<span class="IM[receipt@payment_date]"></span>円
					</div>
				</div>
			</div>
		</div>
	</div>
	</div></div>
	<!-- /お会計記録 -->
	
	
</div></div>