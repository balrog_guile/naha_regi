<?php
/* @var $this ShopController */
/* @var $model ShopModel */

$this->breadcrumbs=array(
	'Shop Models'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ShopModel', 'url'=>array('index')),
	array('label'=>'Manage ShopModel', 'url'=>array('admin')),
);
?>

<h1>Create ShopModel</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>