<?php
/* =============================================================================
 * 店管理画面
 * ========================================================================== */
$this->breadcrumbs=array();
$this->menu=array();
?>
<h1>店舗管理</h1>

<div class="row-fluid sortable ui-sortable">
	<div class="box span12">
		<div class="box-header well" data-original-title="">
			<h2>
				<i class="icon-user"></i>
				店舗
			</h2>
			<div class="box-icon">
				<a class="btn btn-setting btn-round" href="#">
					<i class="icon-cog"></i>
				</a>
				<a class="btn btn-minimize btn-round" href="#">
					<i class="icon-chevron-up"></i>
				</a>
				<a class="btn btn-close btn-round" href="#">
					<i class="icon-remove"></i>
				</a>
			</div>
		</div>
		<div class="box-content">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>
							店舗名
						</th>
						<th>
							電話番号<br />
							FAX<br />
							メールアドレス
						</th>
						<th>
							住所
						</th>
						<th>
							操作
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<?php echo CHtml::textField( 'name[]', '', array( 'class' => 'IM[shop@name]' ) ); ?><br />
						</td>
						<td>
							<?php echo CHtml::textField( 'tel[]', '', array( 'class' => 'IM[shop@tel]' ) ); ?><br />
							<?php echo CHtml::textField( 'fax[]', '', array( 'class' => 'IM[shop@fax]' ) ); ?><br />
							<?php echo CHtml::textField( 'mailaddress[]', '', array( 'class' => 'IM[shop@mailaddress]' ) ); ?>
						</td>
						<td>
							<?php echo CHtml::textField( 'zip[]', '', array( 'class' => 'IM[shop@zip]' ) ); ?><br />
							<?php echo CHtml::textField( 'pref[]', '', array( 'class' => 'IM[shop@pref]' ) ); ?><br />
							<?php echo CHtml::textField( 'address1[]', '', array( 'class' => 'IM[shop@address1]' ) ); ?><br />
							<?php echo CHtml::textField( 'address2[]', '', array( 'class' => 'IM[shop@address2]' ) ); ?><br />
							<?php echo CHtml::textField( 'address3[]', '', array( 'class' => 'IM[shop@address3]' ) ); ?><br />
						</td>
						<td>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>