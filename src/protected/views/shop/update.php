<?php
/* @var $this ShopController */
/* @var $model ShopModel */

$this->breadcrumbs=array(
	'Shop Models'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ShopModel', 'url'=>array('index')),
	array('label'=>'Create ShopModel', 'url'=>array('create')),
	array('label'=>'View ShopModel', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ShopModel', 'url'=>array('admin')),
);
?>

<h1>Update ShopModel <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>