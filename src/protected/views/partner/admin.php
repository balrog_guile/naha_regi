<?php
/* @var $this PartnerController */
/* @var $model PartnerModel */

$this->breadcrumbs=array();

$this->menu=array();

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#partner-model-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>仕入先リスト</h1>

<!--
<div class="search-form" style="">
<?php /*$this->renderPartial('_search',array(
	'model'=>$model,
));*/ ?>
</div>
-->



<!-- 操作パネル -->
<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th"></i> 操作パネル</h2>
			<div class="box-icon">
			</div>
		</div>
		<div class="box-content">
			<div class="row-fluid">
				
				
				<?php echo CHtml::link(
						'新規登録',
						Yii::app()->createUrl('partner/create'),
						array( 'class' => 'btn btn-large btn-primary')
					);
				?>
			</div>
		</div>
	</div>
</div>
<!-- /操作パネル -->


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'partner-model-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'name' => 'id',
			'htmlOptions' => array(
				'width' => '30%'
			)
		),
		array(
			'name' => 'name',
			'htmlOptions' => array(
				'width' => '30%'
			)
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{list} {update} {delete}',
			'buttons'=>array(
				'list' => array(
					'label'=>'　仕入れリスト　',
					'url'=>'Yii::app()->createUrl("partner/list", array("id"=>$data->id))',
					'options' => array( 'class' => 'btn btn-large' ),
				)
			),
		),
		
	),
)); ?>
