<?php

class StockingController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	
	//アセット
	protected $assets_helper;
	// ----------------------------------------------------
	
	/**
	 * コンストラクタ
	 */
	public function __construct($id, $module = null) {
		parent::__construct($id, $module);
		$this->assets_helper = new AssetsHelper( 'ControllerJS' );
		
		
	}
	
	// ----------------------------------------------------
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			/*
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			*/
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update', 'admin','delete', 'stocking', 'delete_stock' ),
				'users'=>array('@'),
			),
			/*
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			*/
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	
	// ----------------------------------------------------
	
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new StockingModel;
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		if(isset($_POST['StockingModel']))
		{
			$model->attributes=$_POST['StockingModel'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}
		
		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		if(isset($_POST['StockingModel']))
		{
			$current = $model->purchase_qty;
			
			$_POST['StockingModel']['sub_total'] = 
					$_POST['StockingModel']['purchase_price'] *
					$_POST['StockingModel']['purchase_qty'];
			$model->attributes=$_POST['StockingModel'];
			if($model->save())
			{
				if( $current != $_POST['StockingModel']['purchase_qty'])
				{
					$ProductSkuModel = new ProductSkuModel();
					$target = $ProductSkuModel->find( 'id = :id', array(':id' => $model->sku_id ) );
					$target->stock = $target->stock + ( $_POST['StockingModel']['purchase_qty'] - $current );
					$target->save();
				}
				
				$this->redirect(
					Yii::app()->createUrl( 'product/stocking/stocking', array( 'skuid' => $model->sku_id ) )
				);
			}
		}
		
		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * SKU別入荷情報
	 * @param int $skuid
	 */
	public function actionStocking( $skuid )
	{
		$model = new StockingModel();
		$skuModel = new ProductSkuModel();
		$productsModel = new ProductsModel();
		
		//ターゲットのSKU
		$target_sku = $skuModel->find( array(
									'condition' => 'id = :id',
									'params' => array(
										':id' => $skuid,
									),
								) );
		
		//ターゲットの親ID
		$target_product = $productsModel->find(
								'id = :id',
								array(
										':id' => $target_sku->product_id
								)
							);
		
		
		//フォームが送信されてきたとき
		if(isset($_POST['StockingModel']))
		{
			if(
				( $_POST['StockingModel']['insert_date'] == '')||
				( strtotime($_POST['StockingModel']['insert_date']) === FALSE )
			){
				$_POST['StockingModel']['insert_date'] = date( 'Y-m-d H:i:s' );
			}
			$_POST['StockingModel']['sku_id'] = $skuid;
			
			
			$model->attributes = $_POST['StockingModel'];
			
			//総額計算
			$model->sub_total = $_POST['StockingModel']['purchase_price'] * $_POST['StockingModel']['purchase_qty'];
			
			if($model->save())
			{
				
				//SKUデータの追加
				$params = array();
				$params['pid'] = $target_sku->products->item_id;
				$params['sid'] = $target_sku->brunch_item_id;
				$params['stock'] = $_POST['StockingModel']['purchase_qty'];
				$params['opt'] = TRUE;
				$res = $skuModel->stockMod( $params );
				
				
				$this->redirect( Yii::app()->createUrl( 'product/stocking/stocking', array('skuid' => $skuid ) ) );
				return;
			}
			
		}
		
		//現在の資産額
		$stock = $model->findAll( array(
			'condition' => '( ( purchase_qty - leaving_qty ) > 0 ) AND ( sku_id = :sku_id )',
			'params' => array(':sku_id' => $skuid )
		) );
		$assets = 0;
		foreach( $stock as $one )
		{
			$assets += ( ( $one->purchase_qty - $one->leaving_qty ) * $one->purchase_price );
		}
		
		
		//アセットの処理
		$this->assets_helper->set_js( 'products/stocking/stocking.js', 'end' );
		$this->assets_helper->add_js( '
			
		', 'end');
		
		
		//仕入れ日
		$model->insert_date = date('Y-m-d H:i:s');
		
		
		//ビュー表示
		$this->render(
			'stocking',
			array(
				'model' => $model,
				'skuModel' => $skuModel,
				'target_sku' => $target_sku,
				'sku_id' => $skuid,
				'target_product' => $target_product,
				'assets'=> $assets,
				
			)
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * 削除
	 */
	public function actionDelete_stock( $id )
	{
		$row = StockingModel::model()->find( 'id = :id', array(':id' => $id ));
		
		$sku = $row->sku;
		$sku->stock -= $row->purchase_qty;
		$sku->save();
		
		$row->delete();
		
		$this->redirect(
			Yii::app()->createUrl( 'product/stocking/stocking', array( 'skuid' => $sku->id ) )
		);
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('StockingModel');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new StockingModel('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['StockingModel']))
			$model->attributes=$_GET['StockingModel'];
		
		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return StockingModel the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=StockingModel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	// ----------------------------------------------------
	
	/**
	 * Performs the AJAX validation.
	 * @param StockingModel $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='stocking-model-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	// ----------------------------------------------------
}