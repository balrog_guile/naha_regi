<?php

class ProductController extends Controller
{
	protected $assets_helper;
	
	// ----------------------------------------------------
	
	/**
	 * コンストラクタ
	 */
	public function __construct($id, $module = null) {
		parent::__construct($id, $module);
		$this->assets_helper = new AssetsHelper( 'ControllerJS' );
	}
	
	// ----------------------------------------------------
	
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			/*
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(),
				'users'=>array('*'),
			),
			*/
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array(
					'index','view','create','update','admin', 'delete',
					'regiview', 'skudelete', 'barcode', 'barcode2',
					'getrel', 'relview', 'Allview',
					'SearchOptionAdds', 'GetSkuView'
				),
				'users'=>array('@'),
			),
			/*
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array(),
			),
			*/
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * 関係商品登録用
	 */
	public function actionGetrel()
	{
		$json = json_decode(rawurldecode(Input::GetPost('json')));
		$ProductsModel = new ProductsModel();
		
		
		
		$items = array();
		foreach( $json as $id )
		{
			$item = $ProductsModel->find( 'id =:id', array( ':id' => $id ) );
			if(! is_null($item) )
			{
				$items[] = $item;
			}
		}
		
		
		$this->renderPartial(
			'getrel',
			array(
				'items' => $items,
			)
		);
		
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * レジ検索用
	 */
	public function actionRegiview()
	{
		$model=new ProductsModel('search');
		$model->unsetAttributes();
		
		//カテゴリリストの作成
		$categoryModel = new CategoryModel();
		$categories_list = array();
		foreach( $categoryModel->getAllSearch() as $row )
		{
			$categories_list[ $row->id ] = str_repeat( '─', $row->depth ) . $row->categori_name;
		}
		
		if(isset($_GET['ProductsModel']))
		{
			$model->attributes=$_GET['ProductsModel'];
		}
		$this->renderPartial(
			'regiview',
			array(
				'model'=>$model,
				'categories_list' => $categories_list,
			)
		);
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * 関連商品検索用
	 */
	public function actionRelview()
	{
		$model=new ProductsModel('search');
		$model->unsetAttributes();
		
		//カテゴリリストの作成
		$categoryModel = new CategoryModel();
		$categories_list = array();
		foreach( $categoryModel->getAllSearch() as $row )
		{
			$categories_list[ $row->id ] = str_repeat( '─', $row->depth ) . $row->categori_name;
		}
		
		if(isset($_GET['ProductsModel']))
		{
			$model->attributes=$_GET['ProductsModel'];
		}
		$this->renderPartial(
			'relview',
			array(
				'model'=>$model,
				'categories_list' => $categories_list,
			)
		);
		
	}
	
	
	// ----------------------------------------------------
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * バーコードテスト
	 */
	public function actionBarcode2( $setcode = null )
	{
		require('Image/Barcode2.php');
		if(is_null($setcode)){ $setcode = '4910000100067'; }
		$code = new Image_Barcode2();
		$code->draw( $setcode, 'code128', 'gif', true, 50, 2, false);
	}
	
	// ----------------------------------------------------
	
	/**
	 * SKUのモデルリストを作成
	 */
	protected function createSkuList( $parent_id = null )
	{
		$return = array(
			'model_list' => array(),
			'validation' => true,
		);
		
		$return['model_list'][0] = new ProductSkuModel;
		
		if(!isset ($_POST['ProductSkuModel']))
		{
			return $return;
		}
		
		$i = 0;
		$c = count($_POST['ProductSkuModel']);
		while( $i < $c )
		{
			$return['model_list'][$i+1] = new ProductSkuModel;
			
			/*
			if(!isset($_POST['ProductSkuModel'][$i]))
			{
				$i ++ ;
				continue;
			}
			*/
			
			//新規追加の場合
			if( $_POST['ProductSkuModel'][$i]['id'] == '' )
			{
				$return['model_list'][$i+1] = new ProductSkuModel;
				unset($_POST['ProductSkuModel'][$i]['id']);//←ID項目削除
				$_POST['ProductSkuModel'][$i]['product_id'] = 0;//←仮の親IDを設定
			}
			//アップデートの場合
			else
			{
				$return['model_list'][$i+1] = ProductSkuModel::model()->findByPk($_POST['ProductSkuModel'][$i]['id']);
				unset($_POST['ProductSkuModel'][$i]['id']);//←ID項目削除
				
			}
			
			//在庫数処理
			if( $_POST['ProductSkuModel'][$i]['stock'] == '' )
			{
				$_POST['ProductSkuModel'][$i]['stock'] = null;
			}
			//在庫処理はやめる
			unset( $_POST['ProductSkuModel'][$i]['stock'] );
			
			
			//バリデートの実行
			$return['model_list'][$i+1]->attributes = $_POST['ProductSkuModel'][$i];
			$res = $return['model_list'][$i+1]->validate();
			
			
			if( $res === FALSE )
			{
				
				$return['validation'] = FALSE;
			}
			
			$i ++ ;
		}
		
		return $return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * インストアコード発行
	 * @param object $sku
	 * @return void
	 */
	private function setInstoreCode( $sku )
	{
		
		switch( Yii::app()->params['barcode'] )
		{
			case 'JAN':
				$code = '21' . sprintf('%010d', $sku->id );
				$code .= self::calcJanCodeDigit( $code );
				$sku->code_str = $code;
				$sku->save();
				break;
			
			case 'INSTORE':
			default:
				$code = sprintf('%06d', $sku->id );
				$sku->code_str = $code;
				$sku->save();
				break;
			
		}
		return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * FAQリスト作成
	 * @param object $model(メインのモデル)
	 * @param boolean $update
	 */
	public function getFaqList( $model, $update = false )
	{
		$return = array(
			'set' => new FaqModel(),
			'models' => array()
		);
		
		
		if(isset($_POST['FaqModel']) )
		{
			$i = 0;
			foreach( $_POST['FaqModel'] as $attribute )
			{
				$faq = new FaqModel();
				if( $update === TRUE )
				{
					$attribute['create_date'] = date('Y-m-d H:i:s');
					$attribute['product_id'] = $model->id;
					$attribute['rank'] = $i;
				}
				else
				{
					$attribute['create_date'] = date('Y-m-d H:i:s');
					$attribute['update_date'] = date('Y-m-d H:i:s');
					$attribute['product_id'] = $model->id;
					$attribute['rank'] = $i;
				}
				$faq->attributes = $attribute;
				$return['models'][] = $faq;
				$i ++ ;
 			}
		}
		else
		{
			$return['models'] = $model->faq;
		}
		
		return $return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new ProductsModel;
		$skumodel = new ProductSkuModel;
		
		//カテゴリリストの作成
		$categoryModel = new CategoryModel();
		$categories_list = array();
		foreach( $categoryModel->getAllSearch() as $row )
		{
			$categories_list[ $row->id ] = str_repeat( '─', $row->depth ) . $row->categori_name;
		}
		
		
		//SKUのオブジェクトリスト
		$skumodel = array();
		$skuModelObjects = $this->createSkuList();
		$skumodel = $skuModelObjects['model_list'];
		
		
		//FAQのリスト
		$faq_list = $this->getFaqList( $model );
		
		
		//タグ
		$tags = array();
		
		
		//追加実行
		$skucheck = true;
		if(isset($_POST['ProductsModel']))
		{
			$_POST['ProductsModel']['create_date'] = date('Y-m-d H:i:s');
			$_POST['ProductsModel']['update_date'] = date('Y-m-d H:i:s');
			$model->attributes = $_POST['ProductsModel'];
			$res = $model->validate();
			
			
			
			if(
				($res === TRUE)&&
				($skuModelObjects['validation'] === TRUE )&&
				( count($skumodel) > 1 )
			)
			{
				$model->save();
				
				
				////SKU保存
				$ii = 0;
				$images = Input::Post('sku_image_path');
				foreach( $skumodel as $sku )
				{
					$sku->product_id = $model->id;//親IDをセーブ
					$sku->save();
					
					
					//skuに対する画像を保存
					if(
						(!isset($images[$ii-1]))||
						(!is_array($images[$ii-1]))
					)
					{
						$images[$ii-1] = array();
					}
					ProductSkuImagesModel::model()->deleteAll(
							'sku_id = :sku_id',
							array( ':sku_id' => $sku->id )
						);
					$xx = 0;
					foreach( $images[$ii-1] as $image )
					{
						$ProductSkuImagesModel = new ProductSkuImagesModel();
						$ProductSkuImagesModel->sku_id = $sku->id;
						$ProductSkuImagesModel->image = $image;
						$ProductSkuImagesModel->rank = $xx;
						$ProductSkuImagesModel->create_date = date('Y-m-d H:i:s');
						$ProductSkuImagesModel->update_date = date('Y-m-d H:i:s');
						//var_dump( $ProductSkuImagesModel->save() );
						$ProductSkuImagesModel->save();
						$xx ++ ;
					}
					
					
					//追加画像の保存
					$add_images = Input::Post('add_images');
					$add_images_content = Input::Post('add_images_content');
					if(!is_array($add_images))
					{
						$add_images = array();
					}
					if(!is_array($add_images_content))
					{
						$add_images_content = array();
					}
					$xx = 0;
					ProductImagesModel::model()->deleteAll(array(
						'condition' => 'product_id = :product_id',
						'params' => array(
							':product_id' => $model->id
						)
					));
					foreach( $add_images as $add_image )
					{
						$ProductImagesModel = new ProductImagesModel();
						$ProductImagesModel->product_id = $model->id;
						$ProductImagesModel->image = $add_image;
						$ProductImagesModel->rank = $xx;
						$ProductImagesModel->content = $add_images_content[$xx];
						$ProductImagesModel->create_date = date('Y-m-d H:i:s');
						$ProductImagesModel->update_date = date('Y-m-d H:i:s');
						$ProductImagesModel->save();
						$xx ++;
					}
					
					
					//JAN（インストアコード）発行
					if( $sku->code_str == '' )
					{
						$this->setInstoreCode($sku);
					}
					
					$ii ++;
				}
				
				
				
				//関連商品を保存
				$RelProductToProductModel = new RelProductToProductModel();
				$json = json_decode( rawurldecode( Input::GetPost('rel_json') ) );
				$RelProductToProductModel->deleteAll( 'products_id = :products_id', array(':products_id'=> $model->id ));
				foreach( $json as $one )
				{
					$RelProductToProductModel2 = new RelProductToProductModel();
					$RelProductToProductModel2->products_id = $model->id;
					$RelProductToProductModel2->rel_product_id = $one;
					$RelProductToProductModel2->save();
				}
				
				
				//////タグ処理
				$tags = Input::Post('tags');
				if(!is_array($tags))
				{
					$tags = array();
				}
				foreach( $tags as $tag )
				{
					$tag_model = TagsModel::model()->find('name = :name', array( ':name'=>$tag ) );
					if(is_null($tag_model))
					{
						$tag_model = new TagsModel();
						$tag_model->name = $tag;
						$tag_model->create_date = date('Y-m-d H:i:s');
						$tag_model->update_date = date('Y-m-d H:i:s');
						$tag_model->save();
					}
					
					$rel_product_tags = new RelProductTagsModel();
					$rel_product_tags->product_id = $model->id;
					$rel_product_tags->tag_id = $tag_model->id;
					$rel_product_tags->save();
					
				}
				
				
				/////オプションSKU
				$optionsku = Input::Post('optionsku');
				if(!is_array($optionsku))
				{
					$optionsku = array();
				}
				$c = 1;
				foreach( $optionsku as $opt )
				{
					$RelOptionSkuModel = new RelOptionSkuModel();
					$RelOptionSkuModel->product_id = $model->id;
					$RelOptionSkuModel->sku_id = $opt;
					$RelOptionSkuModel->rank = $c;
					$RelOptionSkuModel->save();
					$c ++;
				}
				
				
				//セッションにOKを保存
				Yii::app()->session['create_ok'] = true;
				
				//リダイレクト
				$this->redirect(array('update','id'=>$model->id));
				
				return;
			}
			
			///////エラー処理
			
			//SKUなしエラー
			if( count($skumodel) == 1 )
			{
				$skucheck = false;
			}
			
			//タグの再処理
			$tags = Input::Post('tags');
			if(!is_array($tags))
			{
				$tags = array();
			}
			
			//オプションの再処理
			$optionsku = Input::Post('optionsku');
			if(!is_array($optionsku))
			{
				$optionsku = array();
			}
			$model->optionsku = array();
			foreach( $optionsku as $one )
			{
				$sku = ProductSkuModel::model()->find( 'id = :id', array(':id' => $one));
				$p = new stdClass();
				$p->sku = $sku;
				$model->reinpit_optionsku[] = $p;
			}
		}
		
		
		//アセットの処理
		$this->assets_helper->set_js( 'products/products/form.js', 'end' );
		$this->assets_helper->add_js( '
			var baseUrl = "' . Yii::app()->baseUrl .'";
			var homeUrl = "' . Yii::app()->homeUrl .'";
			var skuDeleteUrl = "' . Yii::app()->createUrl( 'product/product/skudelete' ) .'";
			var getRelUrl = "' . Yii::app()->createUrl( 'product/product/getrel' ) .'";
			var addOptionsUrl = "' . Yii::app()->createUrl( 'product/product/SearchOptionAdds' ) .'";
			var relUrl = "' . Yii::app()->createUrl( 'product/product/relview' ) .'";
		', 'end');
		
		
		$this->render('create',array(
			'model'=>$model,
			'skumodel' => $skumodel,
			'categories'=>$categories_list,
			'create_mode' => true,
			'skucheck' => $skucheck,
			'create_ok' => null,
			'rel_json' => json_encode(array()),
			'faq_list' => $faq_list,
			'tags' => $tags,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$model->item_status = explode('\n', $model->item_status);
		
		$categoryModel = new CategoryModel();
		$categories_list = array();
		foreach( $categoryModel->getAllSearch() as $row )
		{
			$categories_list[ $row->id ] = str_repeat( '─', $row->depth ) . $row->categori_name;
		}
		
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		$skucheck = true;
		
		
		//SKU呼び出し
		if(!isset($_POST['ProductSkuModel']))
		{
			$skumodel = ProductSkuModel::model()->findAll(
					array(
						'order'=>'rank',
						'condition'=>'product_id=:product_id',
						'params'=>array( 'product_id' => $id )
					)
				);
			array_unshift( $skumodel, ProductSkuModel::model() );
		}
		//SKUのオブジェクトリスト
		else
		{
			$skumodel = array();
			$skuModelObjects = $this->createSkuList();
			$skumodel = $skuModelObjects['model_list'];
		}
		
		
		//FAQのリスト
		$faq_list = $this->getFaqList( $model, true );
		
		
		//タグリスト
		$tags = array();
		foreach( $model->tags as $tag )
		{
			$tags[] = $tag->tag->name;
		}
		
		
		//編集実行
		if(isset($_POST['ProductsModel']))
		{
			$_POST['ProductsModel']['update_date'] = date('Y-m-d H:i:s');
			$model->attributes=$_POST['ProductsModel'];
			if(is_array($model->item_status))
			{
				$model->item_status = implode('\n', $model->item_status);
			}
			
			//FAQのバリデーション
			$faq_validation = TRUE;
			foreach( $faq_list['models'] as $faq_model )
			{
				$faq_validation = $faq_model->validate();
			}
			
			
			$res = $model->validate();
			if(
				($res === TRUE)&&
				( count($skumodel) > 1 )&&
				($skuModelObjects['validation'] === TRUE )&&
				( $faq_validation === TRUE )
				
			)
			{
				//商品をセーブ
				$model->save();
				
				
				
				///////SKUを保存
				$ii = 0;
				$images = Input::Post('sku_image_path');
				foreach( $skumodel as $sku )
				{
					//新規追加
					if( $sku->product_id == 0 )
					{
						$sku->product_id = $model->id;//親IDをセーブ
					}
					$sku->save();
					
					
					
					//skuに対する画像を保存
					if(
						(!isset($images[$ii-1]))||
						(!is_array($images[$ii-1]))
					)
					{
						$images[$ii-1] = array();
					}
					ProductSkuImagesModel::model()->deleteAll(
							'sku_id = :sku_id',
							array( ':sku_id' => $sku->id )
						);
					$xx = 0;
					foreach( $images[$ii-1] as $image )
					{
						$ProductSkuImagesModel = new ProductSkuImagesModel();
						$ProductSkuImagesModel->sku_id = $sku->id;
						$ProductSkuImagesModel->image = $image;
						$ProductSkuImagesModel->rank = $xx;
						$ProductSkuImagesModel->create_date = date('Y-m-d H:i:s');
						$ProductSkuImagesModel->update_date = date('Y-m-d H:i:s');
						//var_dump( $ProductSkuImagesModel->save() );
						$ProductSkuImagesModel->save();
						$xx ++ ;
					}
					
					
					
					//追加画像の保存
					$add_images = Input::Post('add_images');
					$add_images_content = Input::Post('add_images_content');
					if(!is_array($add_images))
					{
						$add_images = array();
					}
					if(!is_array($add_images_content))
					{
						$add_images_content = array();
					}
					$xx = 0;
					ProductImagesModel::model()->deleteAll(array(
						'condition' => 'product_id = :product_id',
						'params' => array(
							':product_id' => $model->id
						)
					));
					foreach( $add_images as $add_image )
					{
						$ProductImagesModel = new ProductImagesModel();
						$ProductImagesModel->product_id = $model->id;
						$ProductImagesModel->image = $add_image;
						$ProductImagesModel->rank = $xx;
						$ProductImagesModel->content = $add_images_content[$xx];
						$ProductImagesModel->create_date = date('Y-m-d H:i:s');
						$ProductImagesModel->update_date = date('Y-m-d H:i:s');
						$ProductImagesModel->save();
						$xx ++;
					}
					
					
					//JAN（インストアコード）発行
					if( $sku->code_str == '' )
					{
						$this->setInstoreCode( $sku );
					}
					
					
					$ii ++ ;
				}
				
				
				/////オプションSKU
				$optionsku = Input::Post('optionsku');
				if(!is_array($optionsku))
				{
					$optionsku = array();
				}
				RelOptionSkuModel::model()->deleteAll(
						'product_id = :product_id',
						array( ':product_id' => $model->id)
					);
				$c = 1;
				foreach( $optionsku as $opt )
				{
					$RelOptionSkuModel = new RelOptionSkuModel();
					$RelOptionSkuModel->product_id = $model->id;
					$RelOptionSkuModel->sku_id = $opt;
					$RelOptionSkuModel->rank = $c;
					$RelOptionSkuModel->save();
					$c ++;
				}
				
				
				///////FAQを保存
				foreach( $faq_list['models'] as $faq_model )
				{
					$faq_model->save();
				}
				
				
				//////タグ処理
				$tags = Input::Post('tags');
				if(!is_array($tags))
				{
					$tags = array();
				}
				RelProductTagsModel::model()->deleteAll(
						'product_id = :product_id',
						array(':product_id' => $model->id )
				);
				foreach( $tags as $tag )
				{
					$tag_model = TagsModel::model()->find('name = :name', array( ':name'=>$tag ) );
					if(is_null($tag_model))
					{
						$tag_model = new TagsModel();
						$tag_model->name = $tag;
						$tag_model->create_date = date('Y-m-d H:i:s');
						$tag_model->update_date = date('Y-m-d H:i:s');
						$tag_model->save();
					}
					
					$rel_product_tags = new RelProductTagsModel();
					$rel_product_tags->product_id = $model->id;
					$rel_product_tags->tag_id = $tag_model->id;
					$rel_product_tags->save();
					
				}
				
				
				///////関連商品を保存
				$RelProductToProductModel = new RelProductToProductModel();
				$json = json_decode( rawurldecode( Input::GetPost('rel_json') ) );
				$RelProductToProductModel->deleteAll( 'products_id = :products_id', array(':products_id'=> $model->id ));
				foreach( $json as $one )
				{
					$RelProductToProductModel2 = new RelProductToProductModel();
					$RelProductToProductModel2->products_id = $model->id;
					$RelProductToProductModel2->rel_product_id = $one;
					$RelProductToProductModel2->save();
				}
				
				//セッションにOKを保存
				Yii::app()->session['create_ok'] = true;
				
				$this->redirect(array('update','id'=>$model->id));
				return;
			}
			
			
			///////エラー用の処理
			
			if( count($skumodel) == 1 )
			{
				$skucheck = false;
			}
			
			
			//タグの再処理
			$tags = Input::Post('tags');
			if(!is_array($tags))
			{
				$tags = array();
			}
			
			//オプションの再処理
			$optionsku = Input::Post('optionsku');
			if(!is_array($optionsku))
			{
				$optionsku = array();
			}
			$model->optionsku = array();
			foreach( $optionsku as $one )
			{
				$sku = ProductSkuModel::model()->find( 'id = :id', array(':id' => $one));
				$p = new stdClass();
				$p->sku = $sku;
				$model->reinpit_optionsku[] = $p;
			}
			
			
		}
		
		
		//アセットの処理
		$this->assets_helper->set_js( 'products/products/form.js', 'end' );
		$this->assets_helper->add_js( '
			var baseUrl = "' . Yii::app()->baseUrl .'";
			var homeUrl = "' . Yii::app()->homeUrl .'";
			var stockingUrl = "' . Yii::app()->createUrl( 'product/stocking/stocking', array('skuid' => '' ) ) .'";
			var skuDeleteUrl = "' . Yii::app()->createUrl( 'product/product/skudelete' ) .'";
			var getRelUrl = "' . Yii::app()->createUrl( 'product/product/getrel' ) .'";
			var addOptionsUrl = "' . Yii::app()->createUrl( 'product/product/SearchOptionAdds' ) .'";
			var relUrl = "' . Yii::app()->createUrl( 'product/product/relview' ) .'";
		', 'end');
		
		
		//関連商品取得
		//$RelProductToProductModel = new RelProductToProductModel();
		$rel_procucts = $model->rel_product_to_product;
		$rel = array();
		foreach( $rel_procucts as $row )
		{
			$rel[] = $row->id;
		}
		$rel_json = json_encode( $rel );
		$update = array();
		
		
		//セッションデータ
		$create_ok = Yii::app()->session['create_ok'];
		Yii::app()->session['create_ok'] = null;
		
		$this->render('update',array(
			'model'=>$model,
			'categories'=>$categories_list,
			'skumodel' => $skumodel,
			'create_mode' => false,
			'skucheck' => $skucheck,
			'create_ok' => $create_ok,
			'rel_json' => $rel_json,
			'faq_list' => $faq_list,
			'tags' => $tags,
		));
	}
	
	// ----------------------------------------------------
	
	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$productsModel = new ProductsModel();
		$skuModel = new ProductSkuModel();
		
		//子を消す
		$skuList = $skuModel->findAll( 'product_id = :product_id', array( ':product_id' => $id ) );
		foreach( $skuList as $row )
		{
			$row->delete();
		}
		
		//メインを消す
		$product = $productsModel->find( 'id = :id',  array( 'id' => $id ) );
		$product->delete();
		
		
		//メッセージを残す
		Yii::app()->session['product_error_message'] = '商品を削除しました';
		
		
		//一覧へリダイレクト
		$this->redirect( Yii::app()->createUrl('product/product/admin') );
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * SKU削除
	 */
	public function actionSkudelete()
	{
		$id = Input::Get( 'id' );
		
		$sku = new ProductSkuModel();
		$target = $sku->find( 'id = :id', array( ':id' => $id ) );
		$res = $target->delete();
		
		echo json_encode( $res );
		
	}
	// ----------------------------------------------------
	
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->redirect( $this->createUrl('product/product/admin') );
		return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new ProductsModel('search');
		$model->unsetAttributes();
		
		
		$categoryModel = new CategoryModel();
		$categories_list = array();
		foreach( $categoryModel->getAllSearch() as $row )
		{
			$categories_list[ $row->id ] = str_repeat( '─', $row->depth ) . $row->categori_name;
		}
		
		if(isset($_GET['ProductsModel']))
		{
			$model->attributes=$_GET['ProductsModel'];
		}
		
		$this->render('admin',array(
			'model'=>$model,
			'product_error_message' => Yii::app()->session['product_error_message'],
			'categories_list' => $categories_list,
		));
	}
	// ----------------------------------------------------
	
	/**
	 * バーコード一覧
	 */
	public function actionBarcode()
	{
		$model=new ProductsModel('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ProductsModel']))
		{
			$model->attributes=$_GET['ProductsModel'];
		}
		
		
		$categoryModel = new CategoryModel();
		$categories_list = array();
		foreach( $categoryModel->getAllSearch() as $row )
		{
			$categories_list[ $row->id ] = str_repeat( '─', $row->depth ) . $row->categori_name;
		}
		
		
		
		$this->render('barcode',array(
			'model'=>$model,
			'product_error_message' => Yii::app()->session['product_error_message'],
			'categories' => $categories_list,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * オプションSKU用検索
	 */
	public function actionSearchOptionAdds()
	{
		$model = new ProductsModel('search');
		$model->unsetAttributes();
		if(isset($_GET['ProductsModel']))
		{
			$model->attributes=$_GET['ProductsModel'];
		}
		$this->renderPartial(
			'option_adds',
			array(
				'model' => $model
			)
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * 該当商品のSKUを列挙
	 */
	public function actionGetSkuView( $id )
	{
		$product = ProductsModel::model()->findByPk($id);
		if(is_null( $product ) )
		{
			$this->redirect( array('admin') );
			return;
		}
		$this->renderPartial( 'get_sku_view',array('product' => $product ));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ProductsModel the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ProductsModel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	// ----------------------------------------------------
	
	/**
	 * Performs the AJAX validation.
	 * @param ProductsModel $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='products-model-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	// ----------------------------------------------------
	
	/**
	 * JANコード用チェックデジット
	 * 
	 */
	public static function calcJanCodeDigit($num){
		$arr = str_split($num);
		$odd = 0;
		$mod = 0;
		for($i=0;$i<count($arr);$i++){
			if(($i+1) % 2 == 0) {
				//偶数の総和
				$mod += intval($arr[$i]);
			}
			else
			{
				//奇数の総和
				$odd += intval($arr[$i]);
			}
		}
		//偶数の和を3倍+奇数の総和を加算して、下1桁の数字を10から引く
		$cd = 10 - intval(substr((string)($mod * 3) + $odd,-1));
		
		//10なら1の位は0なので、0を返す。
		return $cd === 10 ? 0 : $cd;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 全データの一覧
	 */
	public function actionAllview()
	{
		$this->renderPartial('allview');
	}
	
	// ----------------------------------------------------
}
