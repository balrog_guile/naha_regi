<?php
class CategoriesController extends Controller
{
	
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	
	protected $assets_helper;
	protected $jqueryui;
	
	// ----------------------------------------------------
	
	/**
	 * コンストラクタ
	 */
	public function __construct($id, $module = null) {
		parent::__construct($id, $module);
		$this->assets_helper = new AssetsHelper( 'ControllerJS' );
		
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','view','admin','delete', 'rank' ),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new CategoryModel;
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		if(isset($_POST['CategoryModel']))
		{
			$model->attributes=$_POST['CategoryModel'];
			$model->create_date = date('Y-m-d H:i:s');
			$model->update_date = date('Y-m-d H:i:s');
			$model->set_depth();
			
			
			if($model->save())
			{
				$this->redirect( $this->createUrl( 'product/categories/admin') );
			}
		}
		
		$this->assets_helper->set_js( 'products/category/form.js', 'end' );
		$this->assets_helper->set_js( 'upload_helper.js', 'end' );
		$this->assets_helper->add_js(
				'var get_parent_categories = "' . Yii::app()->createUrl('product/categories/parent_list') .'";',
				'end'
			);
		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		if(isset($_POST['CategoryModel']))
		{
			$model->attributes=$_POST['CategoryModel'];
			$model->update_date = date('Y-m-d H:i:s');
			$model->set_depth( TRUE );
			if( $model->getMotoParent($model->id) != $model->parent_id )
			{
				$model->setInSaveSort( true );
			}
			else
			{
				$model->setInSaveSort( false );
			}
			
			
			if($model->save())
			{
				//$this->redirect(array('view','id'=>$model->id));
				$this->redirect( $this->createUrl( 'product/categories/admin') );
			}
		}
		$this->assets_helper->set_js( 'products/category/form.js', 'end' );
		$this->assets_helper->set_js( 'upload_helper.js', 'end' );
		$this->assets_helper->add_js(
				'var get_parent_categories = "' . Yii::app()->createUrl('product/categories/parent_list') .'";',
				'end'
			);
		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->redirect( $this->createUrl( '/product/categories/admin') );
		return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * ランク変更
	 */
	public function actionRank( $id, $mode )
	{
		$model=new CategoryModel;
		$model->move_rank( $id, $mode );
		
		$this->redirect( $this->createAbsoluteUrl('product/categories/admin') );
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * 親リスト
	 */
	public function actionParent_list()
	{
		$model = new CategoryModel;
		$res = $model->getCategoryList();
		$this->renderPartial( 'parent_view', array( 'datas' =>$res ));
		
	}
	
	// ----------------------------------------------------
	
	
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		//echo Yii::app()->user->name;
		//echo Yii::app()->user->first_name;
		//var_dump( Yii::app()->user->isAdmin() );
		
		$model=new CategoryModel('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['CategoryModel']))
			$model->attributes=$_GET['CategoryModel'];
		
		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return CategoryModel the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=CategoryModel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	// ----------------------------------------------------
	
	/**
	 * Performs the AJAX validation.
	 * @param CategoryModel $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='category-model-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	// ----------------------------------------------------
}
