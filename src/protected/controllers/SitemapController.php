<?php

class SitemapController extends Controller
{
	public function actionIndex()
	{
		
		//商品ページを検索
		$datas_products = ProductsModel::model()->findAll(
				'open_status = :open_status',
				array(
					':open_status' => 1
				)
			);
		
		//カテゴリページを検索
		$datas_category = CategoryModel::model()->findAll();
		
		//静的ページを検索
		$domain_url = Yii::app()->getBaseUrl(true);
		$dir_path = YiiBase::getPathOfAlias( 'webroot.ec_theme.default' );
		$static_pages = $this->getDirectories( $dir_path );
		
		
		//XML用のレスポンスヘッダ
		header("Content-Type: text/xml");
		
		//XMLの生成を開始
		echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
		
		//トップページ分
		echo '<url>
		<loc>' . Yii::app()->getBaseUrl(true) . '/</loc>
		<lastmod>' . date('Y-m-d H:i:s') . '</lastmod>
		<changefreq>daily</changefreq>
		<priority>1.0</priority>
	</url>';
		
		
		//商品ページ分
		foreach( $datas_products as $row )
		{
			echo '<url><loc>' . $domain_url . '/archives/' . $row->link_url . '.html</loc>' . "\n" ;
			echo '<lastmod>' . $row->update_date . '</lastmod></url>' . "\n";
		}
		
		//カテゴリページ分
		foreach( $datas_category as $row )
		{
			echo '<url><loc>' . $domain_url . '/category/' . $row->link_url . '.html</loc>' . "\n";
			echo '<lastmod>' . $row->update_date . '</lastmod></url>' . "\n";
		}
		
		//静的ページ分
		foreach( $static_pages as $page )
		{
			echo '<url><loc>' . $page['url'] .'</loc>' . "\n";
			echo '<lastmod>' . $page['mtime'] . '</lastmod></url>' . "\n";
		}
		
		echo '</urlset>' . "\n";
	}
	
	// ----------------------------------------------------
	
	/**
	 * 回帰処理用
	 * @param string $dir
	 */
	private function getDirectories( $dir )
	{
		$return = array();
		
		//除外ファイルと除外ディレクトリを定義
		$outfiles = array(
			YiiBase::getPathOfAlias( 'webroot.ec_theme.default' ) . '/index.html',
		);
		$outdirs = array(
			YiiBase::getPathOfAlias( 'webroot.ec_theme.default.cart' ),
			YiiBase::getPathOfAlias( 'webroot.ec_theme.default.categories' ),
			YiiBase::getPathOfAlias( 'webroot.ec_theme.default.errors' ),
			YiiBase::getPathOfAlias( 'webroot.ec_theme.default.item_pages' ),
		);
		
		
		//ファイルを検索
		$files = glob( $dir . '/*.html' );
		
		//置き換えの準備
		$check_base = YiiBase::getPathOfAlias( 'webroot.ec_theme.default' );
		$checke_to_change = Yii::app()->getBaseUrl(true);
		$preg_quote = '/^' . str_replace( '/', '\/', $check_base ) . '/';
		
		//ファイルをループ
		foreach ($files as $file)
		{
			if(in_array( $file, $outfiles ))
			{
				continue;
			}
			
			//配列に突っ込む
			//このとき正規表現置換でURLに変換
			$return[] = array(
				'url' => preg_replace( $preg_quote, $checke_to_change, $file ),
				'mtime' => $update_time = date('Y-m-d H:i:s', filemtime( $file ) ),
				
			);
		}
		
		//サブディレクトリもチェック
		$dirs = glob( $dir .'/*', GLOB_ONLYDIR );
		foreach( $dirs as $child_dir )
		{
			if(in_array($child_dir, $outdirs))
			{
				continue;
			}
			$res = $this->getDirectories( $child_dir );
			$return = array_merge( $res, $return );
		}
		
		
		return $return;
	}
	
	
	// ----------------------------------------------------
	
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}

