<?php
/* =============================================================================
 * ファイルアップローダー
 * ========================================================================== */
class FileuploadController extends Controller
{
	public function actionIndex()
	{
		$item = new FileUploadModel();
		$item->attributes = $_POST['FileUploadModel'];
		
		$return_datas = array(
			'result' => false,
			'full_path' => '',
			'full_url' => '',
			'filename' => '',
			'error_message' => '',
		);
		
		
		//バリデーションチェック
		if ($item->validate())
		{
			$image = CUploadedFile::getInstance($item, 'image');
			
			if ($image !== null) {
				
				//送信されたモードによって分配
				switch( $_POST['FileUploadModel']['mode'] )
				{
					case 'category_main':
						$dir = 'category_main/';
						break;
					
					case 'category_thumb':
						$dir = 'category_thumb/';
						break;
					
					case 'customer_assets':
						$dir = 'customer_assets/';
						break;

					case 'product_images':
						$dir = 'product_images/';
						break;
					
					default:
						$dir = '';
					
				}
				
				//ファイル名は時間から判断するように
				$item->image = sha1( time() . '-' . rand(0,2000) ) . '.' . $image->extensionName;
				
				$upload_dir = YiiBase::getPathOfAlias('webroot') . '/files/' . $dir . '/';
				$save_file = $upload_dir . $item->image;
				$save_res_file = '';
				$res_prf = 'res_';
				$retrun_prefix = '';
				
				//ファイルの保存
				if(is_file($save_file))
				{
					unlink($save_file);
				}
				else
				{
					$image->saveAs( $save_file );
					
					//画像リサイズ
					if( exif_imagetype($save_file) !== FALSE )
					{
						$res = $item->imageResize($save_file);
						
						// returnされてきた画像をタイプ別に処理
						if( $res !== FALSE )
						{
							$save_res_file = $upload_dir . $res_prf . $item->image;
							switch($res['type'])
							{
								case 'jpg':
									imagejpeg($res['image'], $save_res_file);
								break;
								case 'gif':
									imagegif($res['image'], $save_res_file);
								break;
								case 'png':
									imagepng($res['image'], $save_res_file);
								break;
							}
							imagedestroy($res['image']);
							$retrun_prefix .= 'res_';
						}
					}
				}
				
				
				//リターンの組み立て
				$return_datas = array(
					'result' => true,
					'full_path' => $save_file,
					'full_url' => Yii::app()->getBaseUrl(true) . '/files/' . $dir . $retrun_prefix . $item->image,
					'absolute_url' => '/files/' . $dir . $retrun_prefix . $item->image,
					'filename' => $retrun_prefix . $item->image,
					'error_message' => '',
					'full_res_path' => $save_res_file,
					'absolute_res_url' => '/files/' . $dir . $res_prf . $item->image,
				);
				
			}
			else
			{
				$return_datas['error_message'] = $item->getError('image');
			}
			
		}
		//バリデーションエラー
		else
		{
			$return_datas['error_message'] = $item->getError('image');
		}
		
		
		echo json_encode( $return_datas );
	}

	
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}