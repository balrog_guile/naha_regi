<?php

class EcpagesController extends Controller
{
	
	//テンプレートフォルダ（システム内）
	public $themeDirInside;
	
	//テンプレートフォルダ（出力用）
	public $themeDirOutput;
	
	//URLセグメントリスト
	public $segment = array();
	
	//読み込み中のテンプレートファイルを保存
	public $currentTemplate = '';
	
	// ----------------------------------------------------
	
	// Uncomment the following methods and override them if needed
	
	
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			
			'accessControl',
			
			/*
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
			*/
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * アクセスフィルタリング
	 */
	public function accessRules()
	{
		
		return array(
			
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array( 'index' ),
				'users'=> array( (Yii::app()->params['open'] === TRUE)?'*':'@' ),
				
			),
			
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	// ----------------------------------------------------
	
	/*
	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
	
	// ----------------------------------------------------
	
	/**
	 * ページ処理
	 */
	public function actionIndex()
	{
		//将来的にここでThemeセットの判定を入れる
		$themeset = 'default/';
		
		//テンプレートフォルダ
		$this->themeDirInside = YiiBase::getPathOfAlias( 'webroot.ec_theme' ) . '/' . $themeset;
		$this->themeDirOutput = Yii::app()->getBaseUrl(true) . '/ec_theme/' . $themeset;
		$this->themeDirOutput = preg_replace( '/^http\:/', '', $this->themeDirOutput );
		$this->themeDirOutput = preg_replace( '/^https\:/', '', $this->themeDirOutput );
		
		
		//セグメント
		$this->segment = explode('/', Yii::app()->request->pathInfo);
		
		
		//インデックスページ
		if(
			( Yii::app()->request->pathInfo == '' )||
			( (count($this->segment)==1)&&($this->segment[0] == 'index.html' )  )
		)
		{
			$this->pages_index();
			return;
		}
		//商品カテゴリページ
		elseif( $this->segment[0] == 'category' )
		{
			$this->category();
			return;
		}
		//商品ページ
		elseif( $this->segment[0] == 'archives' )
		{
			$this->item_pages();
			return;
		}
		//通常のHTMLページ
		elseif(preg_match( '/\.html$/', $this->segment[count($this->segment)-1]))
		{
			$this->pages();
			return;
		}
		//404
		else
		{
			$this->page404('ページがみつかりません');
			return;
		}
	}
	
	// ----------------------------------------------------
	
	/**
	 * 各ページ共通データ
	 */
	protected function setCommonData( &$data )
	{
		//カテゴリリスト
		$categoryModel = new CategoryModel();
		$data['categoryList'] = $categoryModel->getCategoryListAll();
		
		//アプリケーションパス
		$data['template_path_inside'] = $this->themeDirInside;
		$data['webroot'] = Yii::app()->getBaseUrl(true);
		$data['webroot'] = preg_replace( '/^http\:/', '', $data['webroot'] );
		$data['webroot'] = preg_replace( '/^https\:/', '', $data['webroot'] );
		
		//このコントローラー
		$data['that'] = $this;
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * インデックスページ
	 */
	public function pages_index()
	{
		$data = array();
		
		//必要データの追加
		$data['template_path'] = $this->themeDirOutput;
		
		//共通データ処理
		$this->setCommonData( $data );
		
		//ビューの表示
		$this->currentTemplate = $this->themeDirInside.'index.html';
		$this->renderFile( $this->currentTemplate, $data );
	}
	
	// ----------------------------------------------------
	
	/**
	 * カテゴリページ
	 */
	public function category()
	{
		$data = array();
		
		//必要データの追加
		$data['template_path'] = $this->themeDirOutput;
		
		
		//カテゴリデータの作成
		$file = $this->segment[count($this->segment)-1];
		$category = preg_replace( '/\.html$/', '', $file );
		
		$categoryModel = new CategoryModel();
		$data['category'] = $categoryModel->find( 'link_url = :link_url', array( ':link_url' => $category ) );
		if(is_null($data['category']))
		{
			$this->page404( 'カテゴリは見つかりません' );
		}
		
		//子カテゴリ
		$data['children'] = $categoryModel->getCategoryListAll( $data['category']->id );
		
		
		//商品データ
		$relProductCategoriesModel = new RelProductCategoriesModel();
		$find = $relProductCategoriesModel->findAll( 'categories_id = :categories_id', array(':categories_id'=>$data['category']->id) );
		$items = array();
		foreach( $find as $row )
		{
			if(
				(is_array($row->product_search))&&
				( count($row->product_search) > 0 )
			)
			{
				$items[] = $row->product_search[0];
			}
		}
		$data['items'] = $items;
		
		
		//共通データ処理
		$this->setCommonData( $data );
		
		//ビューの表示
		$this->currentTemplate = $this->themeDirInside.'categories/index.html';
		$this->renderFile( $this->currentTemplate, $data );
	}
	
	// ----------------------------------------------------
	
	/**
	 * 商品ページ
	 */
	public function item_pages()
	{
		$data = array();
		
		//必要データの追加
		$data['template_path'] = $this->themeDirOutput;
		
		
		//商品ページデータ
		$file = $this->segment[count($this->segment)-1];
		$itempage = preg_replace( '/\.html$/', '', $file );
		
		
		//商品ページデータの作成
		$productsModel = new ProductsModel();
		$data['product'] = $productsModel->find( 'link_url = :link_url', array( ':link_url' => $itempage ));
		if( is_null($data['product']) )
		{
			$this->page404( '商品が見つかりません' );
			return;
		}
		if( $data['product']->open_status != 1 )
		{
			$this->page404( '商品が見つかりません' );
			return;
		}
		
		//カテゴリデータ取得
		$data['cateogries'] = array();
		foreach( $data['product']->categories_many as $row )
		{
			$one = array( $row );
			if( $row->parent_id > 0 )
			{
				$one = $this->getParentsCategory( $row->parent_id, $one );
			}
			$data['cateogries'][] = $one;
		}
		
		
		//共通データ処理
		$this->setCommonData( $data );
		
		//ビューの表示
		$this->currentTemplate = $this->themeDirInside.'item_pages/index.html';
		$this->renderFile( $this->currentTemplate, $data );
	}
	
	// ----------------------------------------------------
	
	/**
	 * カテゴリの親をたどる
	 * @param int $id
	 * @param array $list
	 * @param array list
	 */
	protected function getParentsCategory( $id, $list )
	{
		$CategoryModel = new CategoryModel();
		$row = $CategoryModel->find( 'id = :id', array(':id' => $id ));
		if(is_null( $row ))
		{
			return $list;
		}
		
		$list[] = $row;
		if( $row->parent_id > 0 )
		{
			$list = $this->getParentsCategory( $row->parent_id, $list );
		}
		
		return $list;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 通常ページ
	 */
	public function pages()
	{
		$data = array();
		
		//必要データの追加
		$data['template_path'] = $this->themeDirOutput;
		
		//ページファイル
		$page = $this->themeDirInside.Yii::app()->request->pathInfo;
		$this->currentTemplate = $page;
		
		//なしのとき
		if(!file_exists($page))
		{
			$this->page404('ページがみつかりません');
			return;
		}
		
		//共通データ処理
		$this->setCommonData( $data );
		
		//ビューの表示
		$this->renderFile( $page, $data );
	}
	
	// ----------------------------------------------------
	
	/**
	 * 404ページ
	 * @param string $errMessage
	 */
	public function page404( $errMessage )
	{
		$data = array();
		
		//必要データの追加
		$data['template_path'] = $this->themeDirOutput;
		$data['error_message'] = $errMessage;
		
		
		//共通データ処理
		$this->setCommonData( $data );
		
		//ビューの表示
		header("HTTP/1.1 404 Not Found");
		$this->currentTemplate = $this->themeDirInside.'errors/404.html';
		$this->renderFile( $this->currentTemplate, $data );
		return;
	}
	// ----------------------------------------------------
	
	/**
	 * メールフォーム組み込みメソッド
	 */
	public function setMailForm( $file = 'mail_attribute' )
	{
		$dir = dirname( $this->currentTemplate );
		require_once( $dir .'/' . $file . '.php' );
		$mailModel = new $file;
		
		//もしモードが確認画面だったらバリデーション
		if( $mailModel->mode == 'confirm' )
		{
			$res = $mailModel->execValidate();
			if( $res === FALSE )
			{
				$mailModel->mode = 'input';
			}
		}
		
		//再入力モード
		if( $mailModel->mode == 'reinput' )
		{
			$mailModel->setAttr();
			$mailModel->mode = 'input';
		}
		
		//メール送信モード
		if( $mailModel->mode == 'send' )
		{
			$res = $mailModel->execValidate();
			if( $res === FALSE )
			{
				$mailModel->mode = 'input';
			}
			else
			{
				$mailModel->sendMail();
				
				if(preg_match( '/\?/', $_SERVER["REQUEST_URI"] ))
				{
					$uri = str_replace( '?', '?mailstep=thanks&', $_SERVER["REQUEST_URI"] );
				}
				else if(preg_match( '/\#/', $_SERVER["REQUEST_URI"] ))
				{
					$uri = str_replace( '#', '?mailstep=thanks#', $_SERVER["REQUEST_URI"] );
				}
				else{
					$uri = $_SERVER["REQUEST_URI"] . '?mailstep=thanks';
				}
				$url = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $uri;
				Yii::app()->controller->redirect($url);
				return;
				
				//$mailModel->mode = 'thanks';
			}
		}
		
		return $mailModel;
	}
	
	// ----------------------------------------------------
}