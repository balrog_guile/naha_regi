<?php
/* =============================================================================
 * フラグリスト定義
 * ========================================================================== */
class VarHelper{
	
	// ----------------------------------------------------
	
	/**
	 * オーダーステータス取得
	 * @param int $status
	 * @param bool $opt
	 */
	public static function order_status( $status = FALSE, $opt = FALSE )
	{
		//$status = (String)$status;
		$return = array(
			'' => '',
			
			//店内用
			'0' => '入力中',
			'1' => 'オーダー確定',
			'2' => 'お支払い済み',
			'3' => '掛売',
			'4' => 'キャンセル',
			'5' => '返品',
			
			//通販用
			'10' => '通販オーダー済み',
			
			
		);
		
		
		if($opt === FALSE)
		{
			unset($return['']);
		}
		
		if( $status === FALSE )
		{
			return $return;
		}
		
		return $return[$status];
	}
	// ----------------------------------------------------
	
	/**
	 * 性別ステータス取得
	 */
	public static function getGender()
	{
		
		$return = array(
			'0' => '男性',
			'1' => '女性',
		);
		
		return $return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 顧客ステータスリスト取得
	 */
	public static function getCustomerStatus()
	{
		
		$return = array(
			'-1' => '退会',
			'0' => '非アクティブ',
			'1' => 'アクティブ',
		);
		
		return $return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 都道府県リスト取得
	 */
	public static function getPrefList()
	{
		$list = array(
			'' => '選択してください',
			'北海道' => '北海道',
			'青森県' => '青森県',
			'岩手県' => '岩手県',
			'宮城県' => '宮城県',
			'秋田県' => '秋田県',
			'山形県' => '山形県',
			'福島県' => '福島県',
			'茨城県' => '茨城県',
			'栃木県' => '栃木県',
			'群馬県' => '群馬県',
			'埼玉県' => '埼玉県',
			'千葉県' => '千葉県',
			'東京都' => '東京都',
			'神奈川県' => '神奈川県',
			'新潟県' => '新潟県',
			'富山県' => '富山県',
			'石川県' => '石川県',
			'福井県' => '福井県',
			'山梨県' => '山梨県',
			'長野県' => '長野県',
			'岐阜県' => '岐阜県',
			'静岡県' => '静岡県',
			'愛知県' => '愛知県',
			'三重県' => '三重県',
			'滋賀県' => '滋賀県',
			'京都府' => '京都府',
			'大阪府' => '大阪府',
			'兵庫県' => '兵庫県',
			'奈良県' => '奈良県',
			'和歌山県' => '和歌山県',
			'鳥取県' => '鳥取県',
			'島根県' => '島根県',
			'岡山県' => '岡山県',
			'広島県' => '広島県',
			'山口県' => '山口県',
			'徳島県' => '徳島県',
			'香川県' => '香川県',
			'愛媛県' => '愛媛県',
			'高知県' => '高知県',
			'福岡県' => '福岡県',
			'佐賀県' => '佐賀県',
			'長崎県' => '長崎県',
			'熊本県' => '熊本県',
			'大分県' => '大分県',
			'宮崎県' => '宮崎県',
			'鹿児島県' => '鹿児島県',
			'沖縄県' => '沖縄県',
		);
		
		
		return $list;
	}
	
	// ----------------------------------------------------
	
	/**
	 * フィールドタイプ
	 */
	public static function getCustomerFieldTypes()
	{
		$return = array(
			'INT' => '整数',
			'FLOAT' => '小数点付き数値',
			'TEXT' => '文字列',
			'LONGTEXT' => 'とても長い文字列',
			'DATE' => '日付',
			'TIME' => '時間',
			'DATETIME' => '日付+時間'
		);
		
		return $return;
	}
	// ----------------------------------------------------
	
	/**
	 * 顧客フィールド入力タイプ
	 */
	public static function getCustomerFieldInputType()
	{
		$return = array(
			'0' => 'テキスト（1行）',
			'1' => 'テキスト（複数行）',
			'2' => 'チェックボックス',
			'3' => 'ラジオボタン',
			'4' => 'セレクトボックス',
			'5' => '日付',
			'6' => '時間',
			'7' => '日付+時間',
		);
		
		return $return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 公開ステータス取得
	 */
	public static function getStatus()
	{
		
		$return = array(
			'1' => '公開',
			'2' => '非公開',
		);
		
		return $return;
	}

	// ----------------------------------------------------
	
	/**
	 * 商品ステータス取得
	 */
	public static function getItemStatus()
	{
		
		$return = array(
			'' => '',
			'1' => 'NEW',
			'2' => '残りわずか',
			'3' => 'ピックアップ',
			'4' => 'ギャラリー',
			'5' => '限定品',
		);
		
		return $return;
	}

	// ----------------------------------------------------
	
	/**
	 * オプション作成
	 */
	public static function createOptions($objs, $valName, $keyName = 'id')
	{
		$return = array('' => '');
		foreach($objs as $obj)
		{
			$return[$obj->$keyName] = $obj->$valName;
		}
		
		return $return;
	}
	
	// ----------------------------------------------------
	
}