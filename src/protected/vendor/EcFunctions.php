<?php
/* =============================================================================
 * EC用ファンクション
 * ========================================================================== */
class EcFunctions
{
	// ----------------------------------------------------
	
	/**
	 * ラベル付きの商品
	 * @param int $target
	 * @param int $limit
	 * @return array
	 */
	static public function getLabelProducts( $target = 1, $limit = 6 )
	{
		$productsModel = new ProductsModel;
		return $productsModel->findAll(array(
			'condition' => '(item_status LIKE :item_status) AND ( open_status = 1 )',
			'params' => array( ':item_status' => '%' . $target . '%' ),
			'limit' => $limit,
			'order' => 'create_date DESC',
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * 指定カテゴリのデータを取得する
	 * @param string $name(URIに相当）
	 * @param int $limit
	 * @return array
	 */
	public function getCategoryProducts( $name, $limit = 5 )
	{
		$list = array();
		$CategoryModel = new CategoryModel();
		$datas = $CategoryModel->getBelongtoList( $name, $list, $limit );
		return $datas;
	}
	
	// ----------------------------------------------------
	
	/**
	 * お知らせを取得
	 * @param int $limit
	 */
	public static function getInformation( $limit = 10 )
	{
		return InformationModel::model()
			->findAll(array( 'limit' => $limit, 'order' => 'display_date DESC'));
	}
	
	// ----------------------------------------------------
	
	/**
	 * 税率取得
	 * @param datetime $target
	 * @return int
	 */
	public static function getTaxRate( $target = null )
	{
		if(is_null($target))
		{
			$target = date('Y-m-d H:i:s');
		}
		
		$return = 0;
		foreach( Yii::app()->params['cart_config']['tax_rate'] as $check_date => $rate )
		{
			if( strtotime($target) >= strtotime( $check_date ) )
			{
				$return = $rate;
			}
		}
		return $return;
		
	}
	
	// ----------------------------------------------------
	/**
	 * 税込価格の取得
	 * @param int $price
	 * @return int
	 */
	public static function getTaxInPrice( $price )
	{
		if(
			(!isset( $price ) )||
			(is_null( $price ) )||
			($price === '')
		)
		{
			return '';
		}
		
		//税法に合わせて端数切り捨てで処理
		return floor( $price * ( 1 + ( self::getTaxRate() / 100 ) ) );
	}
	
	// ----------------------------------------------------
}