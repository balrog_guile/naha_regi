<?php
/* =============================================================================
 * ハイサイカート操作クラス
 * ========================================================================== */
class HaisaiCart
{
	//在庫判定用
	public $zaiko = array();
	
	// ----------------------------------------------------
	
	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		$this->init();
	}
	
	// ----------------------------------------------------
	
	/**
	 * カート初期化
	 */
	public function init( $force = FALSE )
	{
		$data = Yii::app()->session['cart'];
		if( $force === TRUE )
		{
			$data = null;
		}
		if(!is_array($data))
		{
			$data = array(
				'items' => array(),
				'payment_method' => array(),
				'info' => array(),
				'other_data' => array(),
				'error_message' => '',//専用エラーメッセージ
			);
			Yii::app()->session['cart'] = $data;
		}
		return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 専用エラーメッセージの格納
	 * @param string $message
	 */
	public function setError( $message )
	{
		$data = Yii::app()->session['cart'];
		$data['error_message'] = $message;
		Yii::app()->session['cart'] = $data;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 専用エラーメッセージの取得
	 * @param type $delete trueならメッセージ削除
	 * @return type 
	 */
	
	public function getError( $delete = TRUE )
	{
		$data = Yii::app()->session['cart'];
		$return = $data['error_message'];
		
		if( $delete === TRUE )
		{
			$data['error_message'] = '';
		Yii::app()->session['cart'] = $data;
		}
		
		return $return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 支払い方法リストを取得
	 */
	public function getPaymentList()
	{
		$return = Yii::app()->params['cart_config']['payment_method'];
		return $return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 商品追加
	 * @param array $item
	 */
	public function addItem( $items )
	{
		//キー生成
		$key = sha1( time() . '-'. rand(0,10000) );
		
		
		//追加用変数
		$set_items = array();
		
		
		//ループする
		//1度に入る複数商品は1明細として扱う
		foreach( $items as $item )
		{
			
			//格納データの作成
			$arr = array(
				'item_id' => $item['item_id'],//品番
				'item_name' => $item['item_name'],//品名
				'base_price' => $item['base_price'],//基本金額
				'qty' => $item['qty'],//数量
				'skuid' => $item['skuid'],//SKUID
			);
			
			//追加変数
			$set_items[] = $arr;
			
		}
		
		
		
		
		//追加実行
		$data = Yii::app()->session['cart'];
		$data['items'][$key] = $set_items;
		Yii::app()->session['cart'] = $data;
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * 数量変更
	 * @param array $delete_list 削除リスト（対称キーが入った配列）
	 * @param array $qty_list 数量リスト（キー=>数量の配列）
	 */
	public function changeQty( $delete_list, $qty_list )
	{
		//元データ
		$datas = Yii::app()->session['cart'];
		
		
		//数量変更
		foreach( $qty_list as $key => $qty )
		{
			if(!array_key_exists($key, $datas['items']))
			{
				continue;
			}
			
			//数量変更ができるのはオーダーメイド以外だけのため
			//アイテムの最初だけを処理
			$datas['items'][$key][0]['qty'] = $qty;
		}
		
		
		//削除
		foreach( $delete_list as $key )
		{
			unset( $datas['items'][$key] );
		}
		
		
		//セッションに戻す
		Yii::app()->session['cart'] = $datas;
		
		
		return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * お客様情報セット
	 * @param array $list 
	 */
	
	public function setInfo( $list )
	{
		$data = Yii::app()->session['cart'];
		$data['info'] = $list;
		Yii::app()->session['cart'] = $data;
	}
	
	// ----------------------------------------------------
	
	/**
	 * お客様情報取得
	 * @param string $key
	 */
	public function getInfo( $key = null )
	{
		if(is_null($key))
		{
			return Yii::app()->session['cart']['info'];
		}
		
		$data = Yii::app()->session['cart']['info'];
		if( isset($data[$key]) )
		{
			return $data[$key];
		}
		return null;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 支払い情報保存
	 * @param array $list
	 */
	public function setPayment( $list )
	{
		$data = Yii::app()->session['cart'];
		$data['payment_method'] = $list;
		Yii::app()->session['cart'] = $data;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 支払情報取得
	 * @param type $key 
	 */
	
	
	public function getPayment( $key = null )
	{
		if(is_null($key))
		{
			return Yii::app()->session['cart']['payment_method'];
		}
		
		$data = Yii::app()->session['cart']['payment_method'];
		
		if(isset($data[$key]))
		{
			return $data[$key];
		}
		
		return null;
	}
	
	// ----------------------------------------------------\
	
	/**
	 * その他情報の記録
	 * @param string $key
	 * @param mix $val 
	 */
	
	public function setOther( $key, $val )
	{
		if(is_null($val))
		{
			unset( Yii::app()->session['cart']['other_data'][$key] );
			return;
		}
		
		Yii::app()->session['cart']['other_data'][$key] = $val;
	}
	
	// ----------------------------------------------------
	
	/**
	 * その他データの取得
	 * @param string $key
	 */
	public function getOhter( $key )
	{
		return Yii::app()->session['cart']['other_data'][$key];
	}
	
	// ----------------------------------------------------
	
	/**
	 * 計算して取得
	 */
	
	public function calc()
	{
		
		//返り値の定義
		$return = array(
			
			//商品情報の配列
			'items' => array(),
			
			//合計計算
			'calc' => array(
				'item_total' => 0,//商品代金合計
				'item_total_tax' => 0,//商品代金合計
				'item_count' => 0,//かごにはいっている数
				'tax' => 0,//消費税額
				'commission_fee' => 0,//支払い手数料
				'commission_fee_tax' => 0,//支払い手数料
				'delivery_fee' => 0,//配送手数料
				'delivery_fee_tax' => 0,//配送手数料
				'grand_total' => 0,//お支払い合計
				'grand_total_tax' => 0,//お支払い合計
				'stock_error' => false,//在庫エラーがある場合
			),
			
			//文字列データ
			'string' => array(
				'payment_method' => '',//支払い方法を保存
			),
			
		);
		
		
		//かごの中計算
		$this->calcItems( $return );
		
		
		//フィー計算
		$this->callFee( $return );
		
		
		return $return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 在庫計算用に追加
	 * @param int $sku_id
	 * @param int $qty
	 * @return void
	 */
	protected function setZaikoCalc( $sku_id, $qty )
	{
		if(!array_key_exists( $sku_id, $this->zaiko ))
		{
			$this->zaiko[ $sku_id ] = $qty;
		}
		else
		{
			$this->zaiko[ $sku_id ] += $qty;
		}
		return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 現在状態の指定SKUidの在庫をチェック
	 * @param int $sku_id
	 * @return array
	 */
	protected function checkZaiko( $sku_id )
	{
		//在庫チェックの対象に入っていない
		if(!array_key_exists( $sku_id, $this->zaiko ))
		{
			return null;
		}
		
		//SKUモデルに問い合わせ
		$ProductSkuModel = new ProductSkuModel();
		$sku = $ProductSkuModel->find( 'id = :id', array( ':id' => $sku_id ) );
		
		//データがなければ在庫チェックの対象外として処理
		if( is_null( $sku ) )
		{
			return null;
		}
		
		//stockがnullなら在庫チェックの対象外
		if( $sku->stock === null )
		{
			return null;
		}
		
		
		//チェック開始
		$current_qty = $this->zaiko[ $sku_id ];
		
		$return = array(
				'result' => true,
				'current_stock' => $sku->stock,
			);
		
		if( $current_qty > $sku->stock )
		{
			$return['result'] = false;
		}
		return $return;
	}
	// ----------------------------------------------------
	
	/**
	 * 計算 商品情報をループ
	 * @param ref $return 
	 */
	
	protected function calcItems( &$return )
	{
		
		foreach( Yii::app()->session['cart']['items'] as $key => $val )
		{
			
			//金額計算
			$i = 0;
			$set_value = array(
				'items' => array(),//同時追加商品
				'sub_calc' => array(
					'sub_total' => 0,
					'sub_total_tax' => 0,
					'target_price' => 0,
					'base_price' => 0,
					'qty' => 0,
					'is_ordermade' => false,
					'base_price' => 0,
					'base_price_tax' => 0,
				),
			);
			foreach( $val as $one )
			{
				//調整分
				$target_price = $one['base_price'];
				$one['base_price_tax'] = EcFunctions::getTaxInPrice( $target_price );
				$target_price_tax = $one['base_price_tax'];
				
				//小計の小計
				$one['sub_total'] = $target_price * $one['qty'];
				$one['sub_total_tax'] = $target_price_tax * $one['qty'];
				
				//明細行の追加
				$set_value['items'][] = $one;
				
				//合計
				$set_value['sub_calc']['base_price'] = $one['base_price'];
				$set_value['sub_calc']['base_price_tax'] = $one['base_price_tax'];
				$set_value['sub_calc']['sub_total'] += $one['sub_total'];
				$set_value['sub_calc']['sub_total_tax'] += $one['sub_total_tax'];
				$set_value['sub_calc']['target_price']+= $target_price;
				
				//同時購入が1なら全体の個数は1のものに準じる
				if( $i == 0 )
				{
					$set_value['sub_calc']['qty'] = $one['qty'];
				}
				
				//SKU_idが記録されていれば、在庫のチェック対象
				if( $one['skuid'] != '' )
				{
					$this->setZaikoCalc( $one['skuid'], $one['qty'] );
				}
				
				$i ++;
			}
			
			
			//合計
			if( $i > 1 )
			{
				$set_value['sub_calc']['qty'] = 1;
			}
			
			
			//オーダーメイド判定
			if( $i == 1 )
			{
				$set_value['sub_calc']['is_ordermade'] = false;
			}
			else
			{
				$set_value['sub_calc']['is_ordermade'] = true;
			}
			
			
			//配列に追加
			$return['items'][$key] = $set_value;
			
			
			//商品代金合計計算
			$return['calc']['item_total'] += $set_value['sub_calc']['sub_total'];
			$return['calc']['item_total_tax'] += $set_value['sub_calc']['sub_total_tax'];
			
			
			//かごに入っている数
			$return['calc']['item_count'] ++ ;
			
		}
	}

	// ----------------------------------------------------
	
	/**
	 * 支払い手数料など計算
	 * @param type $return 
	 */
	
	protected function callFee( &$return )
	{
		//設定を取得
		$config = Yii::app()->params['cart_config'];
		
		
		//在庫チェックを発動
		foreach( $return['items']  as $key => $val )
		{
			$i = 0;
			foreach( $val['items'] as $one )
			{
				//SKUIDがあればチェック
				if( $one['skuid'] != '' )
				{
					$res = $this->checkZaiko( $one['skuid'] );
					//nullの場合は在庫チェックなし
					if( $res === null )
					{
						$return['items'][$key]['items'][$i]['stock_error'] = false;
						$return['items'][$key]['items'][$i]['current_stock'] = null;
					}
					//在庫チェックエラー
					else if( $res['result'] === FALSE )
					{
						$return['items'][$key]['items'][$i]['stock_error'] = TRUE;
						$return['items'][$key]['items'][$i]['current_stock'] = $res['current_stock'];
						$return['calc']['stock_error'] = TRUE;
					}
					//在庫チェックOK
					else
					{
						$return['items'][$key]['items'][$i]['stock_error'] = FALSE;
						$return['items'][$key]['items'][$i]['current_stock'] = $res['current_stock'];
					}
				}
				
				$i ++;
			}
		}
		
		
		//支払い設定を取得
		$payment_method = $this->getPayment( 'payment_method' );
		
		if(!is_null($payment_method))
		{
			$payment = $config['payment_method'][ $payment_method ];
			$return['string']['payment_method'] = $payment['name'];
		}
		else
		{
			$return['string']['payment_method'] = null;
			$payment = array(
				'name' => null,
				'fee' => array()
			);
		}
		
		
		//支払い手数料
		foreach( $payment['fee'] as $one )
		{
			if( $one[0] <= $return['calc']['item_total'] )
			{
				$return['calc']['commission_fee'] = $one[1];
			}
		}
		$return['calc']['commission_fee_tax'] = EcFunctions::getTaxInPrice( $return['calc']['commission_fee'] ); 
		
		//送料計算
		$delivery_method = $this->getPayment( 'delivery_method' );
		if(!is_null($delivery_method))
		{
			//設定ファイルの特定
			$file = YiiBase::getPathOfAlias('application.config.deliv_fee.'.$delivery_method) .'.php';
			require $file;
			
			//全国一律送料
			if( $delivery_method::$eveness === TRUE )
			{
				foreach( $delivery_method::$eveness_config as $one )
				{
					if( $one[0] <= $return['calc']['item_total'] )
					{
						$return['calc']['delivery_fee'] = $one[1];
					}
				}
				
			}
			//県別送料
			else
			{
				//対象のチェック
				if( $this->getInfo('d_check') == 1 )
				{
					$target = $this->getInfo('d_pref');
				}
				else
				{
					$target = $this->getInfo('pref');
				}
				foreach( $delivery_method::$pref[$target] as $one )
				{
					if( $one[0] <= $return['calc']['item_total'] )
					{
						$return['calc']['delivery_fee'] = $one[1];
					}
				}
			}
		}
		$return['calc']['delivery_fee_tax'] = EcFunctions::getTaxInPrice($return['calc']['delivery_fee']);
		
		
		//総合計を取得
		$return['calc']['grand_total'] = 
					$return['calc']['delivery_fee'] +
					$return['calc']['item_total'] +
					$return['calc']['commission_fee'];
		$return['calc']['grand_total_tax'] = 
					$return['calc']['delivery_fee_tax'] +
					$return['calc']['item_total_tax'] +
					$return['calc']['commission_fee_tax'];
		
		
		return;
	}
	
	// ----------------------------------------------------
}