/* =============================================================================
 * 商品フォーム制御用
 * ========================================================================== */
$(document).ready(function(){
	
	// ----------------------------------------------------
	
	/**
	 * プロパティ
	 **/
	var children_data_tr;
	
	// ----------------------------------------------------
	
	/**
	 * SKU画像アップロードエリア
	 */
	$('.sku_image_set_area').each(function(){
		//sku_name_set_area
		var index = $('.sku_image_set_area').index( this );
		$(this).height(
			$('.sku_name_set_area').eq(index).height()
		);
	});
	
	// ----------------------------------------------------
	
	/**
	 * SKU画像の削除
	 */
	$('.sku_image_rank_delete').on(
		'click',
		function(eve){
			var tr_index = $(this).attr('data-skuimage-index');
			var index = $('.sku_image_add_area:eq(' + tr_index + ') tbody tr .sku_image_rank_delete').index(this);
			$('.sku_image_add_area:eq(' + tr_index + ') tbody tr').eq(index).remove();
		}
	);
	
	
	// ----------------------------------------------------
	
	/**
	 * SKU画像の順番変更
	 */
	$('.sku_image_rank_change').on(
		'click',
		function(e){
			
			//インデックスなど取得
			var tr_index = $(this).attr('data-skuimage-index');
			var type =  $(this).attr('data-sku-image-rank-type');
			var check = '.sku_image_add_area:eq(' + tr_index + ') tbody tr .sku_image_rank_change[data-sku-image-rank-type='+type+']';
			var index = $(check).index(this);
			var len = $(check).length;
			
			//読み飛ばし設定
			if( ( index == 0 ) && ( type == 'up' ) )
			{
				return;
			}
			
			if( ( (index+1) == len ) && ( type == 'down' ) )
			{
				return;
			}
			
			
			//データ入れ替え
			var that = $('.sku_image_add_area:eq(' + tr_index + ') tbody tr').eq(index);
			var clone = $(that).clone(true);
			$(that).fadeOut(
				'normal',
				function(){
					
					if( type == 'up' )
					{
						$('.sku_image_add_area:eq(' + tr_index + ') tbody tr').eq(index-1).before( that );
					}
					else
					{
						$('.sku_image_add_area:eq(' + tr_index + ') tbody tr').eq(index+1).after( that );
					}
					$(that).show();
				}
			);
			
		}
	);
	
	// ----------------------------------------------------
	
	/**
	 * SKU画像アップロード
	 */
	var sku_image_tr = $('.sku_image_add_area').eq(0).find('tbody tr').eq(0).clone(true);
	$('.sku_image_add_area').each(function(){
		$(this).find('tbody tr').eq(0).remove();
	});
	
	//画像アップロード
	$('.sku_image_drop_area').on('drop', function (e) {
		var files = e.originalEvent.dataTransfer.files;
		var index = $('.sku_image_drop_area').index( this );
		
		$(this).css('background-color','#FFF');
		
		
		function setskuimages(i)
		{
			var url = homeUrl + '/fileupload/';
			
			// form生成
			var _form = $('<form></form>');
			_form.append('<input type="hidden" name="FileUploadModel[mode]" value="product_images" />' );
			var formData = new FormData(_form.get(0));
			formData.append('FileUploadModel[image]', files[i]);
			
			$.ajax(
				url,
				{
					'type': 'post',
					'contentType': false,
					'processData': false,
					'data': formData,
					'async': false,
					'dataType': 'json',
					'success': function(data){
						if(data.result == false ){
							alert(data.error_message);
						}
						var imgPath = data.absolute_url;
						var imgResPath = data.absolute_res_url;
						var clone = $(sku_image_tr).clone(true);
						$(clone).find('img').attr('src',baseUrl + imgResPath);
						$(clone).find('input[name^=sku_image_path]').val(imgResPath);
						
						//name属性を変更
						$(clone).find('input[name^=sku_image_path]').attr('name','sku_image_path['+index+'][]' );
						
						//対象インデックスを作成
						$(clone).find('.sku_image_rank_change,.sku_image_rank_delete').attr('data-skuimage-index', index );
						
						//追加
						$('.sku_image_add_area').eq(index).find('tbody').append(clone);
						
						//次の処理
						i ++ ;
						if( i == files.length )
						{
							
						}else{
							setskuimages(i)
						}
						
					},
					'error': function(e){
						$('#loading_modal').modal('hide');
						alert('アップロードに失敗しました');
					}
				}
			);
			
			
		}
		setskuimages(0);
		
		return false;
	})
	.on('dragenter', function () {
		$(this).css('background-color','#FFA0A2');
		// false を返してデフォルトの処理を実行しないようにする
		return false;
	})
	.on('dragover', function () {
		// false を返してデフォルトの処理を実行しないようにする
		return false;
	})
	.on('dragleave', function () {
		$(this).css('background-color','#FFF');
		// false を返してデフォルトの処理を実行しないようにする
		return false;
	})
	;
	
	// ----------------------------------------------------
	
	/**
	 * SKU画像手動選択
	 */
	$('.sku_image_select_area').on(
		'change',
		function(e){
			
			var url = homeUrl + '/fileupload/';
			
			// form生成
			var _form = $('<form></form>');
			_form.append('<input type="hidden" name="FileUploadModel[mode]" value="product_images" />' );
			var formData = new FormData(_form.get(0));
			formData.append('FileUploadModel[image]', $(this).get(0).files[0]);
			var index = $('.sku_image_select_area').index(this);
			
			
			$.ajax(
				url,
				{
					'type': 'post',
					'contentType': false,
					'processData': false,
					'data': formData,
					'async': false,
					'dataType': 'json',
					'success': function(data){
						if(data.result == false ){
							alert(data.error_message);
						}
						var imgPath = data.absolute_url;
						var imgResPath = data.absolute_res_url;
						var clone = $(sku_image_tr).clone(true);
						$(clone).find('img').attr('src',baseUrl + imgResPath);
						$(clone).find('input[name^=sku_image_path]').val(imgResPath);
						
						//name属性を変更
						$(clone).find('input[name^=sku_image_path]').attr('name','sku_image_path['+index+'][]' );
						
						//対象インデックスを作成
						$(clone).find('.sku_image_rank_change,.sku_image_rank_delete').attr('data-skuimage-index', index );
						
						//追加
						$('.sku_image_add_area').eq(index).find('tbody').append(clone);
						
					},
					'error': function(e){
						$('#loading_modal').modal('hide');
						alert('アップロードに失敗しました');
					}
				}
			);
		}
	);
	
	// ----------------------------------------------------
	
	/**
	 * SKUの行処理
	 */
	(function(){
		children_data_tr = $('#children_data_tr').clone(true);
		$(children_data_tr).removeAttr('id');
		$('#children_data_tr').remove();
	})();
	
	// ----------------------------------------------------
	
	/**
	 * SKU追加
	 */
	$('.sku_add').on(
		'click',
		function(e){
			
			//行追加
			var children_data_tr_add = $(children_data_tr).clone(true);
			
			//現在の行数を取得
			var current_rows = $('#children_data_table tbody .children_data_tr').length;
			
			//行番号変更
			var i = 0;
			while( $(children_data_tr_add).find('input[name^=ProductSkuModel]').length > i ){
				var set_name = $(children_data_tr_add).find('input[name^=ProductSkuModel]').eq(i).attr('name');
				set_name = set_name.replace( '[0]', '[' + current_rows + ']' );
				$(children_data_tr_add).find('input[name^=ProductSkuModel]').eq(i).attr('name', set_name);
				i ++ ;
			}
			
			
			$('#children_data_table>tbody').append( children_data_tr_add );
			
			
			//ランク入れ替えボタン
			rank_buttons();
			
		}
	);
	
	// ----------------------------------------------------
	
	/**
	 * skuテーブルのアクティブ・非アクティブの色替え
	 */
	
	$('.sku_delete_flag').on(
		'click',
		function(evet){
			var index = $('.sku_delete_flag').index( this );
			sku_delete_chk( index );
		}
	);
	
	function sku_delete_chk( index ){
		$('.children_data_tr').eq(index).removeClass('inactive');
		if( $('.sku_delete_flag').get(index).checked == true ){
			$('.children_data_tr').eq(index).addClass('inactive');
		}
	}
	
	//初期状態
	$('.children_data_tr').each(function(i){
		sku_delete_chk( i )
	});
	
	
	// ----------------------------------------------------
	
	/**
	 * SKUの表示入れ替え
	 */
	
	function rank_buttons(){
		$('.sku_rank_up').unbind();
		$('.sku_rank_down').unbind();
		$('.sku_rank_up').on(
			'click',
			function(e){
				var index = $('.sku_rank_up').index(this);
				sku_rank( index, 'up' );
			}
		);
		$('.sku_rank_down').on(
			'click',
			function(e){
				var index = $('.sku_rank_down').index(this);
				sku_rank( index, 'down' );
			}
		);
	}
	rank_buttons();//初期用
	
	//関数
	function sku_rank( index, mode )
	{
		//移動の必要性チェック
		if( (mode == 'up')&&( index == 0 )){
			return;
		}
		var row_count = $('.children_data_tr').length;
		if( ( mode == 'down' )&&( (index+1) == row_count )){
			return;
		}
		
		//クローン作成
		var cl = $('.children_data_tr').eq(index).clone(true);
		
		
		//移動番号を設定
		var target;
		if( mode == 'up' ){
			target = $('.children_data_tr').eq(index).prev();
		}
		else{
			target = $('.children_data_tr').eq(index).next();
		}
		
		
		//まず元を消す
		var target2 = $('.children_data_tr').eq(index);
		$(target2).fadeOut(
			'normal',
			function(){
				//$('.children_data_tr').eq(index).remove();
				
				//登場
				if( mode == 'up'){
					$(target).before(target2);
				}
				else{
					$(target).after(target2);
				}
				$(target2).show();
				
				//ランクボタン処理
				rank_buttons();
				
				//ランク値を入れ替え
				change_rank_values();
			}
		);
		
	}
	
	//ランクの値を変更
	function change_rank_values(){
		var count = $('.children_data_tr').length;
		var i = 0;
		while(i < count ){
			
			//ネーム書き換え
			var x = 0;
			while( x < $('.children_data_tr').eq(i).find('input[name^=ProductSkuModel]').length ){
				var name = $('.children_data_tr').eq(i).find('input[name^=ProductSkuModel]').eq(x).attr('name');
				//console.log(name);
				name = name.replace( /\[([0-9]*)\]/, '['+(i)+']' );
				$('.children_data_tr').eq(i).find('input[name^=ProductSkuModel]').eq(x).attr('name', name)
				x ++ ;
			}
			var x = 0;
			while( x < $('.children_data_tr').eq(i).find('textarea[name^=ProductSkuModel]').length ){
				var name = $('.children_data_tr').eq(i).find('textarea[name^=ProductSkuModel]').eq(x).attr('name');
				name = name.replace( /\[([0-9]*)\]/, '['+(i)+']' );
				$('.children_data_tr').eq(i).find('textarea[name^=ProductSkuModel]').eq(x).attr('name', name);
				x ++ ;
			}
			
			//SKUイメージのネーム書き換え
			var x = 0;
			var name = 'sku_image_path[' + i + '][]';
			$('.children_data_tr').eq(i).find('input[name^=sku_image_path]').attr('name',name);
			$('.children_data_tr').eq(i).find('.sku_image_rank_change').attr('data-skuimage-index',i);
			$('.children_data_tr').eq(i).find('.sku_image_rank_delete').attr('data-skuimage-index',i);
			
			
			//ランクの値入れ替え
			$('.children_data_tr').eq(i).find('.sku_rank').val(i);
			i ++ ;
		}
	}
	
	
	// ----------------------------------------------------
	
	/**
	 * 画像アップロード
	 */
	
	$('.upload_image').on('change', function(){
		var _this = $(this);
		var url = homeUrl + '/fileupload/';
		var target = _this.data('target');
		var mode = _this.data('mode');
		
		
		// form生成
		var _form = $('<form></form>');
		_form.append('<input type="hidden" name="FileUploadModel[mode]" value="' + mode + '" />' );
		var formData = new FormData(_form.get(0));
		formData.append('FileUploadModel[image]', _this.get(0).files[0]);
		
		
		// 処理実行
		$('#loading_modal').modal();
		$.ajax(
			url,
			{
				'type': 'post',
				'contentType': false,
				'processData': false,
				'data': formData,
				'dataType': 'json',
				'success': function(data){
					
					$('#loading_modal').modal('hide');
					
					if(data.result == false ){
						alert(data.error_message);
						reutrn;
					}
					//alert('画像アップロード成功');
					var imgPath = data.absolute_url;
					var imgResPath = data.absolute_res_url;
					$('#' + target + '_img').attr('src', baseUrl + imgPath);
					$('#' + target + '_hidden').val(imgPath);
					$('#' + target + '_res_hidden').val(imgResPath);
				},
				'error': function(e){
					$('#loading_modal').modal('hide');
					alert('アップロードに失敗しました');
				}
			}
		);
		
	});
	
	// ----------------------------------------------------
	
	/**
	 * 商品画像アップロードd&d
	 */
	$('.sku_product_drop_area').on('drop', function (e) {
		var files = e.originalEvent.dataTransfer.files;
		var index = $('.sku_image_drop_area').index( this );
		$(this).css('background-color','#FFF');
		
		
		var url = homeUrl + '/fileupload/';
		
		var target = $(this).attr('data-target');
		
		// form生成
		var _form = $('<form></form>');
		_form.append('<input type="hidden" name="FileUploadModel[mode]" value="product_images" />' );
		var formData = new FormData(_form.get(0));
		formData.append('FileUploadModel[image]', files[0]);
		$.ajax(
			url,
			{
				'type': 'post',
				'contentType': false,
				'processData': false,
				'data': formData,
				'async': false,
				'dataType': 'json',
				'success': function(data){
					if(data.result == false ){
						alert(data.error_message);
					}
					var imgPath = data.absolute_url;
					var imgResPath = data.absolute_res_url;
					
					//表示
					$('.upload_result_area[data-target="'+target+'"]')
						.empty()
						.append(
							$('<img>')
								.attr('width',100)
								.attr('src',baseUrl+imgResPath)
						)
					;
					$('#' + target + '_hidden').val(imgPath);
					$('#' + target + '_res_hidden').val(imgResPath);
				},
				'error': function(e){
					$('#loading_modal').modal('hide');
					alert('アップロードに失敗しました');
				}
			}
		);
		
		return false;
	})
	.on('dragenter', function(){
		$(this).css('background-color','#FFA0A2');
		return false;
	})
	.on('dragover', function(){ return false; })
	.on('dragleave', function() {
		$(this).css('background-color','#FFF');
		return false;
	})
	;
	
	// ----------------------------------------------------
	
	/**
	 * 追加画像
	 */
	$('.add_images_delete').on(
		'click',
		function(eve){
			var index = $('.add_images_delete').index(this);
			$('#add_images tbody tr').eq(index).remove();
		}
	);
	
	$('.add_images_rank_change').on(
		'click',
		function(){
			var type = $(this).attr('data-rank-type');
			var index = $('.add_images_rank_change[data-rank-type=' + type + ']').index(this);
			var target = $('#add_images tbody tr').eq(index);
			var clone = $(target).clone(true);
			var length = $('#add_images tbody tr').length;
			
			if( (type == 'up')&&(index==0) )
			{
				return;
			}
			if( (type == 'down')&&( ( index + 1 ) ==  length ) )
			{
				return;
			}
			
			
			$(target).fadeOut(
				'normal',
				function(){
					if( type == 'up' )
					{
						$('#add_images tbody tr').eq(index-1).before( target );
					}
					else
					{
						$('#add_images tbody tr').eq(index+1).after( target );
					}
					$(target).show();
				}
			);
		}
	);
	
	var add_image_tr = $('#add_images tbody tr').eq(0).clone(true);
	$('#add_images tbody tr').eq(0).remove();
	
	$('.add_image_drop_area').on('drop', function (e) {
		var files = e.originalEvent.dataTransfer.files;
		$(this).css('background-color','#FFF');
		add_images( files );
		return false;
	})
	.on('dragenter', function(){
		$(this).css('background-color','#FFA0A2');
		return false;
	})
	.on('dragover', function(){ return false; })
	.on('dragleave', function() {
		$(this).css('background-color','#FFF');
		return false;
	});
	$('input[name=add_images]').on(
		'change',
		function(eve){
			add_images( $(this).get(0).files );
			$(this).val('');
		}
	);
	function add_images( files )
	{
		function add_image_clone( i )
		{
			var url = homeUrl + '/fileupload/';
			var file = files[i];
			var _form = $('<form></form>');
			_form.append('<input type="hidden" name="FileUploadModel[mode]" value="product_images" />' );
			var formData = new FormData(_form.get(0));
			formData.append('FileUploadModel[image]', file);
			$.ajax(
				url,
				{
					'type': 'post',
					'contentType': false,
					'processData': false,
					'data': formData,
					'async': false,
					'dataType': 'json',
					'success': function(data){
						if(data.result == false ){
							alert(data.error_message);
						}
						var imgPath = data.absolute_url;
						var imgResPath = data.absolute_res_url;
						var clone = $(add_image_tr).clone(true);
						
						//clone操作
						$(clone).find('input[name="add_images[]"]').val( imgResPath );
						$(clone).find('img').attr('src',baseUrl+imgResPath)
						
						$('#add_images tbody').append( clone );
						
						i ++ ;
						if( i < files.length )
						{
							add_image_clone(i);
						}
						
					},
					'error': function(e){
						$('#loading_modal').modal('hide');
						alert('アップロードに失敗しました');
					}
				}
			);
		}
		add_image_clone(0);
	}
	
	// ----------------------------------------------------
	
	/**
	 * FAQ
	 */
	function faq_rank(){
		var i = 0;
		var len = $('#faq_table tbody tr').length;
		while( i < len )
		{
			var target = $('#faq_table tbody tr').eq(i);
			
			var list = $(target).find('*[name^=FaqModel]');
			var x = 0;
			while( x < list.length )
			{
				var name = $(target).find('*[name^=FaqModel]').eq(x).attr('name');
				var id = $(target).find('*[name^=FaqModel]').eq(x).attr('id');
				name = name.replace(/FaqModel\[([0-9]*)\]/, 'FaqModel[' + i + ']' );
				id = id.replace(/FaqModel_([0-9]*)/,'FaqModel_' + i);
				$(target).find('*[name^=FaqModel]').eq(x).attr('name',name);
				$(target).find('*[name^=FaqModel]').eq(x).attr('id',id);
				x ++ ;
			}
			
			var list = $(target).find('*[for^=FaqModel]');
			var x = 0;
			while( x < list.length )
			{
				var for1 = $(target).find('*[for^=FaqModel]').eq(x).attr('for');
				for1 = for1.replace(/FaqModel_([0-9]*)/,'FaqModel_' + i);
				$(target).find('*[for^=FaqModel]').eq(x).attr('for',for1);
				x ++ ;
			}
			i ++ ;
		}
		
	}
	$('.faq_delete').on(
		'click',
		function(eve){
			var index = $('.faq_delete').index(this);
			$('#faq_table tbody tr').eq(index).remove();
			faq_rank();
		}
	);
	$('.faq_rank_change').on(
		'click',
		function(eve){
			var type = $(this).attr('data-rank-type');
			var index = $('.faq_rank_change[data-rank-type=' + type + ']').index(this);
			var target = $('#faq_table tbody tr').eq(index);
			var len = $('#faq_table tbody tr').length;
			if( ( type == 'up' ) && ( index == 0 ) )
			{
				return;
			}
			if( ( type == 'down' ) && ( ( index + 1 ) == len ) )
			{
				return;
			}
			$(target).fadeOut('normal', function(){
				if( type == 'up' ){
					$('#faq_table tbody tr').eq( index - 1 ).before( target );
				}
				else{
					$('#faq_table tbody tr').eq( index + 1 ).after( target );
				}
				$(target).show();
				faq_rank();
			});
		}
	);
	var faq_table_tr = $('#faq_table tbody tr').eq(0).clone(true);
	$('#faq_table tbody tr').eq(0).remove();
	$('.faq_add').on(
		'click',
		function(eve){
			var clone = $( faq_table_tr ).clone( true );
			var len = $('#faq_table tbody tr').length;
			var list = $(clone).find('*[name^=FaqModel]');
			var i = 0;
			while( i < list.length )
			{
				var name = $(clone).find('*[name^=FaqModel]').eq(i).attr('name');
				var id = $(clone).find('*[name^=FaqModel]').eq(i).attr('id');
				name = name.replace('FaqModel[0000]','FaqModel[' + len +']');
				id = id.replace('FaqModel_0000','FaqModel_' + len);
				$(clone).find('*[name^=FaqModel]').eq(i).attr('name',name);
				$(clone).find('*[name^=FaqModel]').eq(i).attr('id',id);
				$(clone).find('*[for^=FaqModel]').eq(i).attr('for',for1);
				i ++ ;
			}
			
			var list = $(clone).find('*[for^=FaqModel]');
			var i = 0;
			while( i < list.length )
			{
				var for1 = $(clone).find('*[for^=FaqModel]').eq(i).attr('for');
				for1 = for1.replace('FaqModel_0000','FaqModel_' + len);
				$(clone).find('*[for^=FaqModel]').eq(i).attr('for',for1);
				i ++ ;
			}
			$('#faq_table tbody').append(clone);
		}
	);
	
	// ----------------------------------------------------
	
	/**
	 * タグ追加
	 */
	$('.tags_element a').on(
		'click',
		function(eve){
			var index = $('.tags_element a').index(this);
			$('.tags_element').eq(index).remove();
		}
	);
	var tag_element_source = $('#tag_element_source').clone(true);
	$('#tag_element_source').remove();
	$(tag_element_source).removeAttr( 'id' );
	$('.addtagsbtn').on(
		'click',
		function(eve){
			var text = $('input[name=addtags]').val();
			text = text.replace( '　', ' ' );
			var list = text.split( ' ' );
			var i = 0;
			while( i < list.length )
			{
				if( $('span[data-check-label="' + list[i] + '"]').length > 0 )
				{
					i ++ ;
					continue;
				}
				
				var clone = $(tag_element_source).clone(true);
				$(clone).find('.tags_element_label').empty().append( list[i] );
				$(clone).attr('data-check-label', list[i] );
				$(clone).find('.tags_element_hidden').val( list[i] );
				$('.tags_area').append( clone );
				i ++ ;
			}
			
		}
	);
	
	// ----------------------------------------------------
	
	/**
	 * 入荷
	 */
	
	$('.sku_insert_stocking').on(
		'click',
		function(e){
			var skuid = $(this).attr( 'data-skuid' );
			var url = stockingUrl + '/' + skuid;
			location.href = url;
		}
	);
	
	// ----------------------------------------------------
	
	// ページロード時
	sku_delete_chk();
	// チェックボックス操作時
	$('.sku_delete_flag').on('change', function(){
		sku_delete_chk();
	});
	
	// ----------------------------------------------------
	/**
	 * バーコード操作
	 */
	
	// モーダル表示, リーダーで読み取り
	$('.sku_code_read').on('click', function(e){
		read_barcode(this,e);
	});
	
	function read_barcode( elem, e )
	{
		//e.preventDefault();
		var index = $('.sku_code_read').index(elem);
		var _modal = $('#modal_code_read');
		var _codeVal = _modal.find('#tmp_code');
		var code = '';
		_modal.dialog({
			'modal': true,
			'open': function(e){
				$('#modal_code_read_label').get(0).click();
				$('#modal_code_read_label').unbind( 'keydown' );
				$('#modal_code_read_label').bind(
					'keypress',
					function(e){
						if( e.keyCode == 13 ){
							var set_value = $('#modal_code_read_input').val();
							$('#modal_code_read_input').val( '' );
							var target_id = '#ProductSkuModel_' + index + '_code_str';
							$(target_id).val( set_value );
							_modal.dialog('close');
						}
					}
				)
				
				
			}
		});
		return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * バーコードランダム生成
	 */
	$('.sku_code_gen').on('click', function(e){
		set_rand_code( this, e );
	});
	function set_rand_code( elem, e )
	{
		var res = confirm('コード発行をします。よろしいですか？');
		if( res == false){
			return false;
		}
		
		
		//e.preventDefault();
		var index = $('.sku_code_gen').index(elem);
		var _target = $('#ProductSkuModel_'+ index +'_code_str');
		
		
		$.get(homeUrl + '/product/sku/gencode/').done(function(data){
			_target.val(data);
		});
	}
	
	// ----------------------------------------------------
	
	/**
	 * SKU削除
	 */
	$('.sku_remove_all').on(
		'click',
		function(e){
			sku_remove_all(this,e)
		}
	);
	function sku_remove_all(elem,e){
		
		var res = confirm('このSKUを消してよろしいですか？');
		if( res == false ){
			return false;
		}
		
		
		var target_id = ~~$(elem).attr('data-target_id');
		var index = $('.sku_remove_all').index( elem );
		
		
		//データベース削除
		if( target_id > 0 ){
			
			var url = skuDeleteUrl + '?id=' + target_id;
			
			$.ajax({
				'url': url,
				'type': 'get',
				'dataType': 'json',
				'success': function(data){
					if( data == false ){
						alert('データ削除に失敗しました');
					}
					else{
						sku_remove_all_exec( index );
					}
				},
				'error': function(e){
					alert('通信に失敗しました');
				}
			});
			
			
		}
		//行削除
		else{
			sku_remove_all_exec( index );
		}
		return;
		
		
		//実行関数
		function sku_remove_all_exec( index ){
			//消し去ってやる１！！
			$('.children_data_tr').eq(index).remove();
			
			//ランク値を入れ替え
			change_rank_values();
		}
		
		
	}
	
	// ----------------------------------------------------
	/**
	 * モーダル表示
	 */
	$('#loading_modal').modal({
		//背景をオーバーレイするか否か。(デフォルト) true
		//'static'を指定した場合は、背景をクリックしてもモーダルを閉じない。
		backdrop: 'static',
		//エスケープキーで閉じるか否か。(デフォルト) true
		keyboard: false,
		//訪問時に自動的に表示を行うか否か。(デフォルト) true
		show: false
	});
	
	// ----------------------------------------------------
	
	/**
	 * 関連商品の選択
	 */
	$('.get-rel').on(
		'click',
		function( e ){
			
			//商品ID
			var id = $(this).attr( 'data-id' );
			
			//
			regi_search_modal({});
			
			//再表示
			get_rel();
		}
	);
	
	// ----------------------------------------------------
	
	/**
	 * 関連商品ダイアログの表示
	 */
	var current_search_params = {};
	function regi_search_modal( params ){
		
		
		//一時保存
		current_search_params = params;
		
		
		//取得開始
		$.ajax({
			'url': relUrl,
			'type': 'get',
			'dataType': 'html',
			'data': params,
			'error': function(e){
				alert('通信に失敗しました');
			},
			'success': function(data){
				
				
				//表示
				$('#relmodal').empty();
				$('#relmodal').append(data);
				
				//フォーム実行
				$('#relmodal').find('.regi_search_form').on(
					'submit',
					function(e){
						var form = $(this).get(0);
						var params = {};
						for( var key in form.elements ){
							if( !key.match(/[^0-9]/) ){
								continue;
							}
							params[key] = form.elements[key].value;
						}
						regi_search_modal( params );
						return false;
					}
				);
				
				
				//ページャー
				$('#relmodal').find('.yiiPager a').on(
					'click',
					function(e){
						var url = $(this).attr('href');
						url = url.replace(relUrl,'' );
						var key_list = url.split('/');
						if( key_list[0]==''){
							key_list.shift();
						}
						var params = {};
						
						for( var i in key_list ){
							i = ~~i
							if( (i%2) == 0 ){
								key_list[i] = decodeURIComponent( key_list[i] );
								params[ key_list[i] ] = key_list[i + 1];
							}
						}
						regi_search_modal( params );
						return false;
					}
				);
				
				
				
				//商品選択を実行
				$('#relmodal').find('.next_step').on(
					'click',
					function(ev){
						var products_id = $(this).attr('data-products-id');
						
						//jsonの値を変更
						var json = $('.rel_json').eq(0).val();
						json = decodeURIComponent( json );
						var en = $.parseJSON(json);
						en.push(products_id);
						en = $.stringify( en );
						en = encodeURIComponent( en );
						$('.rel_json').eq(0).val(en);
						$('#relmodal').dialog('close');
						get_rel();
					}
				);
				
				
				//ダイアログ表示
				$('#relmodal').dialog({
					'modal': true,
					'width': '90%',
					'height': $(window).height() * 0.9
				});
				
				
			}
		});
		
	}
	// ----------------------------------------------------
	
	/**
	 * 関連商品の表示
	 */
	function get_rel()
	{
		var json = $('.rel_json').eq(0).val();
		$.ajax({
			'url': getRelUrl,
			'type': 'get',
			'datatype': 'html',
			'data':{
				'json': json
			},
			'error': function(e){
				alert('関連商品の取得に失敗しました');
				return;
			},
			'success': function( data ){
				
				
				$('.rel_products').empty().append( data );
				
				$('.rel_products').find( '.rel_delete' ).on(
					'click',
					function(eve){
						var target= $(this).attr('data-delete');
						//jsonの値を変更
						var json = $('.rel_json').eq(0).val();
						json = decodeURIComponent( json );
						var en2 = $.parseJSON(json);
						var en = [];
						var i = 0;
						while( i < en2.length )
						{
							if( en2[i] != target )
							{
								en.push( en2[i] );
							}
							i ++ ;
						}
						
						en = $.stringify( en );
						en = encodeURIComponent( en );
						$('.rel_json').eq(0).val(en);
						$('#relmodal').dialog('close');
						get_rel();
						
						
					}
				);
				
				
			}
		});
		
	}
	get_rel();
	// ----------------------------------------------------
	
	/**
	 * オプションSKU
	 * @param {type} param
	 */
	$('#sku_option_add_table tbody tr .skuoptiondelete').on(
		'click',
		function(eve){
			var index = $('#sku_option_add_table tbody tr .skuoptiondelete').index(this);
			$('#sku_option_add_table tbody tr').eq(index).remove();
		}
	);
	$('#sku_option_add_table tbody tr .skuoptionrank').on(
		'click',
		function(eve){
			var type = $(this).attr('data-rank-type');
			var index = $('#sku_option_add_table tbody tr .skuoptionrank[data-rank-type="'+ type + '"]').index(this);
			var target = $('#sku_option_add_table tbody tr').eq(index);
			var len = $('#sku_option_add_table tbody tr').length;
			if((type == 'up')&&(index==0))
			{
				reutrn;
			}
			if( ( type == 'down' )&&( len == (index+1) ) )
			{
				return;
			}
			$(target).fadeOut(
				'normal',
				function(eve2){
					
					if( type == 'up'){
						$('#sku_option_add_table tbody tr').eq(index-1).before(target);
					}
					else{
						$('#sku_option_add_table tbody tr').eq(index+1).after(target);
					}
					$(target).show();
				}
			);
		}
	);
	
	$('.select_adds').on(
		'click',
		function(eve){
			optionaddsmodel( addOptionsUrl );
			
		}
	);
	function optionaddsmodel( url )
	{
		//元に戻るURLを記録
		this.return_url;
		if( typeof(this.return_url) == 'undefined' )
		{
			this.return_url = [];
		}
		var that = this;
		that.return_url.push( url );
		if( that.return_url.length > 20 )
		{
			that.return_url.length = 20;
		}
		
		$.ajax({
			'url': url,
			'type':'get',
			'dataType': 'html',
			'success': function(data){
				
				//ダイアログ表示
				$('#option_adds_modal')
					.empty()
					.append(data)
					.dialog({
						'modal': true,
						'width': '90%',
						'height': $(window).height() * 0.9
					});
				
				
				
				//フォーム処理
				$('#option_adds_modal').find('form').on(
						'submit',function(){
							var f = $(this).get(0);
							var params = [];
							for( var key in f.elements )
							{
								if(! key.match(/^ProductsModel\[/))
								{
									continue;
								}
								params.push( key + '=' + f.elements[key].value );
							}
							var url = addOptionsUrl + '?' + params.join('&');
							optionaddsmodel( url );
							return false;
						}
					);
				//aタグ
				$('#option_adds_modal').find('a:not(.next_step)').on(
						'click',function(){
							var href = $(this).attr('href');
							if( href == 'javascript:void(0);')
							{
								return;
							}
							optionaddsmodel( href );
							return false;
						}
					);
				
				//戻るボタン
				$('#option_adds_modal').find('.skuaddreturn').on(
						'click',
						function(e){
							var set_url = that.return_url[ that.return_url.length - 2];
							optionaddsmodel(set_url);
						}
				);
				//すべてチェック
				$('#option_adds_modal').find('.addskucheckall').on(
						'click',
						function(eve){
							$('#option_adds_modal')
								.find('input[type=checkbox]')
								.attr('checked','checked');
						}
					);
				//選択取得
				$('#option_adds_modal').find('.skuaddselect').on(
					'click',
					function(){
						var checked = $('#option_adds_modal').find('input[type=checkbox]:checked');
						var list = [];
						var i = 0;
						while( i < checked.length )
						{
							var datas = $(checked).eq(i).val();
							datas = eval( '(' + datas + ')' );
							list.push( datas );
							i ++;
						}
						//console.log(list);
						selectedOptionSkus(list);
						$('#option_adds_modal').dialog('close');
					}
				);
			},
			'error': function(e){
				alert('通信エラー');
			}
		});
	}
	var sku_option_add_table_row_clone = $('#sku_option_add_table tbody tr').eq(0).clone(true);
	$('#sku_option_add_table tbody tr').eq(0).remove();
	function selectedOptionSkus( list )
	{
		var i = 0;
		var len = list.length;
		i = 0;
		while( i < len )
		{
			var clone = $(sku_option_add_table_row_clone).clone(true);
			var one = list[i];
			$(clone).find('.product').empty().append( one.product.item_id + ' ' + one.product.item_name );
			$(clone).find('.sku').empty().append( one.sku.brunch_item_id + ' ' + one.sku.brunch_item_name );
			
			if( one.image != '' )
			{
				$(clone).find('.skuimage').attr('src',baseUrl+one.image );
			}
			else
			{
				$(clone).find('.skuimage').after('商品画像なし');
				$(clone).find('.skuimage').remove();
			}
			$(clone).find('*[name="optionsku[]"]').val( one.sku.id );
			
			$('#sku_option_add_table tbody').append(clone);
			
			i ++ ;
		}
	}
	
	// ----------------------------------------------------
});

//jQueryエクステんづ
jQuery.extend({
	stringify : function stringify(obj) {
		var t = typeof (obj);
		if (t != "object" || obj === null) {
			if (t == "string") obj = '"' + obj + '"';
			return String(obj);
		}
		else{
			var n, v, json = [], arr = (obj && obj.constructor == Array);
			for (n in obj) {
				v = obj[n];
				t = typeof(v);
				if (obj.hasOwnProperty(n)) {
					if (t == "string") v = '"' + v + '"'; else if (t == "object" && v !== null) v = jQuery.stringify(v);
					json.push((arr ? "" : '"' + n + '":') + String(v));
				}
			}
			return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
		}
	}
});