$(document).ready(function(){
	
	//親選択
	$('.parent_btn').on(
		'click',
		function(e){
			
			$.ajax({
				'url': get_parent_categories,
				'data': {
					'current': $('#CategoryModel_parent_id').val()
				},
				'type': 'get',
				'success': function( data ){
					$('#jquimodal #jquimodal_content').empty().append( data );
					
					$('#jquimodal #jquimodal_ok').unbind();
					$('#jquimodal #jquimodal_ok').bind(
						'click',
						function(e){
							var i = 0;
							while( $('#jquimodal_content .parent').length > i ){
								if( $('#jquimodal_content .parent').get(i).checked == true){
									$('#CategoryModel_parent_id').val( $('#jquimodal_content .parent').eq(i).val() );
									break;
								}
								i ++ ;
							}
							$('#jquimodal').dialog( 'close' );
						}
					);
					
					
					$('#jquimodal').dialog({
						width: '90%',
						height: 'auto',
						modal: true
					});
					
					var i = 0;
					while( $('#jquimodal_content .parent').length > i ){
						if( $('#jquimodal_content .parent').eq(i).val() == $('#CategoryModel_parent_id').val() ){
							$('#jquimodal_content .parent').get(i).checked = true;
							break;
						}
						i ++ ;
					}
					
				}
			});
			
			
		}
	);
	
	
	
	
	
});