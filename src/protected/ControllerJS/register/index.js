/* =============================================================================
 * レジスターコントロールJS
 * ========================================================================== */
var IM_AFTER_INSERT_NODE;
$(document).ready(function(){
	
	// ----------------------------------------------------
	
	//バーコード読み取り後にフォーカスするところ
	var after_barcode_click = null;
	
	// ----------------------------------------------------
	
	//IMを実行
	INTERMediator.construct(true);
	
	// ----------------------------------------------------
	
	/**
	 * バーコードリーダー
	 **/
	
	var barcoderead = '';
	var barcodereadmode = true;
	/*
	$(window).on(
		'keydown',
		function(e){
			
			if( barcodereadmode == true ){
				
				//console.log( e.keyCode );
				
				//LFは読み飛ばし
				if( e.keyCode == 10 ){
					
				}
				
				//コードが改行なので終わり
				else if( e.keyCode == 13 ){
					//alert(barcoderead);
					get_from_code( barcoderead );
					barcoderead = '';
				}
				
				//キーコード加算
				else{
					barcoderead += String.fromCharCode(e.keyCode);
					//barcoderead += e.keyCode + "\n";
				}
				
				
			}
			
		}
	);
	*/

	// ----------------------------------------------------
	
	/**
	 * コードからデータを取得
	 **/
	function get_from_code( code ){
		
		//SKU
		var sku = IM_find(
			'product_sku',
			[
				{'field': 'code_str', 'operator': '=', 'value': code}
			]
		);
		
		//商品がなかったとき
		if( sku == null ){
			alert('商品が見つかりません');
			return;
		}
		
		
		//メイン商品
		var product = IM_find(
			'products',
			[
				{'field': 'id', 'operator': '=', 'value': sku['product_id']}
			]
		);
		
		
		//ダイアログ
		var params = {
			'product': product,
			'sku': sku
		};
		item_select_window( params );
		
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * 商品選択ウインドウ
	 */
	function item_select_window( params ){
		
		
		var target_element = '#item_select_mode_modal';
		
		
		
		//表示内容
		$(target_element).find('.item_select_item_id').empty().append(
			params['product']['item_id'] + '-' + params['sku']['brunch_item_id']
		);
			
		$(target_element).find('.item_select_item_name').empty().append(
			params['product']['item_name'] + '-' + params['sku']['brunch_item_name']
		);
			
		$(target_element).find('.item_select_price').empty().append(
			separate(params['sku']['price']) + '円'
		);
		
		$(target_element).find('.item_select_sale_price').empty().append(
			separate( params['sku']['sale_price'] ) + '円'
		);
		
		
		//数量デフォルト
		$(target_element).find('.qty_disp_area').val(1);
		
		
		//実行ボタン
		$(target_element).find('.add_item').unbind('click');
		$(target_element).find('.add_item').on(
			'click',
			function(e){
				
				//追加後の処理
				IM_AFTER_INSERT_NODE = function(target){
					
					//ダイアログ閉じる
					$('#item_select_mode_modal').dialog('close');
					$('#payment_mode_modal').dialog('close');
					$('#view_select_modal').dialog('close');
					
					
					
					//データ追加処理
					var t = target[0].lastChild;
					$(t).find('input[name=product_id]').val( params['product']['item_id'] + '-' + params['sku']['brunch_item_id'] );
					$(t).find('input[name=item_name]').val( params['product']['item_name'] + '-' + params['sku']['brunch_item_name'] );
					$(t).find('input[name=price]').val( params['sku']['sale_price'] );
					$(t).find('input[name=price_tax]').val(
							Math.floor( params['sku']['sale_price'] * ( 1 + ( tax_rate /100) ) )
						);
					$(t).find('input[name=qty]').val( $(target_element).find('.qty_disp_area').val() );
					$(t).find('.sku_id').eq(0).val( params['sku']['id'] );
					
					
					//データをDBに記録
					var repeat_id = $(t).find('.repeat-id').val();
					IMHelper_update(
						'receipt_detail',
						{ 'id': [ '=', repeat_id ] },
						{
							'item_id': $(t).find('input[name=product_id]').val(),
							'item_name': $(t).find('input[name=item_name]').val(),
							'price': $(t).find('input[name=price]').val(),
							'price_tax': $(t).find('input[name=price_tax]').val(),
							'qty': $(t).find('input[name=qty]').val(),
							'sku_id': $(t).find('.sku_id').eq(0).val()
						}
					);
					
					//総合処理
					calc_item_total();
					
					//フォーカスを戻す
					if( after_barcode_click != null )
					{
						after_barcode_click.click();
						after_barcode_click = null;
					}
					
					
					//ボタン処理を止める
					IM_AFTER_INSERT_NODE = null;
				}
				//追加実行
				$('#detail_box').find('._im_insert_button').get(0).click();
				
			}
		);
		
		
		//ダイアログ
		$(target_element).dialog({
					'modal': true,
					'width': '60%',
					'height': $(window).height() * 0.4
				});
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * 3桁カンマ
	 * @returns {undefined}
	 */
	
	function separate(num){
		return String(num).replace( /(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
	}
	// ----------------------------------------------------
	
	/**
	 * フォーカスものでバーコードたさない処理
	 **/
	function check_focus(){
		
		$('.barcode_focus').unbind( 'focus' );
		$('.barcode_focus').unbind( 'blur' );
		$('.barcode_focus').on(
			'focus',
			function(e){
				barcodereadmode = false;
				//console.log(barcodereadmode);
			}
		);
		$('.barcode_focus').on(
			'blur',
			function(e){
				barcodereadmode = true;
				//console.log(barcodereadmode);
			}
		);
	}
	
	// ----------------------------------------------------
	
	//新規レコード
	$('.set_new').on(
		'click',
		function(e){
			
			if( $('.order_status_check').val() < 2 )
			{
				if(!confirm('現在表示のご注文は入金されていませんが、新しい注文を作成しますか？')){
					return;
				}
			}
			location.href = set_new_url;
		}
	);
	
	
	// ----------------------------------------------------
	
	/**
	 * 履歴表示へジャンプ
	 **/
	$('.history').on(
		'click',
		function(e){
			location.href = history_url;
		}
	);
	
	// ----------------------------------------------------
	
	/**
	 * 最新情報へのジャンプ
	 */
	
	$('.to_newest').on(
		'click',
		function(e){
			location.href = to_newest_url;
		}
	);
	
	// ----------------------------------------------------
	
	/**
	 * receipt表示へ
	 */
	function go_receipt( elem, e )
	{
		var id = $('#record-id').val();
		var url = receipt_url + '/' + id;
		window.open( url );
		return;
		//Ladea様専用
		var set = 'eprints://' + url;
		window.open(set);
		//alert( set) ;
		
	}
	
	// ----------------------------------------------------
	
	//エンクロージャー終了
	INTERMediatorOnPage.expandingEnclosureFinish = function(name,target){
		
		//明細行表示終了
		if( name == 'receipt_detail'){
			finish_enclosure_detail( target )
		}
		
		//メイン表示終了
		if( name == 'receipt'){
			mainEnclosureFinish( target );
			input_checker();
		}
		
		check_focus();
	}
	
	// ----------------------------------------------------
	
	/**
	 * 明細行全行終了
	 **/
	
	function finish_enclosure_detail( target )
	{
		
		//金額数量変更
		$(target).find('.repeat-price,.repeat-price_tax,.repeat-qty').on(
			'change',
				function(eve){
					calc_item_total( this );
					
			}
		);
	}
	
	
	// ----------------------------------------------------
	
	//リピーター終了
	INTERMediatorOnPage.expandingRecordFinish = function( name, repeaters ){
		
		
		//来訪履歴
		if( name == 'comming'){
			
			var comming_id = $( repeaters ).find('.comming_id').val();
			var customer_id = $( repeaters ).find('.customer_id').val();
			
			
			//来訪履歴にジャンプ
			$(repeaters).find('.comming_history').on(
				'click',
				function(e){
					location.href = comming_url + '/' + customer_id + '?target=' + comming_id;
				}
			);
		}
		
		
	}
	
	// ----------------------------------------------------
	
	//エンクロージャ終了 - メイン
	function mainEnclosureFinish( target ){
		
		/**
		 * バーコードリーダー
		 */
		$('.barcodeinput').eq(0).focus();
		$('.barcodeinput').keypress(
			function(e){
				if( e.keyCode == 13 ){
					get_from_code( $(this).val() );
					$(this).val( '' );
					after_barcode_click = $(this).parent('.barcodeinputlabel').get(0);
				}
			}
		);
		
		
		/**
		 * 支払いモーダル表示
		 */
		$(target).find('.pay_with_money').unbind( 'click' );
		$(target).find('.pay_with_money').on(
			'click',
			function(e){
				
				var res = on_payment_product_is_registred();
				if( res == false ){
					return;
				}
				
				
				$('.input_payment').eq(0).val('');
				$('#payment_mode_modal').dialog({
					'modal': true,
					'width': '60%'
				});
			}
		);
		
		
		//支払い実行ボタン
		$('.exec_input_payment').unbind( 'click' );
		$('.exec_input_payment').on(
			'click',
			function(e){
				var input_pay = ~~$('.input_payment').eq(0).val();
				exec_paymet( input_pay );
				$('#payment_mode_modal').dialog( 'close' );
			}
		);
		
		
		/**
		 * receiptボタン
		 */
		$('.go_receipt').unbind( 'click' );
		$('.go_receipt').on(
			'click',
			function(e){
				go_receipt( this, e );
			}
		);
		
		
		/**
		 * 商品探す
		 */
		$(target).find('.select_product').on(
			'click',
			function(e){
				regi_search_modal({});
			}
		);
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * 支払い時商品登録ありなしチェック
	 */
	function on_payment_product_is_registred(){
		
		var i = 0;
		var len = $('.repeat-id').length;
		var sku_list = [];
		while( i < len ){
			if( ~~$('.sku_id').eq(i).val() > 0 ){
				sku_list.push(  'sku_list[' + i + ']=' + $('.sku_id').eq(i).val() );
			}
			i ++ ;
		}
		
		var url = checkregistered + '?' + sku_list.join('&');
		var response;
		
		$.ajax({
			'url': url,
			'type': 'get',
			'dataType': 'json',
			'async': false,
			'success': function( data ){
				response = data;
			},
			'error': function(e){
				alert('通信に失敗しました');
			}
		});
		
		
		
		
		//レスポンスをチェック
		var res = true;
		for( var sku_id in response ){
					
			//公開中止
			if( response[sku_id] == 0 ){
				
				var target = $('.sku_id[value=' + sku_id + ']' ).eq(0);
				var index = $('.sku_id').index( target );
				
				var item_id = $('.input_product_id').eq( index ).val();
				var item_name = $('.item_name').eq( index ).val();
				
				var text = item_name + '(' + item_id + ")\n" + 'は手続き中に販売中止商品となりました。' + "\n" + 'この商品を削除してから会計をしてください';
				alert( text );
				
				res = false;
			}
			//データベース除外
			else if( response[sku_id] == -1 ){
				
				var target = $('.sku_id[value=' + sku_id + ']' ).eq(0);
				var index = $('.sku_id').index( target );
				
				var item_id = $('.input_product_id').eq( index ).val();
				var item_name = $('.item_name').eq( index ).val();
				
				var text = item_name + '(' + item_id + ")\n" + 'は手続き中に仕入れ管理商品ではなくなりました。' + "\n" + '在庫管理をせずに販売する場合は「OK」販売を中止する場合は「キャンセル」をクリックしてください';
				res = confirm( text );
				
				
				//キャンセルなら在庫管理商品から外す
				if( res == true ){
					//var taget = '.sku_id[value=' + sku_id + ']';
					//$('.sku_id[value=' + sku_id + ']').val('');
					
					var i = 0;
					var len = $('.sku_id[value=' + sku_id + ']').length;
					while( i < len ){
						var index = $('.sku_id').index( $('.sku_id[value=' + sku_id + ']').eq(i) );
						var repeat_id = $('.repeat-id').eq( index ).val();
						INTERMediator_DBAdapter.db_update({
							'name': 'receipt_detail',
							'conditions': [
								{ 'field': 'id', 'value': repeat_id, 'operator': '=' }
							],
							'dataset':[
								{ 'field': 'sku_id', 'value': 'null' }
							]
						});
						$('.sku_id[value=' + sku_id + ']').eq(i).val('');
						i ++ ;
					}
				}
				
			}
			//OK
			else{
				
			}
		}
		
		
		
	
		return res;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 商品を探すモーダル
	 */
	var current_search_params = {};
	function regi_search_modal( params ){
		
		
		//一時保存
		current_search_params = params;
		
		var url = search_url;
		
		//取得開始
		$.ajax({
			'url': url,
			'type': 'get',
			'dataType': 'html',
			'data': params,
			'error': function(e){
				alert('通信に失敗しました');
			},
			'success': function(data){
				
				
				//表示
				$('#view_select_modal_content').empty().append(data);
				
				
				//フォーム実行
				$('#view_select_modal_content').find('.regi_search_form').on(
					'submit',
					function(e){
						var form = $(this).get(0);
						var params = {};
						for( var key in form.elements ){
							if( !key.match(/[^0-9]/) ){
								continue;
							}
							params[key] = form.elements[key].value;
						}
						regi_search_modal( params );
						return false;
					}
				);
				
				
				
				//ページャー
				$('#view_select_modal_content').find('.yiiPager a').on(
					'click',
					function(e){
						var url = $(this).attr('href');
						url = url.replace(search_url,'' );
						var key_list = url.split('/');
						if( key_list[0]==''){
							key_list.shift();
						}
						var params = {};
						
						for( var i in key_list ){
							i = ~~i
							if( (i%2) == 0 ){
								key_list[i] = decodeURIComponent( key_list[i] );
								params[ key_list[i] ] = key_list[i + 1];
							}
						}
						regi_search_modal( params );
						return false;
					}
				);
				
				
				//商品選択
				$('#view_select_modal_content').find('.regi_select_item li a').on(
					'click',
					function(e){
						var product_id = $(this).attr('data-products-id');
						var item_id = $(this).attr('data-item-id');
						get_sku( {'parent_id':product_id} );
					}
				);
				
				
				//ダイアログ表示
				$('#view_select_modal').dialog({
					'modal': true,
					'width': '90%',
					'height': $(window).height() * 0.9
				});
				
				
			}
		});
		
	}
	
	
	// ----------------------------------------------------
	
	/**
	 * SKU表示
	 **/
	function get_sku( params )
	{
		var url = sku_url;
		
		$.ajax({
			'url': url,
			'data': params,
			'dataType': 'html',
			'error': function(e){
				alert('通信エラー');
				return;
			},
			'success': function(data){
				
				//追加
				$('#view_select_modal_content').empty().append(data);
				
				//戻るボタン
				$('#view_select_modal_content').find('.return_button').on(
					'click',
					function(e){
						regi_search_modal(current_search_params);
					}
				);
				
				//商品選択
				$('#view_select_modal_content').find('.regi_select_item a').on(
					'click',
					function(e){
						
						if( ($(this).attr('data-stock') != '' ) && ($(this).attr('data-stock') == 0 ) ){
							alert('在庫切れ商品です');
							return;
						}
						
						var set_param = {
							'product': {
								'id': $(this).attr('data-product-id'),
								'item_id': $(this).attr('data-product-item-id'),
								'item_name': $(this).attr('data-product-name')
							},
							'sku':{
								'id': $(this).attr('data-id'),
								'brunch_item_id': $(this).attr('data-brunch-item-id'),
								'brunch_item_name': $(this).attr('data-brunch-item-name'),
								'sale_price': $(this).attr('data-sale-price'),
								'price': $(this).attr('data-price'),
								'stock': $(this).attr('data-stock')
							}
						};
						item_select_window( set_param );
					}
				);
			}
		});
	}
	
	// ----------------------------------------------------
	
	/**
	 * 支払い実行処理
	 * @param int price
	 **/
	function exec_paymet( payment ){
		
		//釣り銭計算
		var change_v = payment - ~~$('.grand_total_tax').val();
		
		if( change_v < 0 ){
			alert( '支払額が少ないです' );
			return;
		}
		
		//フォームにデータを渡してフォーム送信実行
		var form = $('.payment_form').eq(0);
		$(form).find('.payment_id').val( $('#record-id').val() );
		$(form).find('.payment_price').val( payment );
		$(form).find('.payment_method_on').val( 1 );
		$(form).get(0).submit();
		return;
		
		
		//支払い日時の記録
		var d = new Date();
		var f = 'yyyy-MM-dd HH:mm:ss';
		var set_date = comDateFormat(d, f);
		$('.payment_date').val( set_date );
		
		
		
		//レコードアップデート
		var id = $('#record-id').val();
		IMHelper_update(
			'receipt',
			{
				'id':[ '=', id ]
			},
			{
				'order_status': 2,
				'change_val': ~~change_v,
				'payment': payment,
				'payment_date': set_date
			}
		);
		
		
		//各行の在庫処理
		var platina_data = {};
		var i = 0;
		while( i < $('.sku_id').length ){
			var id = $('.sku_id').eq(i).val();
			platina_data['regi_zaiko['+id+']'] = $('.repeat-qty').eq(i).val();
			i ++ ;
		}
		
		$.ajax({
			'url': zaikodown_url,
			'data': platina_data,
			'dataType': 'json',
			'type': 'get',
			'succes': function(data){
				
			}
		});
			
		//表示変更
		$('.order_status').val(2);
		$('.order_status_check').val(2);
		$('.payment').val( payment );
		$('.change_val').val( change_v );
		//明細行変更禁止に
		input_checker();
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * ページ合成呼び出しあと
	 */
	INTERMediatorOnPage.doAfterConstruct = function(){
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * 削除
	 */
	IMonDelete = function(){
		calc_item_total();
	}
	
	// ----------------------------------------------------
	
	/**
	 * ステータスによって入力禁止に
	 */
	function input_checker(){
		if( $('.order_status_check').val() > 1 ){
			$('.input_checker').attr( 'readonly', 'readonly' );
			$('.input_checker_buttons').attr( 'disabled', 'disabled' );
			$('._im_insert_button').attr( 'disabled', 'disabled' );
			$('#detail_box').find('button[id^=IM_Button_]').attr( 'disabled', 'disabled' );
		}
	}
	
	// ----------------------------------------------------
	
	/**
	 * 商品代金計計算
	 */
	function calc_item_total( elem ){
		
		//総合計計算
		var i = 0;
		var item_total = 0;
		var item_total_tax = 0;
		while( $('.sub_total').length > i ){
			
			//小計計算
			var sub_total = ~~$('.repeat-price').eq(i).val() * ~~$('.repeat-qty').eq(i).val();
			$('.sub_total').eq(i).val( sub_total );
			
			var tax_check = false;
			if(
				(typeof(elem) == 'object')&&
				($(elem).hasClass('repeat-price_tax'))
			){
				tax_check = true;
			}
			else{
				if(
					~~$('.repeat-price').eq(i).val() > ~~$('.repeat-price_tax').eq(i).val()
				)
				{
					$('.repeat-price_tax').eq(i).val(
						Math.floor(
							~~$('.repeat-price').eq(i).val() * ( 1 + ( tax_rate/100 ) )
						)
					);
				}
			}
			
			var sub_total_tax = ~~$('.repeat-price_tax').eq(i).val() * ~~$('.repeat-qty').eq(i).val();
				$('.sub_total_tax').eq(i).val( sub_total_tax );
			
			
			//追加データ
			var adddata = {
				'sub_total': sub_total,
				'sub_total_tax': sub_total_tax
			};
			if( tax_check == false ){
				adddata['price_tax'] =  ~~$('.repeat-price_tax').eq(i).val();
			}
			
			
			//小計をDBに保存
			var id = $('.repeat-id').eq(i).val();
			IMHelper_update(
				'receipt_detail',
				{
					'id':[ '=', id ]
				},
				adddata
			);
			
			item_total += sub_total;
			item_total_tax += sub_total_tax;
			
			i ++ ;
		}
		
		//総合系表示とDB保存
		$('.item_total').val( item_total );
		$('.grand_total').val( item_total );
		
		$('.item_total_tax').val( item_total_tax );
		$('.grand_total_tax').val( item_total_tax );
		
		var id = $('#record-id').val();
		IMHelper_update(
			'receipt',
			{
				'id':[ '=', id ]
			},
			{
				'item_total_tax': item_total_tax,
				'grand_total_tax': item_total_tax,
				'item_total': item_total,
				'grand_total': item_total
			}
		);
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * 数字入力
	 */
	$('.input_num').on(
		'click',
		function(e){
			var target = $(this).attr('data-target-input');
			var num = $(this).val();
			num = num.replace(/ /g, '' );
			var set = String( $('.'+target).val() )  + String( num );
			set = ~~set;
			$('.'+target).val( set );
		}
	);
	
	// ----------------------------------------------------
	
});