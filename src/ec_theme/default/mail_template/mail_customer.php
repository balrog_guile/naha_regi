<?php
/* =============================================================================
 * 注文通知メール（お客様あて）
 * ========================================================================== */?>
=====================================================================
 ご注文がありました
=====================================================================
testu


ご注文ID【<?php echo $order_id; ?>】




■ ご注文者様情報 -----

お名前 : <?php echo $model->name1; ?> <?php echo $model->name2; ?> 様
ふりがな : <?php echo $model->kana1; ?> <?php echo $model->kana2; ?> さま
TEL : <?php echo $model->tel; ?>

ご住所 : <?php echo $model->zip; ?>

<?php echo $model->pref; ?>

<?php echo $model->address1; ?>

<?php echo $model->address2; ?>

<?php echo $model->address3; ?>

メールアドレス : <?php echo $model->mailaddress; ?>


<?php if( $model->d_check == 1 ): ?>
■ お届け先情報 -----

お名前 : <?php echo $model->d_name1; ?> <?php echo $model->d_name2; ?> 様
TEL : <?php echo $model->d_tel; ?>

ご住所 : <?php echo $model->d_zip; ?>

<?php echo $model->d_pref; ?>

<?php echo $model->d_address1; ?>

<?php echo $model->d_address2; ?>

<?php echo $model->d_address3; ?>

<?php endif; ?>


■ 配送方法 -----
<?php echo $delivery_list[$model->delivery_method]; ?>

■ お支払い方法 -----
<?php echo $payment_drop_down[$model->payment_method]; ?>


■ ご要望 -----
<?php echo $model->yobo; ?>



■ ご注文内容 -----

<?php foreach( $items as $key =>$item ): ?>
<?php foreach( $item['items'] as $itemone ): ?>
 品番:<?php echo $itemone['item_id']; ?>

 品名:<?php echo $itemone['item_name']; ?>

<?php endforeach; ?>
 単価 : <?php echo number_format($item['sub_calc']['base_price_tax']); ?>円
<?php if( $item['sub_calc']['is_ordermade'] == TRUE ): ?>
 数量 : <?php echo number_format($item['sub_calc']['qty']); ?>個
<?php else: ?>
 数量 : <?php echo number_format($item['sub_calc']['qty']); ?>個
<?php endif; ?>
 小計 : <?php echo number_format($item['sub_calc']['sub_total_tax']); ?>円

 ---
<?php endforeach; ?>


商品代金合計 : <?php echo number_format( $calc['item_total_tax'] ); ?>円
<?php echo $payment_drop_down[$model->payment_method]; ?>手数料 : <?php echo number_format( $calc['commission_fee_tax'] ); ?>円
配送料 : <?php echo number_format( $calc['delivery_fee_tax'] ); ?>円
お支払い総合計 : <?php echo number_format( $calc['grand_total_tax'] ); ?>円
